<?php
/**
 * PersonageServiceTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Service;

/** Usages */
use App\Entity\Personage;
use App\Exception\ChildException;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use App\Service\PersonageService;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class PersonageServiceTest
 * @package App\Tests\Service
 */
class PersonageServiceTest extends TestCase
{
    /**
     *
     */
    public function testSortByBirthYear()
    {
        $middle = new Personage(true, 1210);
        $old = new Personage(true, 1200);
        $young = new Personage(true, 1220);

        $unsorted = new ArrayCollection([
            $middle, $young, $old
        ]);
        $sorted = PersonageService::sortByBirthYear($unsorted);
        $this->assertEquals(3, $sorted->count());
        $this->assertEquals($old, $sorted->first());
        $this->assertEquals($middle, $sorted->next());
        $this->assertEquals($young, $sorted->next());
    }

    /**
     * @throws ChildException
     * @throws PersonageException
     */
    public function testFindHeirFromAscendants()
    {
        $dying = new Personage(true, 1200);
        $this->assertNull(PersonageService::findHeirFromAscendants($dying));

        $father = new Personage(true, 1180);
        $dying->setFather($father);
        $this->assertEquals($father, PersonageService::findHeirFromAscendants($dying));

        $mother = new Personage(false, 1180);
        $dying->setMother($mother);
        $this->assertEquals($father, PersonageService::findHeirFromAscendants($dying));

        $father->setDeathYear(1250);
        $this->assertEquals($mother, PersonageService::findHeirFromAscendants($dying));

        $mother->setDeathYear(1240);
        $this->assertNull(PersonageService::findHeirFromAscendants($dying));

        $greatFather = new Personage(true, 1160);
        $mother->setFather($greatFather);
        $this->assertNull(PersonageService::findHeirFromAscendants($dying));
        $this->assertEquals($greatFather, PersonageService::findHeirFromAscendants($dying, 2));
    }

    /**
     * @throws ChildException
     * @throws PersonageException
     */
    public function testFindHeirFromDescendants()
    {
        $dying = new Personage(true, 1200);
        $this->assertNull(PersonageService::findHeirFromDescendants($dying));

        $elderSon = new Personage(true, 1220);
        $dying->addChild($elderSon);
        $secondSon = new Personage(true, 1221);
        $dying->addChild($secondSon);
        $elderDaughter = new Personage(false, 1219);
        $dying->addChild($elderDaughter);
        $secondDaughter = new Personage(false, 1222);
        $dying->addChild($secondDaughter);

        $grandDaughter = new Personage(false, 1241);
        $elderSon->addChild($grandDaughter);
        $grandSon = new Personage(false, 1240);
        $secondSon->addChild($grandSon);

        $this->assertEquals($elderSon, PersonageService::findHeirFromDescendants($dying));

        $elderSon->setDeathYear(1245);
        $this->assertEquals($grandDaughter, PersonageService::findHeirFromDescendants($dying));

        $grandDaughter->setDeathYear(1242);
        $this->assertEquals($secondSon, PersonageService::findHeirFromDescendants($dying));

        $secondSon->setDeathYear(1245);
        $this->assertEquals($grandSon, PersonageService::findHeirFromDescendants($dying));

        $grandSon->setDeathYear(1260);
        $this->assertEquals($elderDaughter, PersonageService::findHeirFromDescendants($dying));
    }

    /**
     * @throws ChildException
     * @throws PersonageException
     */
    public function testFindHeirFromSiblings()
    {
        $father = new Personage(true, 1180);
        $dying = new Personage(true, 1200);
        $dying->setFather($father);
        $father->addChild($dying);
        $this->assertNull(PersonageService::findHeirFromSiblings($dying));

        $sister = new Personage(false, 1201);
        $father->addChild($sister);
        $this->assertEquals($sister, PersonageService::findHeirFromSiblings($dying));

        $olderBrother = new Personage(true, 1202);
        $father->addChild($olderBrother);
        $this->assertEquals($olderBrother, PersonageService::findHeirFromSiblings($dying));

        $youngerBrother = new Personage(true, 1203);
        $father->addChild($youngerBrother);
        $this->assertEquals($olderBrother, PersonageService::findHeirFromSiblings($dying));

        $nephew = new Personage(true, 1220);
        $sister->addChild($nephew);
        $this->assertEquals($olderBrother, PersonageService::findHeirFromSiblings($dying));

        $sister->setDeathYear(1225);
        $olderBrother->setDeathYear(1225);
        $youngerBrother->setDeathYear(1225);
        $this->assertEquals($nephew, PersonageService::findHeirFromSiblings($dying));

        $niece = new Personage(false, 1220);
        $youngerBrother->addChild($niece);
        $this->assertEquals($niece, PersonageService::findHeirFromSiblings($dying));

        $niece->setDeathYear(1225);
        $this->assertEquals($nephew, PersonageService::findHeirFromSiblings($dying));
    }

    /**
     * @throws ChildException
     * @throws PersonageException
     * @throws MarriageException
     */
    public function testFindHeir()
    {
        $dying = new Personage(true, 1200);
        $daughter = new Personage(false, 1220);
        $dying->addChild($daughter);
        $spouse = new Personage(false, 1200);
        $dying->setSpouse($spouse);
        $mother = new Personage(false, 1180);
        $dying->setMother($mother);
        $brother = new Personage(true, 1200);
        $mother->addChild($dying);
        $mother->addChild($brother);

        $this->assertEquals($daughter, PersonageService::findHeir($dying));

        $daughter->setDeathYear(1240);
        $this->assertEquals($spouse, PersonageService::findHeir($dying));

        $spouse->setDeathYear(1240);
        $this->assertEquals($brother, PersonageService::findHeir($dying));

        $brother->setDeathYear(1240);
        $this->assertEquals($mother, PersonageService::findHeir($dying));

        $mother->setDeathYear(1240);
        $this->assertNull(PersonageService::findHeir($dying));
    }
}
