<?php
/**
 * WorkServiceTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Service;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialQuantity;
use App\Entity\Mesnie;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use App\Service\WorkService;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkServiceTest
 * @package App\Tests\Service
 */
class WorkServiceTest extends TestCase
{
    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testHasMesnieEnoughMaterial()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $wheat = new Material("wheat", Material::TYPE_GRAIN);
        $sowWheat = new WorkType("sow wheat");
        $sowWheat->addMaterialNeed(new MaterialQuantity($wheat, 10));

        $this->assertFalse(WorkService::hasMesnieEnoughMaterial($sowWheat, $mesnie));

        $mesnie->addToInventory($wheat, 9);
        $this->assertFalse(WorkService::hasMesnieEnoughMaterial($sowWheat, $mesnie));

        $mesnie->addToInventory($wheat, 1);
        $this->assertTrue(WorkService::hasMesnieEnoughMaterial($sowWheat, $mesnie));
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testRemoveMaterialNeededFromMesnie()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $wheat = new Material("wheat", Material::TYPE_GRAIN);
        $sowWheat = new WorkType("sow wheat");
        $sowWheat->addMaterialNeed(new MaterialQuantity($wheat, 10));

        $mesnie->addToInventory($wheat, 11);
        WorkService::removeMaterialNeededFromMesnie($sowWheat, $mesnie);
        $this->assertEquals(1, $mesnie->getQuantityFromInventory($wheat));

        try {
            WorkService::removeMaterialNeededFromMesnie($sowWheat, $mesnie);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(1, $mesnie->getQuantityFromInventory($wheat));
        }
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testAddMaterialResultToMesnie()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $wheat = new Material("wheat", Material::TYPE_GRAIN);
        $harvestWheat = new WorkType("harvest wheat");
        $harvestWheat->addMaterialResult(new MaterialQuantity($wheat, 10));

        WorkService::addMaterialResultToMesnie($harvestWheat, $mesnie);
        $this->assertEquals(10, $mesnie->getQuantityFromInventory($wheat));

        WorkService::addMaterialResultToMesnie($harvestWheat, $mesnie);
        $this->assertEquals(20, $mesnie->getQuantityFromInventory($wheat));
    }
}
