<?php
/**
 * MarriageServiceTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Service;

/** Usages */
use App\Entity\Personage;
use App\Exception\ChildException;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use App\Service\MarriageService;
use PHPUnit\Framework\TestCase;

/**
 * Class MarriageServiceTest
 * @package App\Tests\Service
 */
class MarriageServiceTest extends TestCase
{
    /**
     * @throws MarriageException
     * @throws PersonageException
     */
    public function testMarriageImpeachment()
    {
        $man = new Personage(true, 1200);
        $woman = new Personage(false, 1210);
        $this->assertEquals(0, MarriageService::marriageImpeachment($man, $woman));
        $this->assertEquals(0, MarriageService::marriageImpeachment($woman, $man));
    }

    /**
     * @throws MarriageException
     * @throws PersonageException
     */
    public function testMarriageImpeachmentAge()
    {
        $man = new Personage(true, 1200);
        $woman = new Personage(false, 1210);
        $this->assertEquals(0, MarriageService::marriageImpeachment($man, $woman, 1224));
        $this->assertEquals(0, MarriageService::marriageImpeachment($woman, $man, 1224));
        $this->assertEquals(-3, MarriageService::marriageImpeachment($man, $woman, 1223));
        $this->assertEquals(-3, MarriageService::marriageImpeachment($woman, $man, 1223));
    }

    /**
     * @throws MarriageException
     * @throws PersonageException
     * @throws ChildException
     */
    public function testMarriageImpeachmentConsanguinity()
    {
        $man = new Personage(true, 1200);
        $father = new Personage(true, 1180);
        $man->setFather($father);
        $greatFather = new Personage(true, 1160);
        $father->setFather($greatFather);
        $uncle = new Personage(true, 1185);
        $uncle->setFather($greatFather);
        $cousin = new Personage(false, 1210);
        $cousin->setFather($uncle);
        $this->assertEquals(-2, MarriageService::marriageImpeachment($man, $cousin));
        $this->assertEquals(-2, MarriageService::marriageImpeachment($cousin, $man));
    }

    /**
     * @throws MarriageException
     * @throws PersonageException
     */
    public function testMarriageImpeachmentAlreadyMarried()
    {
        $celibateMan = new Personage(true, 1200);
        $celibateWoman = new Personage(false, 1200);
        $marriedMan = new Personage(true, 1200);
        $marriedWoman = new Personage(false, 1200);
        $marriedMan->setSpouse($marriedWoman);
        $marriedWoman->setSpouse($marriedMan);

        $this->assertEquals(0, MarriageService::marriageImpeachment($celibateMan, $celibateWoman));
        $this->assertEquals(-1, MarriageService::marriageImpeachment($marriedMan, $marriedWoman));
        $this->assertEquals(-1, MarriageService::marriageImpeachment($marriedMan, $celibateWoman));
        $this->assertEquals(-1, MarriageService::marriageImpeachment($celibateMan, $marriedWoman));
    }

    /**
     * @throws PersonageException
     */
    public function testMarriageImpeachmentSodomite()
    {
        $man = new Personage(true, 1200);
        $anotherMan = new Personage(true, 1200);

        try {
            MarriageService::marriageImpeachment($man, $anotherMan);
            $this->assertTrue(false);
        } catch (MarriageException $exception) {
            $this->assertEquals(MarriageException::ERROR_MARRY_SEX, $exception->getCode());
        }
    }
}
