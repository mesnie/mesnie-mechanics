<?php
/**
 * MarketServiceTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Service;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialValue;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use App\Service\MarketService;
use PHPUnit\Framework\TestCase;

/**
 * Class MarketServiceTest
 * @package App\Tests\Service
 */
class MarketServiceTest extends TestCase
{
    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testGetPriceWithoutValue()
    {
        $city = new City("Villeneuve");
        $material = new Material("wheat", Material::TYPE_GRAIN);

        try {
            MarketService::getPrice($city, $material);
            $this->assertTrue(false);
        } catch (MoneyException $exception) {
            $this->assertEquals(MoneyException::NO_VALUE, $exception->getCode());
        }
    }

    /**
     * @throws MoneyException
     * @throws MaterialException
     * @throws \Exception
     */
    public function testGetPrice()
    {
        $city = new City("Villeneuve");
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $value = rand(10, 100)/10;
        $materialValue = new MaterialValue($city, $material, $value);
        $city->addMaterialValue($materialValue);
        $material->addMaterialValue($materialValue);
        $quantity = rand(1, 10);

        $this->assertEquals($value, MarketService::getPrice($city, $material));
        $this->assertEquals($value * $quantity, MarketService::getPrice($city, $material, $quantity));
    }
}
