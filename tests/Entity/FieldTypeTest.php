<?php
/**
 * FieldTypeTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\FieldType;
use App\Entity\WorkType;
use PHPUnit\Framework\TestCase;

/**
 * Class FieldTypeTest
 * @package App\Tests\Entity
 */
class FieldTypeTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testWorkTypes()
    {
        $fieldType = new FieldType("empty");
        $this->assertEquals(0, $fieldType->getWorkTypes()->count());

        $workType = new WorkType("labor");
        $fieldType->addWorkType($workType);
        $this->assertEquals(1, $fieldType->getWorkTypes()->count());
        $this->assertEquals($workType, $fieldType->getWorkTypes()->first());

        $fieldType->removeWorkType($workType);
        $this->assertEquals(0, $fieldType->getWorkTypes()->count());
    }
}
