<?php
/**
 * WorkTypeTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\FieldType;
use App\Entity\Material;
use App\Entity\MaterialQuantity;
use App\Entity\WorkType;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkTypeTest
 * @package App\Tests\Entity
 */
class WorkTypeTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testFieldType()
    {
        $workType = new WorkType("labor");
        $this->assertNull($workType->getFieldType());

        $fieldType = new FieldType("empty");
        $workType = new WorkType("labor", 1, "", $fieldType);
        $this->assertEquals($fieldType, $workType->getFieldType());
    }

    /**
     * @throws \Exception
     */
    public function testWeeksNeeded()
    {
        $workType = new WorkType("labor");
        $this->assertEquals(1, $workType->getWeeksNeeded());

        $workType = new WorkType("labor", 4);
        $this->assertEquals(4, $workType->getWeeksNeeded());
    }

    /**
     * @throws \Exception
     */
    public function testMaterialResults()
    {
        $workType = new WorkType("labor");
        $this->assertEquals(0, $workType->getMaterialResults()->count());

        $materialOne = new Material("wheat", Material::TYPE_GRAIN);
        $materialResultOne = new MaterialQuantity($materialOne, 5);
        $workType->addMaterialResult($materialResultOne);
        $this->assertEquals(1, $workType->getMaterialResults()->count());
        $this->assertEquals($materialResultOne, $workType->getMaterialResults()->first());

        $materialTwo = new Material("winter wheat", Material::TYPE_GRAIN);
        $materialResultTwo = new MaterialQuantity($materialTwo, 10);
        $workType->addMaterialResult($materialResultTwo);
        $this->assertEquals(2, $workType->getMaterialResults()->count());
        $this->assertEquals($materialResultOne, $workType->getMaterialResults()->first());
        $this->assertEquals($materialResultTwo, $workType->getMaterialResults()->next());
    }

    /**
     * @throws \Exception
     */
    public function testMaterialNeeds()
    {
        $workType = new WorkType("labor");
        $this->assertEquals(0, $workType->getMaterialNeeds()->count());

        $materialOne = new Material("wheat", Material::TYPE_GRAIN);
        $materialNeedOne = new MaterialQuantity($materialOne, 5);
        $workType->addMaterialNeed($materialNeedOne);
        $this->assertEquals(1, $workType->getMaterialNeeds()->count());
        $this->assertEquals($materialNeedOne, $workType->getMaterialNeeds()->first());

        $materialTwo = new Material("winter wheat", Material::TYPE_GRAIN);
        $materialNeedTwo = new MaterialQuantity($materialTwo, 10);
        $workType->addMaterialNeed($materialNeedTwo);
        $this->assertEquals(2, $workType->getMaterialNeeds()->count());
        $this->assertEquals($materialNeedOne, $workType->getMaterialNeeds()->first());
        $this->assertEquals($materialNeedTwo, $workType->getMaterialNeeds()->next());
    }
}
