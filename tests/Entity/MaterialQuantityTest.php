<?php
/**
 * MaterialQuantityTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialQuantity;
use App\Entity\Mesnie;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use PHPUnit\Framework\TestCase;

/**
 * Class MaterialQuantityTest
 * @package App\Tests\Entity
 */
class MaterialQuantityTest extends TestCase
{
    /**
     * @throws MaterialException
     */
    public function testMaterial()
    {
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $materialQuantity = new MaterialQuantity($material);
        $this->assertEquals($material, $materialQuantity->getMaterial());
    }

    /**
     * @throws MaterialException
     */
    public function testQuantity()
    {
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $materialQuantity = new MaterialQuantity($material);
        $this->assertEquals(1, $materialQuantity->getQuantity());

        try {
            new MaterialQuantity($material, 0);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
        }

        $materialQuantity = new MaterialQuantity($material, 10);
        $this->assertEquals(10, $materialQuantity->getQuantity());

        try {
            $materialQuantity->add(0);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(10, $materialQuantity->getQuantity());
        }

        $materialQuantity->add(10);
        $this->assertEquals(20, $materialQuantity->getQuantity());

        try {
            $materialQuantity->remove(0);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(20, $materialQuantity->getQuantity());
        }

        $materialQuantity->remove(5);
        $this->assertEquals(15, $materialQuantity->getQuantity());

        try {
            $materialQuantity->remove(15);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(15, $materialQuantity->getQuantity());
        }
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testOwner()
    {
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $materialQuantity = new MaterialQuantity($material);
        $this->assertNull($materialQuantity->getOwner());

        $city = new City("Villeneuve");
        $owner = new Mesnie($city);
        $materialQuantity->setOwner($owner);
        $this->assertEquals($owner, $materialQuantity->getOwner());
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testNeededByWork()
    {
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $materialQuantity = new MaterialQuantity($material);
        $this->assertNull($materialQuantity->getNeededByWork());

        $workType = new WorkType("sow wheat");
        $materialQuantity->setNeededByWork($workType);
        $this->assertEquals($workType, $materialQuantity->getNeededByWork());
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testResultFromWork()
    {
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $materialQuantity = new MaterialQuantity($material);
        $this->assertNull($materialQuantity->getResultFromWork());

        $workType = new WorkType("harvest wheat");
        $materialQuantity->setResultFromWork($workType);
        $this->assertEquals($workType, $materialQuantity->getResultFromWork());
    }
}
