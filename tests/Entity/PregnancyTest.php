<?php
/**
 * MaterialTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\Personage;
use App\Entity\Pregnancy;
use App\Exception\ChildException;
use PHPUnit\Framework\TestCase;

/**
 * Class PregnancyTest
 * @package App\Tests\Entity
 */
class PregnancyTest extends TestCase
{
    /**
     * @throws ChildException
     */
    public function testParents()
    {
        $mother = new Personage(false, 1200);
        $father = new Personage(true, 1200);
        $pregnancy = new Pregnancy($mother, $father);
        $this->assertEquals($mother, $pregnancy->getMother());
        $this->assertEquals($father, $pregnancy->getFather());

        try {
            $otherWoman = new Personage(false, 1200);
            new Pregnancy($mother, $otherWoman);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PARENT_SEX, $exception->getCode());
        }

        try {
            $otherMan = new Personage(false, 1200);
            new Pregnancy($otherMan, $father);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PARENT_SEX, $exception->getCode());
        }
    }

    /**
     * @throws ChildException
     */
    public function testMonth()
    {
        $mother = new Personage(false, 1200);
        $father = new Personage(true, 1200);
        $pregnancy = new Pregnancy($mother, $father);
        $this->assertEquals(0, $pregnancy->getMonth());

        $pregnancy = new Pregnancy($mother, $father, 8);
        $this->assertEquals(8, $pregnancy->getMonth());

        $pregnancy->upMonth();
        $this->assertEquals(9, $pregnancy->getMonth());

        try {
            $pregnancy->upMonth();
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::WRONG_PREGNANCY_MONTH, $exception->getCode());
            $this->assertEquals(9, $pregnancy->getMonth());
        }

        try {
            new Pregnancy($mother, $father, -1);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::WRONG_PREGNANCY_MONTH, $exception->getCode());
        }

        try {
            new Pregnancy($mother, $father, 10);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::WRONG_PREGNANCY_MONTH, $exception->getCode());
        }
    }

    /**
     * @throws ChildException
     */
    public function testChildrenNumber()
    {
        $mother = new Personage(false, 1200);
        $father = new Personage(true, 1200);
        $pregnancy = new Pregnancy($mother, $father);
        $this->assertEquals(1, $pregnancy->getChildrenNumber());

        $pregnancy = new Pregnancy($mother, $father, 1, 5);
        $this->assertEquals(5, $pregnancy->getChildrenNumber());

        try {
            new Pregnancy($mother, $father, 1, 0);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::WRONG_PREGNANCY_NUMBER, $exception->getCode());
        }
    }
}
