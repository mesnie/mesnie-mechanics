<?php
/**
 * UserTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Mesnie;
use App\Entity\User;
use App\Exception\UserException;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest
 * @package App\Tests\Entity
 */
class UserTest extends TestCase
{
    /**
     * @throws UserException
     */
    public function testMail()
    {
        $user = new User("email@mail.com");
        $this->assertEquals("email@mail.com", $user->getEmail());
        $this->assertEquals("email@mail.com", $user->getUsername());

        try {
            new User("email@mail.");
            $this->assertTrue(false);
        } catch (UserException $exception) {
            $this->assertEquals(UserException::INVALID_EMAIL, $exception->getCode());
        }

        try {
            new User("@mail.com");
            $this->assertTrue(false);
        } catch (UserException $exception) {
            $this->assertEquals(UserException::INVALID_EMAIL, $exception->getCode());
        }
    }

    /**
     * @throws UserException
     */
    public function testRoles()
    {
        $user = new User("email@mail.com");
        $roleUser = ['ROLE_USER'];
        $roleAdmin = ['ROLE_ADMIN'];
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        $this->assertEquals($roleUser, $user->getRoles());

        $user->setRoles($roleAdmin);
        $this->assertEquals($roles, $user->getRoles());

        $user->setRoles($roles);
        $this->assertEquals($roles, $user->getRoles());
    }

    /**
     * @throws UserException
     * @throws \Exception
     */
    public function testMesnie()
    {
        $user = new User("email@mail.com");
        $this->assertNull($user->getMesnie());

        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        try {
            $user->setMesnie($mesnie);
            $this->assertTrue(false);
        } catch (UserException $exception) {
            $this->assertEquals(UserException::MESNIE_NOT_RELATED, $exception->getCode());
        }

        $mesnie->setUser($user);
        $user->setMesnie($mesnie);
        $this->assertEquals($mesnie, $user->getMesnie());
    }

    /**
     * @throws UserException
     */
    public function testPassword()
    {
        $user = new User("email@mail.com");
        $this->assertEquals('', $user->getPassword());

        $password = "password";
        $user->setPassword($password);
        $this->assertEquals($password, $user->getPassword());
    }
}
