<?php
/**
 * MesnieTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Field;
use App\Entity\FieldType;
use App\Entity\Material;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\User;
use App\Exception\ChildException;
use App\Exception\FamilyException;
use App\Exception\FieldException;
use App\Exception\MarriageException;
use App\Exception\MaterialException;
use App\Exception\PersonageException;
use PHPUnit\Framework\TestCase;

/**
 * Class MesnieTest
 * @package App\Tests\Entity
 */
class MesnieTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testCity()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $this->assertEquals($city, $mesnie->getCity());
    }

    /**
     * @throws FamilyException
     * @throws \Exception
     */
    public function testFamily()
    {
        $city = new City("Villeneuve");
        $family = new Family("Capet");
        $mesnie = new Mesnie($city, $family);
        $this->assertEquals($family, $mesnie->getFamily());

        $mesnie = new Mesnie($city);
        $this->assertNull($mesnie->getFamily());

        $mesnie->setFamily($family);
        $this->assertEquals($family, $mesnie->getFamily());

        $mesnie->setFamily(null);
        $this->assertNull($mesnie->getFamily());
    }

    /**
     * @throws \Exception
     */
    public function testUser()
    {
        $city = new City("Villeneuve");
        $user = new User("email@mail.com");
        $mesnie = new Mesnie($city, null, $user);
        $this->assertEquals($user, $mesnie->getUser());

        $mesnie = new Mesnie($city);
        $this->assertNull($mesnie->getUser());

        $mesnie->setUser($user);
        $this->assertEquals($user, $mesnie->getUser());

        $mesnie->setUser(null);
        $this->assertNull($mesnie->getUser());
    }

    /**
     * @throws FieldException
     * @throws \Exception
     */
    public function testField()
    {
        $city = new City("Villeneuve");
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);
        $mesnie = new Mesnie($city);
        $this->assertEquals(0, $mesnie->getFields()->count());

        $mesnie->removeField($field);
        $this->assertEquals(0, $mesnie->getFields()->count());

        try {
            $mesnie->addField($field);
            $this->assertTrue(false);
        } catch (FieldException $exception) {
            $this->assertEquals(FieldException::WRONG_OWNER, $exception->getCode());
            $this->assertEquals(0, $mesnie->getFields()->count());
        }

        $field->setMesnie($mesnie);
        $mesnie->addField($field);
        $this->assertEquals(1, $mesnie->getFields()->count());
        $this->assertEquals($field, $mesnie->getFields()->first());

        $mesnie->removeField($field);
        $this->assertEquals(0, $mesnie->getFields()->count());
    }

    /**
     * @throws MaterialException
     * @throws \Exception
     */
    public function testInventory()
    {
        $city = new City("Villeneuve");
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $material2 = new Material("winter wheat", Material::TYPE_GRAIN);
        $mesnie = new Mesnie($city);
        $this->assertEquals(0, $mesnie->getInventory()->count());
        $this->assertEquals(0, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(0, $mesnie->getQuantityFromInventory($material2));

        try {
            $mesnie->addToInventory($material, 0);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(0, $mesnie->getQuantityFromInventory($material));
        }

        $mesnie->addToInventory($material, 5);
        $this->assertEquals(1, $mesnie->getInventory()->count());
        $this->assertEquals(5, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(0, $mesnie->getQuantityFromInventory($material2));

        try {
            $mesnie->removeFromInventory($material, 0);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(5, $mesnie->getQuantityFromInventory($material));
        }

        $mesnie->addToInventory($material, 15);
        $this->assertEquals(1, $mesnie->getInventory()->count());
        $this->assertEquals(20, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(0, $mesnie->getQuantityFromInventory($material2));

        $mesnie->addToInventory($material2, 10);
        $this->assertEquals(2, $mesnie->getInventory()->count());
        $this->assertEquals(20, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(10, $mesnie->getQuantityFromInventory($material2));

        $mesnie->removeFromInventory($material, 5);
        $this->assertEquals(2, $mesnie->getInventory()->count());
        $this->assertEquals(15, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(10, $mesnie->getQuantityFromInventory($material2));

        try {
            $mesnie->removeFromInventory($material, 16);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_QUANTITY_NEGATIVE, $exception->getCode());
            $this->assertEquals(15, $mesnie->getQuantityFromInventory($material));
        }

        $mesnie->removeFromInventory($material, 15);
        $this->assertEquals(1, $mesnie->getInventory()->count());
        $this->assertEquals(0, $mesnie->getQuantityFromInventory($material));
        $this->assertEquals(10, $mesnie->getQuantityFromInventory($material2));

        try {
            $mesnie->removeFromInventory($material, 1);
            $this->assertTrue(false);
        } catch (MaterialException $exception) {
            $this->assertEquals(MaterialException::ERROR_INVENTORY_ENOUGH, $exception->getCode());
        }
    }

    /**
     * @throws FamilyException
     * @throws \Exception
     */
    public function testMembers()
    {
        $city = new City("Villeneuve");
        $member = new Personage(true, 1200);
        $mesnie = new Mesnie($city);
        $this->assertEquals(0, $mesnie->getMembers()->count());
        $this->assertFalse($mesnie->isMember($member));

        try {
            $mesnie->addMember($member);
            $this->assertTrue(false);
        } catch (FamilyException $exception) {
            $this->assertEquals(FamilyException::WRONG_MESNIE, $exception->getCode());
            $this->assertEquals(0, $mesnie->getMembers()->count());
            $this->assertFalse($mesnie->isMember($member));
        }

        $member->setMesnie($mesnie);
        $mesnie->addMember($member);
        $this->assertEquals(1, $mesnie->getMembers()->count());
        $this->assertEquals($member, $mesnie->getMembers()->first());
        $this->assertTrue($mesnie->isMember($member));

        $mesnie->removeMember($member);
        $this->assertEquals(0, $mesnie->getMembers()->count());
        $this->assertFalse($mesnie->isMember($member));
    }

    /**
     * @throws FamilyException
     * @throws MarriageException
     * @throws PersonageException
     * @throws \Exception
     */
    public function testNubileMember()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $boy = new Personage(true, 1200);
        $boy->setMesnie($mesnie);
        $mesnie->addMember($boy);
        $girl = new Personage(false, 1200);
        $girl->setMesnie($mesnie);
        $mesnie->addMember($girl);

        $this->assertEquals(0, $mesnie->getNubileMembers(1201)["men"]->count());
        $this->assertEquals(0, $mesnie->getNubileMembers(1201)["women"]->count());

        $this->assertEquals(0, $mesnie->getNubileMembers(1213)["men"]->count());
        $this->assertEquals(0, $mesnie->getNubileMembers(1213)["women"]->count());

        $this->assertEquals(0, $mesnie->getNubileMembers(1214)["men"]->count());
        $this->assertEquals(1, $mesnie->getNubileMembers(1214)["women"]->count());

        $this->assertEquals(0, $mesnie->getNubileMembers(1215)["men"]->count());
        $this->assertEquals(1, $mesnie->getNubileMembers(1215)["women"]->count());

        $this->assertEquals(1, $mesnie->getNubileMembers(1216)["men"]->count());
        $this->assertEquals(1, $mesnie->getNubileMembers(1216)["women"]->count());

        $this->assertEquals(1, $mesnie->getNubileMembers(1300)["men"]->count());
        $this->assertEquals(1, $mesnie->getNubileMembers(1300)["women"]->count());

        $boy->setSpouse($girl);
        $girl->setSpouse($boy);
        $this->assertEquals(0, $mesnie->getNubileMembers(1300)["men"]->count());
        $this->assertEquals(0, $mesnie->getNubileMembers(1300)["women"]->count());
    }

    /**
     * @throws FamilyException
     * @throws PersonageException
     * @throws \Exception
     */
    public function testInactiveMember()
    {
        $city = new City("Villeneuve");
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);
        $mesnie = new Mesnie($city);
        $worker = new Personage(true, 1200);
        $worker->setMesnie($mesnie);
        $mesnie->addMember($worker);

        $this->assertEquals(0, $mesnie->getInactiveMembers(1214)->count());

        $this->assertEquals(1, $mesnie->getInactiveMembers(1215)->count());

        $worker->setWorkField($field);
        $this->assertEquals(0, $mesnie->getInactiveMembers(1215)->count());

        $worker->setWorkField(null);
        $this->assertEquals(1, $mesnie->getInactiveMembers(1215)->count());
    }

    /**
     * @throws FamilyException
     * @throws PersonageException
     * @throws \Exception
     */
    public function testHead()
    {
        $city = new City("Villeneuve");
        $head = new Personage(true, 1200);
        $mesnie = new Mesnie($city);
        $this->assertNull($mesnie->getHead());

        try {
            $mesnie->setHead($head);
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertEquals(PersonageException::ERROR_MESNIE_HEAD, $exception->getCode());
            $this->assertNull($mesnie->getHead());
        }

        $head->setMesnie($mesnie);
        try {
            $mesnie->setHead($head);
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertEquals(PersonageException::ERROR_MESNIE_HEAD, $exception->getCode());
            $this->assertNull($mesnie->getHead());
        }

        $mesnie->addMember($head);
        $mesnie->setHead($head);
        $this->assertEquals($head, $mesnie->getHead());
    }

    /**
     * @throws FamilyException
     * @throws MarriageException
     * @throws PersonageException
     * @throws ChildException
     * @throws \Exception
     */
    public function testMemberHeir()
    {
        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city);
        $head = new Personage(true, 1200);
        $head->setMesnie($mesnie);
        $father = new Personage(true, 1180);
        $father->setDeathYear(1210);
        $head->setFather($father);
        $greatFather = new Personage(true, 1160);
        $greatFather->setDeathYear(1200);
        $greatFather->addChild($father);
        $father->setFather($greatFather);
        $uncle = new Personage(true, 1185);
        $uncle->setDeathYear(1210);
        $uncle->setFather($greatFather);
        $greatFather->addChild($uncle);

        $this->assertNull($mesnie->getMemberHeir(1250));

        $mesnie->addMember($head);
        $mesnie->setHead($head);
        $this->assertNull($mesnie->getMemberHeir(1250));

        $unrelatedYoungFemale = new Personage(false, 1230);
        $unrelatedYoungFemale->setMesnie($mesnie);
        $mesnie->addMember($unrelatedYoungFemale);
        $this->assertEquals($unrelatedYoungFemale, $mesnie->getMemberHeir(1250));

        $unrelatedOldFemale = new Personage(false, 1200);
        $unrelatedOldFemale->setMesnie($mesnie);
        $mesnie->addMember($unrelatedOldFemale);
        $this->assertEquals($unrelatedOldFemale, $mesnie->getMemberHeir(1250));

        $unrelatedOldMale = new Personage(true, 1210);
        $unrelatedOldMale->setMesnie($mesnie);
        $mesnie->addMember($unrelatedOldMale);
        $this->assertEquals($unrelatedOldMale, $mesnie->getMemberHeir(1250));

        $unrelatedYoungMale = new Personage(true, 1211);
        $unrelatedYoungMale->setMesnie($mesnie);
        $mesnie->addMember($unrelatedYoungMale);
        $this->assertEquals($unrelatedOldMale, $mesnie->getMemberHeir(1250));

        $femaleYoungCousin = new Personage(false, 1220);
        $uncle->addChild($femaleYoungCousin);
        $femaleYoungCousin->setFather($uncle);
        $femaleYoungCousin->setMesnie($mesnie);
        $mesnie->addMember($femaleYoungCousin);
        $this->assertEquals($femaleYoungCousin, $mesnie->getMemberHeir(1250));

        $femaleOldCousin = new Personage(false, 1200);
        $uncle->addChild($femaleOldCousin);
        $femaleOldCousin->setFather($uncle);
        $femaleOldCousin->setMesnie($mesnie);
        $mesnie->addMember($femaleOldCousin);
        $this->assertEquals($femaleOldCousin, $mesnie->getMemberHeir(1250));

        $maleCousin = new Personage(true, 1220);
        $uncle->addChild($maleCousin);
        $maleCousin->setFather($uncle);
        $maleCousin->setMesnie($mesnie);
        $mesnie->addMember($maleCousin);

        $wife = new Personage(false, 1235);
        $head->setSpouse($wife);
        $this->assertEquals($maleCousin, $mesnie->getMemberHeir(1250));

        $wife->setMesnie($mesnie);
        $mesnie->addMember($wife);
        $this->assertEquals($wife, $mesnie->getMemberHeir(1250));
    }
}
