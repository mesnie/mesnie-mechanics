<?php
/**
 * CityTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialValue;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use PHPUnit\Framework\TestCase;

/**
 * Class CityTest
 * @package App\Tests\Entity
 */
class CityTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testConstruct(): void
    {
        $name = "Villeneuve";
        $city = new City($name);

        $this->assertEquals($name, $city->getName());
        $this->assertEquals(strtolower($name), $city->getIdentifier());
        $this->assertEmpty($city->getMaterialValues());
    }

    /**
     * @throws MaterialException
     * @throws MoneyException
     * @throws \Exception
     */
    public function testMaterialValue()
    {
        $city = new City("Villeneuve");
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $value = new MaterialValue($city, $material, 10);
        $this->assertEmpty($city->getMaterialValues());

        $city->removeMaterialValue($value);
        $this->assertEmpty($city->getMaterialValues());

        $city->addMaterialValue($value);
        $this->assertEquals(1, $city->getMaterialValues()->count());

        $city->addMaterialValue($value);
        $this->assertEquals(1, $city->getMaterialValues()->count());

        $city->removeMaterialValue($value);
        $this->assertEmpty($city->getMaterialValues());
    }
}
