<?php
/**
 * MaterialTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\Material;
use App\Exception\MaterialException;
use PHPUnit\Framework\TestCase;

/**
 * Class MaterialTest
 * @package App\Tests\Entity
 */
class MaterialTest extends TestCase
{
    /**
     * @throws MaterialException
     */
    public function testType()
    {
        $validType = Material::TYPE_GRAIN;
        $material = new Material("wheat", $validType);
        $this->assertEquals($validType, $material->getType());

        try {
            new Material("wheat", $validType."a");
            $this->assertTrue(false);
        } catch (\Exception $exception) {
            $this->assertTrue(true);
        }
    }
}
