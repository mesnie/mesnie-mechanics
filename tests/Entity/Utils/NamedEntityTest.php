<?php
/**
 * NamedEntityTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity\Utils;

/** Usages */
use App\Entity\Utils\NamedEntity;
use PHPUnit\Framework\TestCase;

/**
 * Class NamedEntityTest
 * @package App\Tests\Entity\Utils
 */
class NamedEntityTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testNaming()
    {
        $strangeName = "éù, p__";
        $namedEntity = new NamedEntity($strangeName);
        $this->assertEquals($strangeName, $namedEntity->getName());
        $this->assertEquals("eu_p", $namedEntity->getIdentifier());
    }

    /**
     * @throws \Exception
     */
    public function testNamingFail()
    {
        $this->expectException(\Exception::class);
        $tooShortName = "abc";
        new NamedEntity($tooShortName);
    }
}
