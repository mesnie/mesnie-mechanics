<?php
/**
 * DescribedEntityTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity\Utils;

/** Usages */
use App\Entity\Utils\DescribedEntity;
use PHPUnit\Framework\TestCase;

/**
 * Class DescribedEntityTest
 *
 * @package App\Tests\Entity\Utils
 */
class DescribedEntityTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testDescription()
    {
        $description =
            "zserdctfvbughynij,ok
            fvygbuhnj
            
            vgt";
        $describedEntity = new DescribedEntity("naming", $description);
        $this->assertEquals($description, $describedEntity->getDescription());
    }

    /**
     * @throws \Exception
     */
    public function testNoDescription()
    {
        $describedEntity = new DescribedEntity("naming");
        $this->assertEquals("", $describedEntity->getDescription());
    }
}
