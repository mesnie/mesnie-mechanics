<?php
/**
 * ServerDateTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\ServerDate;
use PHPUnit\Framework\TestCase;

/**
 * Class ServerDateTest
 * @package App\Tests\Entity
 */
class ServerDateTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testUpWeek()
    {
        $serverDate = new ServerDate(1200, 12, 1);
        $this->assertEquals(1200, $serverDate->getYear());
        $this->assertEquals(12, $serverDate->getMonth());
        $this->assertEquals(1, $serverDate->getWeek());
        $this->assertTrue($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1200, $serverDate->getYear());
        $this->assertEquals(12, $serverDate->getMonth());
        $this->assertEquals(2, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1200, $serverDate->getYear());
        $this->assertEquals(12, $serverDate->getMonth());
        $this->assertEquals(3, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1200, $serverDate->getYear());
        $this->assertEquals(12, $serverDate->getMonth());
        $this->assertEquals(4, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1201, $serverDate->getYear());
        $this->assertEquals(1, $serverDate->getMonth());
        $this->assertEquals(1, $serverDate->getWeek());
        $this->assertTrue($serverDate->isNewMonth());
        $this->assertTrue($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1201, $serverDate->getYear());
        $this->assertEquals(1, $serverDate->getMonth());
        $this->assertEquals(2, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1201, $serverDate->getYear());
        $this->assertEquals(1, $serverDate->getMonth());
        $this->assertEquals(3, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1201, $serverDate->getYear());
        $this->assertEquals(1, $serverDate->getMonth());
        $this->assertEquals(4, $serverDate->getWeek());
        $this->assertFalse($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());

        $serverDate->upWeek();
        $this->assertEquals(1201, $serverDate->getYear());
        $this->assertEquals(2, $serverDate->getMonth());
        $this->assertEquals(1, $serverDate->getWeek());
        $this->assertTrue($serverDate->isNewMonth());
        $this->assertFalse($serverDate->isNewYear());
    }

    /**
     * @throws \Exception
     */
    public function testWeekDayLike()
    {
        $serverDate = new ServerDate(1200, 12, 1);
        $this->assertEquals(1, $serverDate->getWeekDayLike());
        $serverDate->upWeek();
        $this->assertEquals(8, $serverDate->getWeekDayLike());
        $serverDate->upWeek();
        $this->assertEquals(15, $serverDate->getWeekDayLike());
        $serverDate->upWeek();
        $this->assertEquals(22, $serverDate->getWeekDayLike());
    }

    /**
     * @throws \Exception
     */
    public function testEnglish()
    {
        $serverDate = new ServerDate(1200, 6, 1);
        $this->assertEquals("june", $serverDate->getMonthEnglish());
        $this->assertEquals("1200, june, week 1", $serverDate->getEnglish());

        $serverDate = new ServerDate(1201, 3, 2);
        $this->assertEquals("march", $serverDate->getMonthEnglish());
        $this->assertEquals("1201, march, week 2", $serverDate->getEnglish());

        $serverDate = new ServerDate(1202, 11, 3);
        $this->assertEquals("november", $serverDate->getMonthEnglish());
        $this->assertEquals("1202, november, week 3", $serverDate->getEnglish());

        $serverDate = new ServerDate(1203, 2, 4);
        $this->assertEquals("february", $serverDate->getMonthEnglish());
        $this->assertEquals("1203, february, week 4", $serverDate->getEnglish());
    }
}
