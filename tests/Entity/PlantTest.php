<?php
/**
 * MaterialTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\Plant;
use App\Exception\PlantException;
use PHPUnit\Framework\TestCase;

/**
 * Class PlantTest
 * @package App\Tests\Entity
 */
class PlantTest extends TestCase
{
    /**
     * @throws PlantException
     * @throws \Exception
     */
    public function testType()
    {
        $validType = "cereal";
        $plant = new Plant("wheat", $validType, 5);
        $this->assertEquals($validType, $plant->getType());

        try {
            new Plant("wheat", $validType."a", 5);
            $this->assertTrue(false);
        } catch (PlantException $exception) {
            $this->assertEquals(PlantException::TYPE_NOT_ALLOWED, $exception->getCode());
        }
    }

    /**
     * @throws PlantException
     */
    public function testGrowthTime()
    {
        $growthTime = rand(1, 20);
        $plant = new Plant("wheat", "cereal", $growthTime);
        $this->assertEquals($growthTime, $plant->getGrowthTime());
    }
}
