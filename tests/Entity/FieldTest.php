<?php
/**
 * FieldTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Field;
use App\Entity\FieldType;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\Plant;
use App\Entity\WorkType;
use App\Exception\FieldException;
use App\Exception\WorkException;
use PHPUnit\Framework\TestCase;

class FieldTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testType()
    {
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);
        $this->assertEquals($fieldType, $field->getType());

        $otherFieldType = new FieldType("labored");
        $field->setType($otherFieldType);
        $this->assertEquals($otherFieldType, $field->getType());
    }

    /**
     * @throws \Exception
     */
    public function testMesnie()
    {
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);
        $this->assertNull($field->getMesnie());

        $city = new City("Villeneuve");

        $mesnie = new Mesnie($city);
        $field->setMesnie($mesnie);
        $this->assertEquals($mesnie, $field->getMesnie());

        $field->setMesnie(null);
        $this->assertNull($field->getMesnie());
    }

    /**
     * @throws FieldException
     * @throws \Exception
     */
    public function testGrowth()
    {
        $fieldType = new FieldType("growing wheat");
        $field = new Field($fieldType);
        $this->assertNull($field->getGrowingPlant());
        $this->assertEquals(0, $field->getGrowth());
        $this->assertFalse($field->isGrown());

        try {
            $field->upGrowth();
            $this->assertTrue(false);
        } catch (FieldException $exception) {
            $this->assertEquals(FieldException::NO_GROWING_PLANT, $exception->getCode());
        }

        $plantType = new Plant("wheat", "cereal", 4);
        $field->setGrowingPlant($plantType);
        $this->assertEquals(0, $field->getGrowth());
        $this->assertEquals($plantType, $field->getGrowingPlant());
        $this->assertFalse($field->isGrown());

        $field->upGrowth();
        $this->assertEquals(1, $field->getGrowth());

        $field->upGrowth();
        $field->upGrowth();
        $this->assertEquals(3, $field->getGrowth());
        $this->assertFalse($field->isGrown());

        $field->upGrowth();
        $this->assertEquals(4, $field->getGrowth());
        $this->assertTrue($field->isGrown());
    }

    /**
     * @throws FieldException
     * @throws WorkException
     * @throws \Exception
     */
    public function testWork()
    {
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);
        $this->assertNull($field->getWorkInProgress());
        $this->assertEquals(0, $field->getProgress());
        $this->assertNull($field->getWorker());
        $this->assertFalse($field->isWorkFinished());

        try {
            $field->upProgress();
            $this->assertTrue(false);
        } catch (FieldException $exception) {
            $this->assertEquals(FieldException::NO_WORK, $exception->getCode());
        }

        $field->setWorker(null);

        $worker = new Personage(true, 1200);
        try {
            $field->setWorker($worker);
            $this->assertTrue(false);
        } catch (WorkException $exception) {
            $this->assertEquals(WorkException::ERROR_NO_WORK, $exception->getCode());
        }

        $workType = new WorkType("labor", 4, "", null);
        $field->setWorkInProgress($workType);
        $this->assertEquals($workType, $field->getWorkInProgress());
        $this->assertEquals(0, $field->getProgress());

        $field->setWorker($worker);
        $this->assertEquals($worker, $field->getWorker());

        $field->setWorker(null);
        $this->assertNull($field->getWorker());

        $field->upProgress();
        $field->upProgress();
        $field->upProgress();
        $this->assertEquals(3, $field->getProgress());
        $this->assertFalse($field->isWorkFinished());

        $field->upProgress();
        $this->assertEquals(4, $field->getProgress());
        $this->assertTrue($field->isWorkFinished());

        $field->setWorkInProgress(null);
        $this->assertNull($field->getWorkInProgress());
        $this->assertEquals(0, $field->getProgress());
    }

    public function testIsWorkFinished()
    {
        $this->assertTrue(true);
    }
}
