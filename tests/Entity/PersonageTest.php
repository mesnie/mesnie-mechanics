<?php
/**
 * PersonageTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Field;
use App\Entity\FieldType;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\Pregnancy;
use App\Exception\ChildException;
use App\Exception\FamilyException;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use PHPUnit\Framework\TestCase;

/**
 * Class PersonageTest
 * @package App\Tests\Entity
 */
class PersonageTest extends TestCase
{
    /**
     * @throws PersonageException
     */
    public function testDeathYear()
    {
        $personage = new Personage(true, 1200);
        $this->assertNull($personage->getDeathYear());

        try {
            $personage->setDeathYear(1199);
            $this->assertFalse(true);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }

        $personage->setDeathYear(1250);
        $this->assertEquals(1250, $personage->getDeathYear());


        try {
            $personage->setDeathYear(1251);
            $this->assertFalse(true);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }
    }

    /**
     * @throws PersonageException
     */
    public function testGetAge()
    {
        $personage = new Personage(true, 1200);

        $this->assertEquals(50, $personage->getAge(1250));

        try {
            $personage->getAge(1199);
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }

        $personage->setDeathYear(1220);
        $this->assertEquals(20, $personage->getAge(1250));
    }

    /**
     * @throws PersonageException
     */
    public function testFirstName()
    {
        $personage = new Personage(true, 1200);

        $this->assertNull($personage->getFirstName());

        try {
            $personage->setFirstName("Lu");
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }

        $personage->setFirstName("Luc");
        $this->assertEquals("Luc", $personage->getFirstName());

        try {
            $personage->setFirstName("Jean");
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }
    }

    /**
     * @throws ChildException
     */
    public function testFather()
    {
        $personage = new Personage(true, 1200);

        try {
            $wrongSex = new Personage(false, 1180);
            $personage->setFather($wrongSex);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PARENT_SEX, $exception->getCode());
        }

        try {
            $tooYoung = new Personage(true, 1191);
            $personage->setFather($tooYoung);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_CHILD_AGE, $exception->getCode());
        }

        $father = new Personage(true, 1180);
        $personage->setFather($father);
        $this->assertEquals($father, $personage->getFather());

        try {
            $secondFather = new Personage(true, 1180);
            $personage->setFather($secondFather);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_CHILD_ALREADY, $exception->getCode());
            $this->assertEquals($father, $personage->getFather());
        }
    }

    /**
     * @throws ChildException
     */
    public function testMother()
    {
        $personage = new Personage(true, 1200);

        try {
            $wrongSex = new Personage(true, 1180);
            $personage->setMother($wrongSex);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PARENT_SEX, $exception->getCode());
        }

        try {
            $tooYoung = new Personage(false, 1191);
            $personage->setMother($tooYoung);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_CHILD_AGE, $exception->getCode());
        }

        $mother = new Personage(false, 1180);
        $personage->setMother($mother);
        $this->assertEquals($mother, $personage->getMother());

        try {
            $secondMother = new Personage(true, 1180);
            $personage->setMother($secondMother);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_CHILD_ALREADY, $exception->getCode());
            $this->assertEquals($mother, $personage->getMother());
        }
    }

    /**
     * @throws MarriageException
     */
    public function testSpouse()
    {
        $personage = new Personage(true, 1200);
        $this->assertNull($personage->getSpouse());

        try {
            $pederast = new Personage(true, 1200);
            $personage->setSpouse($pederast);
            $this->assertTrue(false);
        } catch (MarriageException $exception) {
            $this->assertEquals(MarriageException::ERROR_MARRY_SEX, $exception->getCode());
        }

        $wife = new Personage(false, 1200);
        $personage->setSpouse($wife);
        $this->assertEquals($wife, $personage->getSpouse());

        $lover = new Personage(false, 1200);
        try {
            $personage->setSpouse($lover);
            $this->assertTrue(false);
        } catch (MarriageException $exception) {
            $this->assertEquals(MarriageException::ERROR_MARRY_ALREADY, $exception->getCode());
        }

        $personage->removeSpouse();
        $this->assertNull($personage->getSpouse());

        try {
            $personage->removeSpouse();
            $this->assertTrue(false);
        } catch (MarriageException $exception) {
            $this->assertEquals(MarriageException::ERROR_REMOVE_NOONE, $exception->getCode());
        }

        $personage->setSpouse($lover);
        $this->assertEquals($lover, $personage->getSpouse());
    }

    /**
     * @throws ChildException
     */
    public function testChildren()
    {
        $personage = new Personage(true, 1200);
        $this->assertEquals(0, $personage->getChildren()->count());

        try {
            $tooOldChild = new Personage(true, 1209);
            $personage->addChild($tooOldChild);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_CHILD_AGE, $exception->getCode());
            $this->assertEquals(0, $personage->getChildren()->count());
        }

        $youngestChild = new Personage(true, 1230);
        $personage->addChild($youngestChild);
        $oldestChild = new Personage(true, 1220);
        $personage->addChild($oldestChild);
        $middleChild = new Personage(true, 1225);
        $personage->addChild($middleChild);

        $this->assertEquals(3, $personage->getChildren()->count());
        $this->assertEquals($oldestChild, $personage->getChildren()->first());
        $this->assertEquals($middleChild, $personage->getChildren()->next());
    }

    /**
     * @throws ChildException
     */
    public function testPregnant()
    {
        $mother = new Personage(false, 1200);
        $father = new Personage(true, 1200);
        $falseMother = new Personage(false, 1200);

        $pregnancy = new Pregnancy($mother, $father);

        try {
            $father->setPregnancy($pregnancy);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PREGNANCY_MALE, $exception->getCode());
        }

        try {
            $falseMother->setPregnancy($pregnancy);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PREGNANCY_OTHER, $exception->getCode());
        }

        $mother->setPregnancy($pregnancy);
        $this->assertEquals($pregnancy, $mother->getPregnancy());

        try {
            $secondPregnancy = new Pregnancy($mother, $father);
            $mother->setPregnancy($secondPregnancy);
            $this->assertTrue(false);
        } catch (ChildException $exception) {
            $this->assertEquals(ChildException::ERROR_PREGNANCY_ALREADY, $exception->getCode());
        }
    }

    /**
     * @throws FamilyException
     * @throws \Exception
     */
    public function testMesnieAndFamily()
    {
        $city = new City("Villeneuve");
        $personage = new Personage(true, 1200);
        $mesnie = new Mesnie($city);
        $family = new Family("Capet");
        $this->assertNull($personage->getMesnie());
        $this->assertNull($personage->getFamily());

        $personage->setMesnie($mesnie);
        $this->assertEquals($mesnie, $personage->getMesnie());
        $this->assertNull($personage->getFamily());

        $mesnie->setFamily($family);
        $this->assertEquals($family, $personage->getFamily());

        $personage->setMesnie(null);
        $this->assertNull($personage->getMesnie());
        $this->assertNull($personage->getFamily());
    }

    /**
     * @throws ChildException
     */
    public function testConsanguinity()
    {
        $personage = new Personage(true, 1200);

        $father = new Personage(true, 1180);
        $personage->setFather($father);
        $father->addChild($personage);
        $this->assertEquals(-1, $personage->consanguinity($father));
        $this->assertEquals(-1, $father->consanguinity($personage));

        $greatMother = new Personage(false, 1160);
        $father->setMother($greatMother);
        $greatMother->addChild($father);
        $this->assertEquals(-2, $personage->consanguinity($greatMother));
        $this->assertEquals(-2, $greatMother->consanguinity($personage));

        $brother = new Personage(true, 1201);
        $brother->setFather($father);
        $father->addChild($brother);
        $this->assertEquals(1, $personage->consanguinity($brother));
        $this->assertEquals(1, $brother->consanguinity($personage));

        $uncle = new Personage(true, 1181);
        $uncle->setMother($greatMother);
        $greatMother->addChild($uncle);
        $this->assertEquals(2, $personage->consanguinity($uncle));
        $this->assertEquals(2, $uncle->consanguinity($personage));

        $cousin = new Personage(true, 1202);
        $cousin->setFather($uncle);
        $uncle->addChild($cousin);
        $this->assertEquals(2, $personage->consanguinity($cousin));
        $this->assertEquals(2, $cousin->consanguinity($personage));

        $spouse = new Personage(false, 1203);
        $this->assertEquals(0, $personage->consanguinity($spouse));
        $this->assertEquals(0, $spouse->consanguinity($personage));
    }

    /**
     * @throws PersonageException
     */
    public function testFertileAndNubile()
    {
        $male = new Personage(true, 1200);
        try {
            $male->isFertile(1199);
            $this->assertTrue(false);
        } catch (PersonageException $exception) {
            $this->assertTrue(true);
        }
        $this->assertFalse($male->isFertile(1213));
        $this->assertTrue($male->isFertile(1214));
        $this->assertFalse($male->isNubile(1215));
        $this->assertTrue($male->isNubile(1216));

        $female = new Personage(false, 1200);
        $this->assertFalse($female->isFertile(1211));
        $this->assertTrue($female->isFertile(1212));
        $this->assertFalse($female->isFertile(1246));
        $this->assertTrue($female->isFertile(1245));
        $this->assertFalse($female->isNubile(1213));
        $this->assertTrue($female->isNubile(1214));
        $this->assertTrue($female->isNubile(1250));
    }

    /**
     * @throws \Exception
     */
    public function testWork()
    {
        $personage = new Personage(true, 1200);
        $fieldType = new FieldType("empty");
        $field = new Field($fieldType);

        $this->assertNull($personage->getWorkField());


        $personage->setWorkField($field);
        $this->assertEquals($field, $personage->getWorkField());

        $personage->setWorkField();
        $this->assertNull($personage->getWorkField());
    }

    /**
     * @throws ChildException
     */
    public function testFamilyTree()
    {
        $personage = new Personage(true, 1200);
        $upperTree = [
            'self'      => $personage->toArray(0),
            'father'    => null,
            'mother'    => null
        ];
        $lowerTree = [
            'self'      => $personage->toArray(0),
            'children'  => []
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $father = new Personage(true, 1180);
        $personage->setFather($father);
        $father->addChild($personage);
        $upperTree["father"] = [
            "self"      => $father->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $mother = new Personage(false, 1180);
        $personage->setMother($mother);
        $mother->addChild($personage);
        $upperTree["mother"] = [
            "self"      => $mother->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $fatherOfFather = new Personage(true, 1160);
        $father->setFather($fatherOfFather);
        $upperTree["father"]["father"] = [
            "self"      => $fatherOfFather->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $motherOfFather = new Personage(false, 1160);
        $father->setMother($motherOfFather);
        $upperTree["father"]["mother"] = [
            "self"      => $motherOfFather->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $fatherOfMother = new Personage(true, 1160);
        $mother->setFather($fatherOfMother);
        $upperTree["mother"]["father"] = [
            "self"      => $fatherOfMother->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $motherOfMother = new Personage(false, 1160);
        $mother->setMother($motherOfMother);
        $upperTree["mother"]["mother"] = [
            "self"      => $motherOfFather->toArray(0),
            "father"    => null,
            "mother"    => null
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $boy = new Personage(true, 1220);
        $personage->addChild($boy);
        $lowerTree["children"][] = [
            "self"      => $boy->toArray(0),
            "children"  => []
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());

        $girlOfBoy = new Personage(false, 1240);
        $boy->addChild($girlOfBoy);
        $lowerTree["children"][0]["children"][] = [
            "self"      => $girlOfBoy->toArray(0),
            "children"  => []
        ];
        $this->assertEquals($upperTree, $personage->upperTree());
        $this->assertEquals($lowerTree, $personage->lowerTree());
        $this->assertEquals(array_merge($upperTree, $lowerTree), $personage->getFamilyTree());
    }

    /**
     * @throws ChildException
     */
    public function testSiblings()
    {
        $personage = new Personage(true, 1200);
        $father = new Personage(true, 1180);
        $personage->setFather($father);
        $father->addChild($personage);
        $mother = new Personage(false, 1180);
        $personage->setMother($mother);
        $mother->addChild($personage);

        $this->assertEquals(0, $personage->getSiblings()->count());

        $brother = new Personage(true, 1202);
        $father->addChild($brother);
        $mother->addChild($brother);
        $this->assertEquals(1, $personage->getSiblings()->count());
        $this->assertEquals($brother, $personage->getSiblings()->get(0));

        $sister = new Personage(false, 1201);
        $father->addChild($sister);
        $mother->addChild($sister);
        $this->assertEquals(2, $personage->getSiblings()->count());
        $this->assertEquals($sister, $personage->getSiblings()->get(0));
        $this->assertEquals($brother, $personage->getSiblings()->get(1));

        $halfBrother = new Personage(true, 1195);
        $father->addChild($halfBrother);
        $this->assertEquals(3, $personage->getSiblings()->count());
        $this->assertEquals($halfBrother, $personage->getSiblings()->get(0));
        $this->assertEquals($sister, $personage->getSiblings()->get(1));
        $this->assertEquals($brother, $personage->getSiblings()->get(2));

        $halfSister = new Personage(true, 1210);
        $mother->addChild($halfSister);
        $this->assertEquals(4, $personage->getSiblings()->count());
        $this->assertEquals($halfBrother, $personage->getSiblings()->get(0));
        $this->assertEquals($sister, $personage->getSiblings()->get(1));
        $this->assertEquals($brother, $personage->getSiblings()->get(2));
        $this->assertEquals($halfSister, $personage->getSiblings()->get(3));
    }

    /**
     * @throws ChildException
     * @throws MarriageException
     * @throws PersonageException
     */
    public function testHeir()
    {
        $dyingMan = new Personage(true, 1200);
        $this->assertNull($dyingMan->getHeir());

        $brother = new Personage(true, 1201);
        $father = new Personage(true, 1180);
        $father->setDeathYear(1210);
        $dyingMan->setFather($father);
        $father->addChild($dyingMan);
        $father->addChild($brother);
        $this->assertEquals($brother, $dyingMan->getHeir());

        $sonOfBrother = new Personage(true, 1221);
        $brother->addChild($sonOfBrother);
        $this->assertEquals($brother, $dyingMan->getHeir());
        $brother->setDeathYear(1230);
        $this->assertEquals($sonOfBrother, $dyingMan->getHeir());

        $wife = new Personage(false, 1200);
        $dyingMan->setSpouse($wife);
        $this->assertEquals($wife, $dyingMan->getHeir());

        $daughter = new Personage(false, 1220);
        $dyingMan->addChild($daughter);
        $this->assertEquals($daughter, $dyingMan->getHeir());

        $daughterOfDaughter = new Personage(false, 1240);
        $daughter->addChild($daughterOfDaughter);
        $this->assertEquals($daughter, $dyingMan->getHeir());
        $daughter->setDeathYear(1250);
        $this->assertEquals($daughterOfDaughter, $dyingMan->getHeir());

        $son = new Personage(true, 1221);
        $dyingMan->addChild($son);
        $grandSon = new Personage(true, 1241);
        $son->addChild($grandSon);
        $grandGrandDaughter = new Personage(false, 1261);
        $grandSon->addChild($grandGrandDaughter);
        $this->assertEquals($son, $dyingMan->getHeir());
        $son->setDeathYear(1250);
        $this->assertEquals($grandSon, $dyingMan->getHeir());
        $grandSon->setDeathYear(1270);
        $this->assertEquals($grandGrandDaughter, $dyingMan->getHeir());
    }
}
