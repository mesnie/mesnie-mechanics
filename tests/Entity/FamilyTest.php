<?php
/**
 * PersonageTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Exception\ChildException;
use App\Exception\FamilyException;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class FamilyTest
 * @package App\Tests\Entity
 */
class FamilyTest extends TestCase
{
    /**
     * @throws FamilyException
     * @throws \Exception
     */
    public function testMesnies()
    {
        $family = new Family("Capet");
        $this->assertTrue($family->getMesnies() instanceof ArrayCollection);
        $this->assertEquals(0, $family->getMesnies()->count());

        $city = new City("Villeneuve");
        $mesnie = new Mesnie($city, $family);
        $family->addMesnie($mesnie);
        $this->assertEquals(1, $family->getMesnies()->count());

        $family->removeMesnie($mesnie);
        $this->assertEquals(0, $family->getMesnies()->count());
        $this->assertNull($mesnie->getFamily());

        $anotherFamily = new Family("Plantagenêt");
        $mesnie->setFamily($anotherFamily);
        try {
            $family->addMesnie($mesnie);
            $this->assertTrue(false);
        } catch (FamilyException $exception) {
            $this->assertEquals(FamilyException::OTHER_FAMILY, $exception->getCode());
        }
    }

    /**
     * @throws FamilyException
     * @throws \Exception
     */
    public function testPatriarch()
    {
        $city = new City("Villeneuve");

        $family = new Family("Capet");
        $this->assertNull($family->getPatriarch());

        $anotherFamily = new Family("Plantagenêt");
        $mesnie = new Mesnie($city, $anotherFamily);
        $patriarch = new Personage(true, 1200);
        $patriarch->setMesnie($mesnie);
        try {
            $family->setPatriarch($patriarch);
            $this->assertTrue(false);
        } catch (FamilyException $exception) {
            $this->assertEquals(FamilyException::OTHER_FAMILY, $exception->getCode());
        }

        $mesnie->setFamily($family);
        $family->setPatriarch($patriarch);
        $this->assertEquals($patriarch, $family->getPatriarch());
    }

    /**
     * @throws FamilyException
     * @throws ChildException
     * @throws \Exception
     */
    public function testHeir()
    {
        $city = new City("Villeneuve");

        $family = new Family("Capet");
        $mesnie1 = new Mesnie($city, $family);
        $family->addMesnie($mesnie1);
        $mesnie2 = new Mesnie($city, $family);
        $family->addMesnie($mesnie2);
        $patriarch = new Personage(true, 1200);
        $patriarch->setMesnie($mesnie1);
        $mesnie1->addMember($patriarch);
        $family->setPatriarch($patriarch);
        $this->assertNull($family->getHeir());

        $youngMan = new Personage(true, 1210);
        $youngMan->setMesnie($mesnie2);
        $mesnie2->addMember($youngMan);
        $this->assertEquals($youngMan, $family->getHeir());

        $oldWoman = new Personage(false, 1180);
        $oldWoman->setMesnie($mesnie1);
        $mesnie1->addMember($oldWoman);
        $this->assertEquals($youngMan, $family->getHeir());

        $oldMan = new Personage(true, 1180);
        $oldMan->setMesnie($mesnie1);
        $mesnie1->addMember($oldMan);
        $this->assertEquals($oldMan, $family->getHeir());

        $sonOfPatriarch = new Personage(true, 1220);
        $patriarch->addChild($sonOfPatriarch);
        $this->assertEquals($sonOfPatriarch, $family->getHeir());
    }
}
