<?php
/**
 * MaterialValueTest.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Entity;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialValue;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use PHPUnit\Framework\TestCase;

/**
 * Class MaterialValueTest
 * @package App\Tests
 */
class MaterialValueTest extends TestCase
{
    /**
     * @throws MaterialException
     * @throws MoneyException
     * @throws \Exception
     */
    public function testConstruct(): void
    {
        $city = new City("Villeneuve");
        $material = new Material("wheat", Material::TYPE_GRAIN);
        $value = 10.37;
        $materialValue = new MaterialValue($city, $material, $value);

        $this->assertEquals($city, $materialValue->getCity());
        $this->assertEquals($material, $materialValue->getMaterial());
        $this->assertEquals($value, $materialValue->getValue());
    }

}
