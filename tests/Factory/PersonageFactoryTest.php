<?php
/**
 * PersonageFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Tests\Factory;

/** Usages */
use App\Exception\PersonageException;
use App\Factory\Names\FrenchNames;
use App\Factory\PersonageFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class PersonageFactoryTest
 * @package App\Tests\Factory
 */
class PersonageFactoryTest extends TestCase
{
    /**
     * @throws PersonageException
     */
    public function testCreateNubileMan()
    {
        $nubileFrenchMan = PersonageFactory::createNubileMan(1200, "fr");
        $this->assertTrue(in_array($nubileFrenchMan->getFirstName(), FrenchNames::MALE_FRENCH_NAMES));
        $this->assertTrue($nubileFrenchMan->isMale());
        $this->assertTrue($nubileFrenchMan->isNubile(1200));
    }

    /**
     * @throws PersonageException
     */
    public function testCreateNubileWoman()
    {
        $nubileFrenchWoman = PersonageFactory::createNubileWoman(1200, "fr");
        $this->assertTrue(in_array($nubileFrenchWoman->getFirstName(), FrenchNames::FEMALE_FRENCH_NAMES));
        $this->assertFalse($nubileFrenchWoman->isMale());
        $this->assertTrue($nubileFrenchWoman->isNubile(1200));
    }
}
