<?php
/**
 * MaterialQuantityRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\MaterialQuantity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class MaterialQuantityRepository
 * @package App\Repository
 *
 * @method MaterialQuantity|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialQuantity|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialQuantity[]    findAll()
 * @method MaterialQuantity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialQuantityRepository extends ServiceEntityRepository
{
    /**
     * MaterialQuantityRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MaterialQuantity::class);
    }
}
