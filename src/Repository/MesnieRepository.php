<?php
/**
 * MesnieRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\Mesnie;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class MesnieRepository
 * @package App\Repository
 *
 * @method Mesnie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mesnie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mesnie[]    findAll()
 * @method Mesnie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MesnieRepository extends ServiceEntityRepository
{
    /**
     * MesnieRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Mesnie::class);
    }

    /**
     * @param User $user
     * @return Mesnie|null
     */
    public function getFromUser(User $user): ?Mesnie
    {
        return $this->findOneBy([
            "user" => $user
        ]);
    }
}
