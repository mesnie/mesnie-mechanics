<?php
/**
 * FieldRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\Field;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class FieldRepository
 * @package App\Repository
 *
 * @method Field|null find($id, $lockMode = null, $lockVersion = null)
 * @method Field|null findOneBy(array $criteria, array $orderBy = null)
 * @method array|Field[]    findAll()
 * @method array|Field[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldRepository extends ServiceEntityRepository
{
    /**
     * FieldRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Field::class);
    }

    /**
     * Returns all Fields on which a Personage is working.
     *
     * @return ArrayCollection
     */
    public function getAllWithWorkers() : ArrayCollection
    {
        return new ArrayCollection(
            $this
                ->createQueryBuilder("field")
                ->where("field.worker IS NOT NULL")
                ->getQuery()
                ->getResult()
        );
    }
}
