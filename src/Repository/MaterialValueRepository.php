<?php
/**
 * MaterialValueRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\MaterialValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MaterialValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaterialValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaterialValue[]    findAll()
 * @method MaterialValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialValueRepository extends ServiceEntityRepository
{
    /**
     * MaterialValueRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MaterialValue::class);
    }
}
