<?php
/**
 * ServerDateRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\ServerDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ServerDateRepository
 * @package App\Repository
 *
 * @method ServerDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServerDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServerDate[]    findAll()
 * @method ServerDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServerDateRepository extends ServiceEntityRepository
{
    /**
     * ServerDateRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServerDate::class);
    }

    /**
     * Return the current server date.
     *
     * @return ServerDate
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentDate(): ServerDate
    {
        return $this->createQueryBuilder("currentDate")
            ->getQuery()
            ->getSingleResult();
    }
}
