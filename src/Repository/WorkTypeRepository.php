<?php
/**
 * WorkTypeRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\FieldType;
use App\Entity\WorkType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class WorkTypeRepository
 * @package App\Repository
 *
 * @method WorkType|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkType|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkType[]    findAll()
 * @method WorkType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkTypeRepository extends ServiceEntityRepository
{
    /**
     * WorkTypeRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WorkType::class);
    }

    /**
     * Returns all the workType availables on a FieldType.
     *
     * @param FieldType $fieldType
     * @return ArrayCollection
     *
     * @todo test
     */
    public function findByFieldType(FieldType $fieldType): ArrayCollection
    {
        return $this
            ->createQueryBuilder("workType")
            ->where("workType.fieldType = :fieldType")
            ->setParameter("fieldType", $fieldType)
            ->orderBy("workType.identifier", "ASC")
            ->getQuery()
            ->getResult();
    }
}
