<?php
/**
 * PregnancyRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\Pregnancy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class PregnancyRepository
 * @package App\Repository
 *
 * @method Pregnancy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pregnancy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pregnancy[]    findAll()
 * @method Pregnancy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PregnancyRepository extends ServiceEntityRepository
{
    /**
     * PregnancyRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pregnancy::class);
    }
}
