<?php
/**
 * PersonageRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\Personage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class PersonageRepository
 * @package App\Repository
 *
 * @method Personage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personage[]    findAll()
 * @method Personage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonageRepository extends ServiceEntityRepository
{
    /**
     * PersonageRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Personage::class);
    }

    /**
     * Return Collection of fertile women,
     * i.e. female personage older than 12 and younger than 45.
     *
     * @param int $currentYear
     * @return ArrayCollection|Personage[]
     */
    public function getFertileWomen(int $currentYear): ArrayCollection
    {
        $arrayResults = $this
            ->createQueryBuilder("woman")
            ->where("woman.isMale = 0")
            ->andWhere("woman.deathYear is NULL")
            ->andWhere("woman.birthYear <= :youngestBirth")
            ->andWhere("woman.birthYear >= :oldestBirth")
            ->setParameter("youngestBirth", $currentYear - Personage::FERTILITY_FEMALE)
            ->setParameter("oldestBirth", $currentYear - Personage::MENOPAUSE)
            ->getQuery()
            ->getResult()
            ;
        return new ArrayCollection($arrayResults);
    }
}
