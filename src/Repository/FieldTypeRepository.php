<?php
/**
 * FieldTypeRepository.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Repository;

/** Usages */
use App\Entity\FieldType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class FieldTypeRepository
 * @package App\Repository
 *
 * @method FieldType|null find($id, $lockMode = null, $lockVersion = null)
 * @method FieldType|null findOneBy(array $criteria, array $orderBy = null)
 * @method FieldType[]    findAll()
 * @method FieldType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldTypeRepository extends ServiceEntityRepository
{
    /**
     * FieldTypeRepository constructor.
     * Autowire Registry.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FieldType::class);
    }
}
