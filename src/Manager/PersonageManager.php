<?php
/**
 * PersonageManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use App\Entity\Personage;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use App\Service\MarriageService;
use Doctrine\ORM\ORMException;

/**
 * Class PersonageManager
 * @package App\Manager
 */
class PersonageManager extends BaseManager
{
    /**
     * Marry the two Personages, add the female to the male's mesnie, and save it.
     * Checks the sex of the two Personages and if they have an impeachment.
     *
     * @param Personage $male
     * @param Personage $female
     * @throws PersonageException
     * @throws MarriageException
     * @throws ORMException
     */
    public function marriage(Personage $male, Personage $female): void
    {
        if (!$male->isMale() || $female->isMale()) {
            throw new MarriageException(
                "The personage ".$male->getId()." is a female or ".$female->getId()." is a male",
                MarriageException::ERROR_MARRY_SEX,
                $male,
                $female
            );
        }

        $empeachment = MarriageService::marriageImpeachment($male, $female);
        if (0 !== $empeachment) {
            throw new MarriageException(
                "The personages ".$male->getId()." and ".$female->getId()." have an empeachment (code $empeachment)",
                MarriageException::ERROR_MARRY_EMPEACHMT,
                $male,
                $female
            );
        }

        $male->setSpouse($female);
        $female->setSpouse($male);
        $female->setMesnie($male->getMesnie());

        $this->persist($male);
        $this->persist($female);
        $this->flush();
    }
}
