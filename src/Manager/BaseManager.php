<?php
/**
 * BaseManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class BaseManager
 * Inherited by all managers, offer a layer over the entityManager.
 *
 * @package App\Manager
 */
abstract class BaseManager
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    /**
     * BaseManager constructor.
     * Autowire entityManager.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Call the entityManager to persist the given entity.
     *
     * @param $entity
     * @throws ORMException
     */
    public function persist($entity): void
    {
        $this->entityManager->persist($entity);
    }

    /**
     * Call the entityManager to remove the given entity.
     *
     * @param $entity
     * @throws ORMException
     */
    public function remove($entity): void
    {
        $this->entityManager->remove($entity);
    }

    /**
     * Call the entityManager to flush all changes.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function flush(): void
    {
        $this->entityManager->flush();
    }

    /**
     * Call the entityManager to get the repository corresponding to the given class.
     *
     * @param string $entityName
     * @return EntityRepository
     */
    protected function getRepository(string $entityName): EntityRepository
    {
        return $this->entityManager->getRepository($entityName);
    }
}
