<?php
/**
 * MesnieManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Exception\FamilyException;
use App\Exception\PersonageException;
use App\Factory\MesnieFactory;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class MesnieManager
 * @package App\Manager
 */
class MesnieManager extends BaseManager
{
    /**
     * @param Personage $head
     * @param int       $year
     * @param string    $newFamilyName
     * @return Mesnie
     * @throws FamilyException 201
     * @throws ORMException
     * @throws PersonageException 161
     * @throws OptimisticLockException
     */
    public function fundMesnie(Personage $head, int $year, string $newFamilyName = null): Mesnie
    {
        $city = $head->getMesnie()->getCity();
        $this->removePersonageFromMesnie($head, $year);

        $newEntities = MesnieFactory::fundMesnie($head, $city, $newFamilyName);

        foreach ($newEntities as $newEntity) {
            $this->persist($newEntity);
        }
        $this->flush();

        return $newEntities["mesnie"];
    }

    /**
     * Remove a Personage from its Mesnie.
     * If head of the Mesnie, call the process of head removal.
     * Save it.
     *
     * @param Personage $perso
     * @param int       $year
     * @return Mesnie|null
     * @throws ORMException
     * @throws PersonageException
     * @throws OptimisticLockException
     */
    private function removePersonageFromMesnie(Personage $perso, int $year): ?Mesnie
    {
        $mesnie = $perso->getMesnie();
        if ($perso->isMesnieHead()) {
            $mesnie = $this->removeHead($mesnie, $year);
        }

        $perso->setMesnie(null);
        $this->persist($perso);
        $this->flush();

        return $mesnie;
    }

    /**
     * If the Mesnie has an heir, remove the current head and replace it by the heir.
     * Else, just destroy the mesnie.
     * Save it.
     *
     * @param Mesnie    $mesnie
     * @param int       $year
     * @return Mesnie|null
     * @throws ORMException
     * @throws PersonageException
     */
    private function removeHead(Mesnie $mesnie, int $year): ?Mesnie
    {
        $heir = $mesnie->getMemberHeir($year);
        if ($heir) {
            $mesnie->removeMember($mesnie->getHead());
            $mesnie->setHead($heir);
            $this->persist($mesnie);
            $this->flush();
        } else {
            $this->destroyMesnie($mesnie);
            $mesnie = null;
        }

        return $mesnie;
    }

    /**
     * Remove the mesnie from all its members,
     * remove the mesnie from all its fields
     * and delete the mesnie itself.
     * (the inventory shall be autoremoved by doctrine)
     *
     * @param Mesnie $mesnie
     * @throws ORMException
     */
    private function destroyMesnie(Mesnie $mesnie): void
    {
        foreach ($mesnie->getMembers() as $member) {
            $member->setMesnie(null);
            $this->persist($member);
        }

        foreach ($mesnie->getFields() as $field) {
            $field->setMesnie(null);
            $this->persist($field);
        }

        $this->remove($mesnie);
        $this->flush();
    }
}
