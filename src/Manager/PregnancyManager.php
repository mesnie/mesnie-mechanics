<?php
/**
 * PregnancyManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use App\Entity\Personage;
use App\Entity\Pregnancy;
use App\Exception\ChildException;
use App\Exception\FamilyException;
use App\Exception\PersonageException;
use App\Factory\PersonageFactory;
use App\Factory\PregnancyFactory;
use App\Repository\PersonageRepository;
use App\Repository\PregnancyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class PregnancyManager
 * @package App\Manager
 */
class PregnancyManager extends BaseManager
{
    /**
     * Return the PregnancyRepository
     *
     * @return PregnancyRepository
     */
    private function getRepo(): PregnancyRepository
    {
        /** @var PregnancyRepository $pregnancyRepo */
        $pregnancyRepo = $this->getRepository(Pregnancy::class);

        return $pregnancyRepo;
    }

    /**
     * Create and save a pregnancy.
     *
     * @param $mother
     * @param $father
     * @return Pregnancy
     * @throws ChildException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function setPregnant($mother, $father): Pregnancy
    {
        $pregnancy = PregnancyFactory::getPregnant($mother, $father);
        $this->persist($pregnancy);
        $this->flush();

        return $pregnancy;
    }

    /**
     * MAY create and save a pregnancy.
     *
     * @param Personage $mother
     * @param Personage $father
     * @return bool
     * @throws ChildException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function randomPregnancy(Personage $mother, Personage $father): bool
    {
        if (1 === rand(1, Pregnancy::PREGNANCY_PROBABILITY)) {
            $this->setPregnant($mother, $father);
            return true;
        }

        return false;
    }

    /**
     * Each woman who may get pregnant has a chance to be.
     *
     * @param int $year
     * @return int the count of women who get pregnant.
     * @throws PersonageException
     * @throws ChildException
     * @throws ORMException
     */
    public function randomPregnancies(int $year): int
    {
        $count = 0;

        /** @var PersonageRepository $persoRepo */
        $persoRepo = $this->getRepository(Personage::class);
        $fertileWomen = $persoRepo->getFertileWomen($year);

        /** @var Personage $fertileWoman */
        foreach ($fertileWomen as $fertileWoman) {
            if (PregnancyFactory::mayGetPregnant($fertileWoman, $fertileWoman->getSpouse(), $year)) {
                if ($this->randomPregnancy($fertileWoman, $fertileWoman->getSpouse())) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Given a pregnancy, deliver it,
     * i.e. create one or more children with its parents
     * and remove the pregnancy.
     *
     * @param Pregnancy $pregnancy
     * @param int $year
     * @return ArrayCollection|Personage[] the children born from the delivery.
     * @throws ChildException
     * @throws FamilyException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function delivery(Pregnancy $pregnancy, int $year): ArrayCollection
    {
        $children = new ArrayCollection();
        $childrenNumber = $pregnancy->getChildrenNumber();
        while (0 < $childrenNumber) {
            $child = PersonageFactory::deliverChild($pregnancy, $year);
            $pregnancy->getMother()->addChild($child);
            $pregnancy->getFather()->addChild($child);
            $pregnancy->getMother()->getMesnie()->addMember($child);
            $children->add($child);
            $this->persist($child);
            $this->persist($pregnancy->getMother()->getMesnie());
            $childrenNumber--;
        }
        $this->remove($pregnancy);
        $this->flush();

        return $children;
    }

    /**
     * Up the month of a pregnancy.
     * If 9th month, deliver it.
     *
     * @param Pregnancy $pregnancy
     * @param int $year
     * @throws ChildException
     * @throws FamilyException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function upPregnancyMonth(Pregnancy $pregnancy, int $year): void
    {
        $pregnancy->upMonth();
        if (9 <= $pregnancy->getMonth()) {
            $this->delivery($pregnancy, $year);
        } else {
            $this->persist($pregnancy);
            $this->flush();
        }
    }

    /**
     * Up the month of all pregnancies
     * and deliver those who are in the 9th month.
     *
     * @param int $year
     * @throws ChildException
     * @throws FamilyException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upAllPregnanciesMonth(int $year): void
    {
        $allPregnancies = $this->getRepo()->findAll();
        foreach ($allPregnancies as $pregnancy) {
            $this->upPregnancyMonth($pregnancy, $year);
        }
    }
}
