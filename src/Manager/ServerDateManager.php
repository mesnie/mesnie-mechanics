<?php
/**
 * ServerDateManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use App\Entity\ServerDate;
use App\Exception\DateException;
use App\Repository\ServerDateRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

/**
 * Class DateManager
 * @package App\Manager
 */
class ServerDateManager extends BaseManager
{
    /**
     * Return the ServerDate repository.
     *
     * @return ServerDateRepository
     */
    private function getRepo() : ServerDateRepository
    {
        /** @var ServerDateRepository $serverDateRepo */
        $serverDateRepo = $this->getRepository(ServerDate::class);

        return $serverDateRepo;
    }

    /**
     * Create and save a new ServerDate.
     * Shall not be used if one already exists.
     * Default to 1st january 1200.
     *
     * @param int $year
     * @param int $month
     * @param int $week
     * @return ServerDate
     * @throws DateException
     */
    private function createDate(int $year = 1200, int $month = 1, int $week = 1): ServerDate
    {
        $serverDate = new ServerDate($year, $month, $week);
        try {
            $this->persist($serverDate);
            $this->flush();
        } catch (ORMException $exception) {
            throw new DateException($exception->getMessage(), DateException::CANNOT_CREATE);
        }

        return $serverDate;
    }

    /**
     * Return the current ServerDate.
     * If none, create it before returning it.
     *
     * @param LoggerInterface|null $logger
     * @return ServerDate
     * @throws DateException
     */
    public function currentDate(?LoggerInterface $logger = null): ServerDate
    {
        try {
            $currentDate = $this->getRepo()->getCurrentDate();
        } catch (NoResultException $exception) {
            $currentDate = $this->createDate();
            $logger->info("date created");
        } catch (NonUniqueResultException $exception) {
            throw new DateException($exception->getMessage(), DateException::NON_UNIQUE);
        }

        return $currentDate;
    }

    /**
     * Add a week to the ServerDate and save it.
     *
     * @return ServerDate
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function nextWeek(): ServerDate
    {
        $date = $this->currentDate();
        $date->upWeek();
        $this->persist($date);
        $this->flush();

        return $date;
    }
}
