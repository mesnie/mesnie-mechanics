<?php
/**
 * FieldManager.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Manager;

/** Usages */
use App\Entity\Field;
use App\Entity\Personage;
use App\Entity\WorkType;
use App\Exception\FieldException;
use App\Exception\MaterialException;
use App\Exception\WorkException;
use App\Repository\FieldRepository;
use App\Service\WorkService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class FieldManager
 * @package App\Manager
 */
class FieldManager extends BaseManager
{
    /**
     * Syntax sugar to get the FieldRepository.
     *
     * @return FieldRepository
     */
    public function getFieldRepo() : FieldRepository
    {
        /** @var FieldRepository $fieldRepo */
        $fieldRepo =  $this->getRepository(Field::class);

        return $fieldRepo;
    }

    /**
     * Returns an array of all fields in the database.
     *
     * @return array|Field[]
     */
    private function getAllFields() : array
    {
        return $this->getFieldRepo()->findAll();
    }

    /**
     * Returns an ArrayCollection of all fields with someone working on it.
     *
     * @return ArrayCollection|Field[]
     */
    private function getFieldsWithWorkers() : ArrayCollection
    {
        return $this->getFieldRepo()->getAllWithWorkers();
    }

    /**
     * From a given Field, if it has a Plant, up its growth
     * and, if the plant is grown, get the result from the growth :
     * change the type of the Field and remove the Plant.
     * Return true if the field had a Plant.
     *
     * @param Field $field
     * @return bool
     * @throws FieldException
     */
    private function upGrowth(Field $field): bool
    {
        $plant = $field->getGrowingPlant();
        if ($plant) {
            $field->upGrowth();
        }

        return (null !== $plant);
    }

    /**
     * Get all Fields and apply the upGrowth() on each of them,
     * growing the Plant if it has one
     * and setting the growth result if its grown.
     * Save the changes to database.
     * Return the count of plants having grown up.
     *
     * @return int
     * @throws FieldException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function allGrowth(): int
    {
        $fields = $this->getAllFields();

        $count = 0;
        foreach ($fields as $field) {
            if ($this->upGrowth($field)) {
                $count++;
            }
        }

        $this->flush();

        return $count;
    }

    /**
     * On the given Field,
     * set the given WorkType as workInProgress,
     * set the given Personage as worker,
     * remove the materials needed by the WorkType from the mesnie owning the Field.
     * Save the changes to database.
     * Before, check that the Field does not already have a workInProgress,
     * that the workType is ok with the fieldType
     * And that the mesnie owning the field has the needed materials.
     *
     * @param Field     $field
     * @param WorkType  $workType
     * @param Personage $worker
     * @return Field
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WorkException
     * @throws MaterialException
     */
    public function newWorkOnField(Field $field, WorkType $workType, Personage $worker): Field
    {
        if ($field->getWorkInProgress()) {
            throw new WorkException(
                "The field ".$field->getId()." already has work "
                .$field->getWorkInProgress()->getIdentifier()." in progress",
                WorkException::ERROR_FIELD_ALREADY,
                $workType
            );
        }
        if ($workType->getFieldType() && ($workType->getFieldType() !== $field->getType())) {
            throw new WorkException(
                "The field ".$field->getId()." is of type ".$field->getType()->getIdentifier()
                .", incompatible with the workType ".$workType->getIdentifier(),
                WorkException::ERROR_FIELD_TYPE,
                $workType
            );
        }
        if ($workType->getPlantNeeded() &&
            ($workType->getPlantNeeded() !== $field->getGrowingPlant() || !$field->isGrown())
        ) {
            throw new WorkException(
                "The field ".$field->getId()
                ." does not have a fully grown ".$workType->getPlantNeeded()->getIdentifier()
                .", needed by the workType ".$workType->getIdentifier(),
                WorkException::ERROR_MISSING_PLANT,
                $workType
            );
        }
        if (!WorkService::hasMesnieEnoughMaterial($workType, $field->getMesnie())) {
            throw new WorkException(
                "The field ".$field->getId()
                ." needs materials"
                .", for work ".$workType->getIdentifier()
                .", but mesnie ".$field->getMesnie()->getId()." do not have enough.",
                WorkException::ERROR_NEED_ENOUGH,
                $workType
            );
        }

        $mesnie = WorkService::removeMaterialNeededFromMesnie($workType, $field->getMesnie());
        $this->persist($mesnie);

        $field->setWorkInProgress($workType);
        $field->setProgress(0);
        $field->setWorker($worker);
        $this->persist($field);

        $this->flush();

        return $field;
    }

    /**
     * Set the given Personage as worker on the given Field
     * and save to database.
     * Checks that the given Field has a workInProgress.
     *
     * @param Field $field
     * @param Personage $worker
     * @return Field
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WorkException
     */
    public function setWorkerOnField(Field $field, Personage $worker): Field
    {
        if (!$field->getWorkInProgress()) {
            throw new WorkException(
                "The field ".$field->getId()." has no work in progress,"
                ." one shall be selected",
                WorkException::ERROR_NO_WORK,
                null
            );
        }

        $field->setWorker($worker);

        $this->persist($field);
        $this->flush();

        return $field;
    }

    /**
     * Reset the workInProgress from the given Field to null,
     * its worker to null
     * and its progress to 0.
     * Save the changes to database.
     * Checks that the given Field has a workInProgress.
     *
     * @param Field $field
     * @return Field
     * @throws WorkException
     * @throws ORMException
     */
    public function cancelWorkInProgress(Field $field): Field
    {
        if (!$field->getWorkInProgress()) {
            throw new WorkException(
                "The field ".$field->getId()." has no work in progress to cancel",
                WorkException::ERROR_NOTHING_CANCEL,
                null
            );
        }

        $field->setWorkInProgress(null);
        $field->setWorker(null);
        $field->setProgress(0);

        $this->persist($field);
        $this->flush();

        return $field;
    }

    /**
     * Called on a Field having achieved its workInProgress.
     * Give to the Mesnie owning the Field the material results from the workInProgress,
     * reset the FieldType according to the work achieved if needed
     * reset the growingPlant according to the work achieved
     * reset the workInProgress to null and the progress to 0.
     * Save changes to database.
     * Checks that the given Field has a workInProgress.
     *
     * @param Field $field
     * @return Field
     * @throws MaterialException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WorkException
     */
    private function workResult(Field $field): Field
    {
        if (!$field->getWorkInProgress()) {
            throw new WorkException(
                "The field ".$field->getId()." has no work to get result from",
                WorkException::ERROR_NO_WORK,
                null
            );
        }

        $mesnie = WorkService::addMaterialResultToMesnie($field->getWorkInProgress(), $field->getMesnie());
        $this->persist($mesnie);

        $newFieldType = $field->getWorkInProgress()->getFieldTypeResult();
        if ($newFieldType) {
            $field->setType($newFieldType);
        }

        $field->setGrowingPlant($field->getWorkInProgress()->getPlantResult());

        $field->setProgress(0);
        $field->setWorkInProgress(null);
        $this->persist($field);
        $this->flush();

        return $field;
    }

    /**
     * If the given Field has no worker, return false.
     * Else, checks that it has a workInprogress, up its progress and return true.
     * If the work is achieved, also get the results from it,
     * i.e. change fieldType, change growingPlant, reset workInProgress and progress.
     * Save changes to database.
     *
     * @param Field $field
     * @return bool
     * @throws FieldException
     * @throws MaterialException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WorkException
     */
    public function progressWork(Field $field): bool
    {
        if (!$field->getWorker()) {
            return false;
        }

        if (!$field->getWorkInProgress()) {
            throw new WorkException(
                "The field ".$field->getId()." has a worker ".$field->getWorker()->getId()
                .", but no work in progress",
                WorkException::ERROR_WORKER_IDLE,
                null
            );
        }

        $field->upProgress();
        $field->setWorker(null);
        if ($field->isWorkFinished()) {
            $field = $this->workResult($field);
        }

        $this->persist($field);
        $this->flush();

        return true;
    }

    /**
     * Get all Fields with workers
     * and up the progress on each on them,
     * calling the work results if the work is achieved.
     * Save changes to database.
     * Return the count of works having progressed.
     *
     * @return int
     * @throws FieldException
     * @throws MaterialException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws WorkException
     */
    public function progressAllWork(): int
    {
        $count = 0;
        $fields = $this->getFieldsWithWorkers();
        foreach ($fields as $field) {
            if ($this->progressWork($field)) {
                $count++;
            }
        }

        return $count;
    }
}
