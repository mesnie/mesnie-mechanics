<?php
/**
 * PregnancyFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\Personage;
use App\Entity\Pregnancy;
use App\Exception\ChildException;
use App\Exception\PersonageException;

/**
 * Class PregnancyFactory
 * @package App\Factory
 */
class PregnancyFactory
{

    /**
     * Creates a pregnancy with one child.
     *
     * @param Personage $mother
     * @param Personage $father
     * @return Pregnancy
     * @throws ChildException
     */
    public static function getPregnant(Personage $mother, Personage $father): Pregnancy
    {
        $pregnancy = new Pregnancy($mother, $father, 0, 1);

        return $pregnancy;
    }

    /**
     * @param Personage $mother
     * @param Personage $father
     * @param int $year
     * @return bool
     * @throws PersonageException
     */
    public static function mayGetPregnant(Personage $mother, ?Personage $father, int $year): bool
    {
        return (
            $father
            && is_null($mother->getPregnancy())
            && $mother->isFertile($year)
            && $father->isFertile($year)
            && $mother->getMesnie()
            && $mother->getMesnie() === $father->getMesnie()
        );
    }
}
