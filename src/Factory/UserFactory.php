<?php
/**
 * UserFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\User;
use App\Exception\UserException;

/**
 * Class UserFactory
 * @package App\Factory
 */
class UserFactory
{
    /**
     * @param string $email
     * @return User
     * @throws UserException
     */
    public static function createUser(string $email): User
    {
        $user = new User($email);
        $user->setRoles(['ROLE_USER']);

        return $user;
    }

    /**
     * @param string $email
     * @return User
     * @throws UserException
     */
    public static function createAdmin(string $email): User
    {
        $user = new User($email);
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);

        return $user;
    }
}
