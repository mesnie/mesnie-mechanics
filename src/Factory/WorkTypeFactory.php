<?php
/**
 * WorkTypeFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\FieldType;
use App\Entity\Plant;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class WorkTypeFactory
 * Factory used to set the WorkTypes from the DataFixtures.
 *
 * @package App\Factory
 */
class WorkTypeFactory
{
    /**
     * Parse the given JSON file to build a Collection of WorkTypes.
     * Need the Collection of FieldTypes to set its fieldType and fieldTypeResults,
     * the Collection of Materials to it its materialNeeds and materialResults
     * and the Collection of Plants to set the plantResults.
     *
     * @param string          $path
     * @param ArrayCollection $fieldTypes
     * @param ArrayCollection $materials
     * @param ArrayCollection $plants
     * @return ArrayCollection
     * @throws MaterialException
     * @throws \Exception
     */
    public static function workTypesFromJsonFile(
        string          $path,
        ArrayCollection $fieldTypes,
        ArrayCollection $materials,
        ArrayCollection $plants
    ): ArrayCollection {
        $workTypes = new ArrayCollection();

        try {
            $json = file_get_contents($path);
            $decoded = json_decode($json);
        } catch (\Exception $exception) {
            throw new \Exception("unable to decode JSON from $path");
        }

        foreach ($decoded->workTypes as $workTypeObject) {
            $fieldType = self::createWorkTypeFromObject($workTypeObject, $fieldTypes, $materials, $plants);
            $workTypes->add($fieldType);
        }

        return $workTypes;
    }

    /**
     * Create a WorkType from a given object.
     * Need the Collection of FieldTypes to set its fieldType and fieldTypeResult,
     * the Collection of Materials to set its materialNeeds and materialResults
     * and the Collection of Plants to set its plantResult.
     *
     * @param                 $object
     * @param ArrayCollection $fieldTypes
     * @param ArrayCollection $materials
     * @param ArrayCollection $plants
     * @return WorkType
     * @throws MaterialException
     * @throws \Exception
     */
    public static function createWorkTypeFromObject(
        $object,
        ArrayCollection $fieldTypes,
        ArrayCollection $materials,
        ArrayCollection $plants
    ): WorkType {
        $fieldType = self::getFieldType(
            $object->fieldTypeIdentifier,
            $fieldTypes
        );
        $fieldTypeResult = self::getFieldType(
            $object->fieldTypeResultIdentifier,
            $fieldTypes
        );
        $plantResult = self::getPlant(
            $object->plantResult,
            $plants
        );
        $plantNeeded = self::getPlant(
            $object->plantNeeded,
            $plants
        );
        $workType = new WorkType(
            $object->name,
            $object->weeksNeeded,
            $object->description,
            $fieldType,
            $fieldTypeResult,
            $plantResult,
            $plantNeeded
        );
        $workType = self::addMaterialsToWorkType(
            $workType,
            $object->materialNeeded,
            $object->materialResult,
            $materials
        );

        return $workType;
    }

    /**
     * Create all MaterialQuantities defined by the dataNeeded and dataResult,
     * set the first as materials needed by the given WorkType
     * and the second as materials resulting from it.
     *
     * @param WorkType $workType
     * @param array $dataNeeded
     * @param array $dataResult
     * @param ArrayCollection $materialDefinitions
     * @return WorkType
     * @throws MaterialException
     */
    private static function addMaterialsToWorkType(
        WorkType        $workType,
        array           $dataNeeded,
        array           $dataResult,
        ArrayCollection $materialDefinitions
    ) : WorkType {

        $materialsNeeded = MaterialFactory::createMaterialQuantitiesFromArrayOfObjects(
            $dataNeeded,
            $materialDefinitions,
            $workType
        );
        $materialsResult = MaterialFactory::createMaterialQuantitiesFromArrayOfObjects(
            $dataResult,
            $materialDefinitions,
            null,
            $workType
        );

        foreach ($materialsNeeded as $materialNeeded) {
            $workType->addMaterialNeed($materialNeeded);
        }
        foreach ($materialsResult as $materialResult) {
            $workType->addMaterialResult($materialResult);
        }

        return $workType;
    }

    /**
     * Parse a given Collection of FieldTypes to find the one correspond to the given identifier.
     * If not found, return null.
     * If "none" as identifier, bypass the parsing and return null.
     *
     * @param string            $fieldTypeIdentifier
     * @param ArrayCollection   $fieldTypes
     * @return FieldType|null
     */
    private static function getFieldType(string $fieldTypeIdentifier, ArrayCollection $fieldTypes): ?FieldType
    {
        if ("none" === $fieldTypeIdentifier) {
            return null;
        }

        /** @var FieldType $fieldType */
        foreach ($fieldTypes as $fieldType) {
            if ($fieldType->getIdentifier() === $fieldTypeIdentifier) {
                return $fieldType;
            }
        }

        return null;
    }

    /**
     * Parse a given Collection of Plants to find the one corresponding to the given identifier.
     * If not found, return null.
     * If "none" as identifier, bypass the parsing and return null.
     *
     * @param string                    $plantIdentifier
     * @param ArrayCollection|Plant[]   $plants
     * @return Plant|null
     */
    private static function getPlant(string $plantIdentifier, ArrayCollection $plants): ?Plant
    {
        if ("none" === $plantIdentifier) {
            return null;
        }

        foreach ($plants as $plant) {
            if ($plant->getIdentifier() === $plantIdentifier) {
                return $plant;
            }
        }

        return null;
    }
}
