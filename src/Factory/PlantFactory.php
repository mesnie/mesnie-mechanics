<?php
/**
 * PlantFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\Plant;
use App\Exception\PlantException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class PlantFactory
 * @package App\Factory
 */
class PlantFactory
{
    /**
     * @param string $path
     * @return ArrayCollection|Plant[]
     * @throws PlantException
     * @throws \Exception
     */
    public static function plantsFromJsonFile(string $path): ArrayCollection
    {
        $json = file_get_contents($path);

        $plants = new ArrayCollection();

        try {
            $decoded = json_decode($json);
        } catch (\Exception $exception) {
            throw new \Exception("unable to decode JSON from $path");
        }

        foreach ($decoded->plants as $plantObject) {
            $plant = self::createPlantFromObject($plantObject);
            $plants->add($plant);
        }

        return $plants;
    }

    /**
     * @param $object
     * @return Plant
     * @throws PlantException
     */
    public static function createPlantFromObject($object): Plant
    {
        $name = $object->name;
        $type = $object->type;
        $description = $object->description;
        $growthTime = $object->growthTime;

        return new Plant($name, $type, $growthTime, $description);
    }
}
