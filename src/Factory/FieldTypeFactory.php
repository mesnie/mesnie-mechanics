<?php
/**
 * FieldTypeFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\FieldType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class FieldTypeFactory
 * @package App\Factory
 */
class FieldTypeFactory
{
    /**
     * @param string $path
     * @return ArrayCollection|FieldType[]
     * @throws \Exception
     */
    public static function fieldTypesFromJsonFile(string $path): ArrayCollection
    {
        $json = file_get_contents($path);

        $fieldTypes = new ArrayCollection();

        try {
            $decoded = json_decode($json);
        } catch (\Exception $exception) {
            throw new \Exception("unable to decode JSON from $path");
        }

        foreach ($decoded->fieldTypes as $fieldTypeObject) {
            $fieldType = self::createFieldTypeFromObject($fieldTypeObject);
            $fieldTypes->add($fieldType);
        }

        return $fieldTypes;
    }

    /**
     * @param $object
     * @return FieldType
     * @throws \Exception
     */
    public static function createFieldTypeFromObject($object): FieldType
    {
        $name = $object->name;
        $description = $object->description;

        return new FieldType($name, $description);
    }
}
