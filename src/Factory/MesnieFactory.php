<?php
/**
 * MesnieFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\User;
use App\Exception\FamilyException;
use App\Exception\MoneyException;
use App\Exception\PersonageException;

/**
 * Class MesnieFactory
 * @package App\Factory
 */
class MesnieFactory
{
    /**
     * @param Personage     $funder
     * @param City          $city
     * @param string|null   $newFamilyName
     * @return array
     * @throws FamilyException
     * @throws PersonageException
     * @throws MoneyException
     */
    public static function fundMesnie(Personage $funder, City $city, ?string $newFamilyName): array
    {
        if ($newFamilyName) {
            return self::fundMesnieAndFamily($funder, $city, $newFamilyName);
        }

        $mesnie = self::createMesnie($city, null, $funder->getFamily(), $funder);

        return [
            "mesnie"    => $mesnie,
            "funder"    => $funder
        ];
    }

    /**
     * @param City              $city
     * @param User|null         $user
     * @param Family|null       $family
     * @param Personage|null    $head
     * @return Mesnie
     * @throws FamilyException
     * @throws PersonageException
     * @throws MoneyException
     */
    public static function createMesnie(
        City        $city,
        ?User       $user = null,
        ?Family     $family = null,
        ?Personage  $head = null
    ): Mesnie {
        $mesnie = new Mesnie($city, $family, $user);
        if ($head) {
            $head->setMesnie($mesnie);
            $mesnie->addMember($head);
            $mesnie->setHead($head);
        }
        if ($family) {
            $family->addMesnie($mesnie);
        }

        return $mesnie;
    }

    /**
     * @param Personage $funder
     * @param City $city
     * @param string $name
     * @return array
     * @throws FamilyException
     * @throws PersonageException
     * @throws MoneyException
     * @throws \Exception
     */
    public static function fundMesnieAndFamily(Personage $funder, City $city, string $name): array
    {
        $family = new Family($name);
        $mesnie = self::createMesnie($city, null, $family, $funder);
        $family->setPatriarch($funder);

        return [
            "family"    => $family,
            "mesnie"    => $mesnie,
            "funder"    => $funder
        ];
    }
}
