<?php
/**
 * CityFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\City;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CityFactory
 * @package App\Factory
 */
class CityFactory
{
    /**
     * @param string $path
     * @return ArrayCollection|City[]
     * @throws \Exception
     */
    public static function citiesFromJsonFile(string $path): ArrayCollection
    {
        $json = file_get_contents($path);

        $cities = new ArrayCollection();

        try {
            $decoded = json_decode($json);
        } catch (\Exception $exception) {
            throw new \Exception("unable to decode JSON from $path");
        }

        foreach ($decoded->cities as $cityObject) {
            $city = self::createCityFromObject($cityObject);
            $cities->add($city);
        }

        return $cities;
    }

    /**
     * @param $object
     * @return City
     * @throws \Exception
     */
    public static function createCityFromObject($object): City
    {
        $name = $object->name;
        $description = $object->description;

        return new City($name, $description);
    }
}
