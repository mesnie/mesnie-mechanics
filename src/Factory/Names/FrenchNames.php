<?php
/**
 * FrenchNames.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory\Names;

/**
 * Interface FrenchNames
 * Defines a list of male and female medieval french names
 * @package App\Factory\Names
 */
interface FrenchNames
{
    const FEMALE_FRENCH_NAMES = [
        "Adelaïde",
        "Adèle",
        "Adeline",
        "Agathe",
        "Albine",
        "Aliénor",
        "Alix",
        "Andrée",
        "Barbe",
        "Béatrice",
        "Benoite",
        "Berthe",
        "Brunehaut",
        "Clotilde",
        "Emma",
        "Ermengarde",
        "Eudoxie",
        "Fleur",
        "Gabrielle",
        "Guillemette",
        "Hermine",
        "Hildegarde",
        "Huguette",
        "Ide",
        "Jehanne",
        "Lucie",
        "Mahaud",
        "Mahaut",
        "Marguerite",
        "Marie",
        "Mathilde",
        "Michèle",
        "Ode",
        "Paule",
        "Pétronille",
        "Renaude",
        "Sophie",
        "Théophanie",
        "Yselda"
    ];

    const MALE_FRENCH_NAMES = [
        "Abélard",
        "André",
        "Anselin",
        "Arnoult",
        "Bérenger",
        "Benoit",
        "Clotaire",
        "Cloud",
        "Flour",
        "Foulques",
        "Fulbert",
        "Gerbert",
        "Gislebert",
        "Gossuin",
        "Guiscard",
        "Hubert",
        "Hugues",
        "Jacques",
        "Jehan",
        "Jocelyn",
        "Lambert",
        "Luc",
        "Maïeul",
        "Marc",
        "Mathieu",
        "Michel",
        "Paul",
        "Pierre",
        "Philippe",
        "Rainier",
        "Roscelin",
        "Théodore",
        "Thomas"
    ];

    /**
     * @return string
     */
    public static function getFemaleFrenchName(): string;

    /**
     * @return string
     */
    public static function getMaleFrenchName(): string;
}
