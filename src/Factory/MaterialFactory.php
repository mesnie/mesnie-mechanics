<?php
/**
 * MaterialFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\MaterialQuantity;
use App\Entity\MaterialValue;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class MaterialFactory
 * @package App\Factory
 */
class MaterialFactory
{
    /**
     * Parse the JSON file and create all Materials.
     *
     * @param string                    $path
     * @param ArrayCollection|City[]    $cities
     * @return ArrayCollection|Material[]
     * @throws MaterialException
     * @throws MoneyException
     * @throws \Exception
     */
    public static function materialsFromJsonFile(string $path, ArrayCollection $cities): ArrayCollection
    {
        $json = file_get_contents($path);

        $materials = new ArrayCollection();

        try {
            $decoded = json_decode($json);
        } catch (\Exception $exception) {
            throw new \Exception("unable to decode JSON from $path");
        }

        foreach ($decoded->materials as $materialObject) {
            $material = self::createMaterialFromObject($materialObject, $cities);
            $materials->add($material);
        }

        return $materials;
    }

    /**
     * From a given object, create a Material. Use the cities to build MaterialValues.
     *
     * @param                           $object
     * @param ArrayCollection|City[]    $cities
     * @return Material
     * @throws MaterialException
     * @throws MoneyException
     */
    public static function createMaterialFromObject($object, ArrayCollection $cities): Material
    {
        $name = $object->name;
        $type = $object->type;
        $description = $object->description;
        $material = new Material($name, $type, $description);
        $material = self::createValuesFromObject($object, $cities, $material);

        return $material;
    }

    /**
     * For a given object, create the MaterialValues and attach them to the Material.
     *
     * @param                           $object
     * @param ArrayCollection|City[]    $cities
     * @param Material                  $material
     * @return Material
     * @throws MoneyException
     */
    private static function createValuesFromObject($object, ArrayCollection $cities, Material $material): Material
    {
        foreach ($cities as $city) {
            $value = self::getValueFromObject($object, $city);
            if ($value) {
                $material->addMaterialValue(new MaterialValue($city, $material, $value));
            }
        }

        return $material;
    }

    /**
     * For a given city, get the material value from the object extracted from the fixtures.
     *
     * @param       $object
     * @param City  $city
     * @return float|null
     */
    private static function getValueFromObject($object, City $city): ?float
    {
        $key = $city->getIdentifier()."_value";
        if (property_exists($object, $key)) {
            return $object->$key;
        }

        return null;
    }

    /**
     * @param array             $objects
     * @param ArrayCollection   $materials
     * @param WorkType|null     $needer
     * @param WorkType|null     $resultFrom
     * @return ArrayCollection|MaterialQuantity[]
     * @throws MaterialException
     */
    public static function createMaterialQuantitiesFromArrayOfObjects(
        array           $objects,
        ArrayCollection $materials,
        ?WorkType       $needer = null,
        ?WorkType       $resultFrom = null
    ): ArrayCollection {
        $materialQuantities = new ArrayCollection();

        foreach ($objects as $object) {
            $materialQuantity = self::createMaterialQuantityFromObject($object, $materials);
            if ($needer) {
                $materialQuantity->setNeededByWork($needer);
            } elseif ($resultFrom) {
                $materialQuantity->setResultFromWork($resultFrom);
            }
            $materialQuantities->add($materialQuantity);
        }

        return $materialQuantities;
    }

    /**
     * @param                   $object
     * @param ArrayCollection   $materials
     * @return MaterialQuantity|null
     * @throws MaterialException
     */
    private static function createMaterialQuantityFromObject($object, ArrayCollection $materials): ?MaterialQuantity
    {
        $materialIdentifier = $object->identifier;
        $quantity = $object->quantity;

        $material = self::getMaterial($materialIdentifier, $materials);

        $materialQuantity = null;
        if ($material) {
            $materialQuantity = new MaterialQuantity($material, $quantity);
        }

        return $materialQuantity;
    }

    /**
     * @param string            $materialIdentifier
     * @param ArrayCollection   $materials
     * @return Material|null
     */
    private static function getMaterial(string $materialIdentifier, ArrayCollection $materials): ?Material
    {
        if ("none" === $materialIdentifier) {
            return null;
        }

        /** @var Material $material */
        foreach ($materials as $material) {
            if ($material->getIdentifier() === $materialIdentifier) {
                return $material;
            }
        }

        return null;
    }
}
