<?php
/**
 * PersonageFactory.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Factory;

/** Usages */
use App\Entity\Personage;
use App\Entity\Pregnancy;
use App\Exception\ChildException;
use App\Exception\FamilyException;
use App\Exception\PersonageException;
use App\Factory\Names\FrenchNames;

/**
 * Class PersonageFactory
 * @package App\Factory
 */
class PersonageFactory implements FrenchNames
{
    const PERSONAGE_START = 15;

    /**
     * @return string
     */
    public static function getFemaleFrenchName(): string
    {
        return self::FEMALE_FRENCH_NAMES[array_rand(self::FEMALE_FRENCH_NAMES)];
    }

    /**
     * @return string
     */
    public static function getMaleFrenchName(): string
    {
        return self::MALE_FRENCH_NAMES[array_rand(self::MALE_FRENCH_NAMES)];
    }

    /**
     * @param string    $firstname
     * @param bool      $isMale
     * @param int       $currentYear
     * @return Personage
     * @throws PersonageException
     */
    public static function createPersonage(string $firstname, bool $isMale, int $currentYear): Personage
    {
        $personage = new Personage($isMale, $currentYear - self::PERSONAGE_START);
        $personage->setFirstName($firstname);

        return $personage;
    }

    /**
     * @param int       $currentYear
     * @param bool      $isMale
     * @param string    $language
     * @return Personage
     * @throws PersonageException 101, 102
     */
    private static function createNubile(int $currentYear, bool $isMale, string $language = "fr"): Personage
    {
        $nubility = $isMale ? Personage::NUBILITY_MALE : Personage::NUBILITY_FEMALE;
        $age = $nubility + rand(0, 10);

        $nubile = new Personage($isMale, $currentYear - $age);
        switch ($language) {
            case "fr":
                $nubile->setFirstName(($isMale) ? self::getMaleFrenchName() : self::getFemaleFrenchName());
                break;
            default:
                $nubile->setFirstName(($isMale) ? self::getMaleFrenchName() : self::getFemaleFrenchName());
                break;
        }

        return $nubile;
    }

    /**
     * @param int       $currentYear
     * @param string    $language
     * @return Personage
     * @throws PersonageException 101, 102
     */
    public static function createNubileWoman(int $currentYear, string $language = "fr"): Personage
    {
        return self::createNubile($currentYear, false, $language);
    }

    /**
     * @param int       $currentYear
     * @param string    $language
     * @return Personage
     * @throws PersonageException 101, 102
     */
    public static function createNubileMan(int $currentYear, string $language = "fr"): Personage
    {
        return self::createNubile($currentYear, true, $language);
    }

    /**
     * @param Pregnancy $pregnancy
     * @param int       $year
     * @return Personage
     * @throws ChildException
     * @throws FamilyException
     */
    public static function deliverChild(Pregnancy $pregnancy, int $year): Personage
    {
        $isMale = (bool) rand(0, 1);
        $child = new Personage($isMale, $year);
        $child->setMother($pregnancy->getMother());
        $child->setFather($pregnancy->getFather());
        $child->setMesnie($pregnancy->getMother()->getMesnie());

        return $child;
    }
}
