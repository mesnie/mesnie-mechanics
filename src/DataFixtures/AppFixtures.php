<?php

/** Namespace */
namespace App\DataFixtures;

/** Usages */
use App\Entity\Material;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    /**
     * @param ObjectManager $manager
     * @throws \App\Exception\MaterialException
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $manager->flush();
    }
}
