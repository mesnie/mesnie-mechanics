<?php
/**
 * DateService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usages */
use App\Entity\ServerDate;
use App\Exception\DateException;
use App\Manager\FieldManager;
use App\Manager\PregnancyManager;
use App\Manager\ServerDateManager;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

/**
 * Class DateService
 * @package App\Service
 */
class DateService
{
    /** @var ServerDateManager $serverDateManager */
    private $serverDateManager;

    /** @var WorkService $workService */
    private $workService;

    /** @var PregnancyManager $pregnancyManager */
    private $pregnancyManager;

    /** @var FieldManager $fieldManager */
    private $fieldManager;

    /** @var LoggerInterface $logger */
    private $logger;

    /**
     * DateService constructor.
     * Autowire the managers, the WorkService and the Logger.
     *
     * @param ServerDateManager $serverDateManager
     * @param PregnancyManager  $pregnancyManager
     * @param FieldManager      $fieldManager
     * @param WorkService       $workService
     * @param LoggerInterface   $logger
     */
    public function __construct(
        ServerDateManager   $serverDateManager,
        PregnancyManager    $pregnancyManager,
        FieldManager        $fieldManager,
        WorkService         $workService,
        LoggerInterface     $logger
    ) {
        $this->serverDateManager = $serverDateManager;
        $this->pregnancyManager = $pregnancyManager;
        $this->fieldManager = $fieldManager;
        $this->workService = $workService;
        $this->logger = $logger;
    }

    /**
     * Return the date of the server from the database.
     *
     * @return ServerDate
     * @throws DateException
     */
    public function currentDate(): ServerDate
    {
        return $this->serverDateManager->currentDate($this->logger);
    }

    /**
     * Return the year of the server current date.
     *
     * @return int
     * @throws DateException
     */
    public function getYear(): int
    {
        return $this->currentDate()->getYear();
    }

    /**
     * Change and save the server date to the next week,
     * execute all events related to the new week,
     * if new month, execute all events related to it,
     * if new year, execute all events related to it.
     *
     * @throws ORMException
     * @throws \Exception
     */
    public function upWeek(): ServerDate
    {
        $currentDate = $this->serverDateManager->nextWeek();

        $this->newWeek();
        if ($currentDate->isNewMonth()) {
            $this->newMonth($currentDate);
        }
        if ($currentDate->isNewYear()) {
            $this->newYear();
        }

        return $currentDate;
    }

    /**
     * Execute all events linked to the new week :
     * Execute all works.
     */
    private function newWeek(): void
    {
        try {
            $workCount = $this->fieldManager->progressAllWork();
            $this->logger->info($workCount . " works finished");
        } catch (\Exception $exception) {
            $this->logger->error("allWork KO : ".$exception->getMessage());
        }
    }

    /**
     * Execute all events linked to the new month :
     * First, up all pregnancies and deliver the 9 month old ones.
     * Second, start random new pregnancies for married couples.
     * Third, grow up all plants on fields.
     *
     * @param ServerDate $currentDate
     */
    private function newMonth(ServerDate $currentDate): void
    {
        try {
            $this->pregnancyManager->upAllPregnanciesMonth($currentDate->getYear());
        } catch (\Exception $exception) {
            $this->logger->error("upPregnancies KO : ".$exception->getMessage());
        }

        try {
            $newPregnancies = $this->pregnancyManager->randomPregnancies($currentDate->getYear());
            if ($newPregnancies > 0) {
                $log = $newPregnancies . " new pregnanc";
                $log .= ($newPregnancies > 1) ? "ies" : "y";
                $log .= " this month.";
                $this->logger->info($log);
            }
        } catch (\Exception $exception) {
            $this->logger->error("randomPregnancies KO : ".$exception->getMessage());
        }

        try {
            $this->fieldManager->allGrowth();
        } catch (\Exception $exception) {
            $this->logger->error("plantGrowth KO : ".$exception->getMessage());
        }
    }

    /**
     * Execute all events linked to the new year.
     *
     * @temp currently none, but taxes, for example, will probably be there later.
     */
    private function newYear(): void
    {
    }
}
