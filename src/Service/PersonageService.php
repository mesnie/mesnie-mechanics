<?php
/**
 * PersonageService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usages */
use App\Entity\Personage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class PersonageService
 * @package App\Service
 */
class PersonageService
{
    /**
     * Sort a given ArrayCollection of Personage by birth year,
     * the oldest first, the youngest last.
     *
     * @param Collection|Personage[] $collection Collection of Personages to be sorted
     * @return ArrayCollection sorted ArrayCollection of Personages
     */
    public static function sortByBirthYear(Collection $collection): ArrayCollection
    {
        $iterator = $collection->getIterator();
        $iterator->uasort(
            function (Personage $a, Personage $b) {
                return ($a->getBirthYear() < $b->getBirthYear()) ? -1 : 1;
            }
        );
        return new ArrayCollection(array_values(iterator_to_array($iterator)));
    }

    /**
     * Find and return the heir of a Personage :
     * first from its descendants,
     * then its spouse,
     * then from its siblings,
     * last from its ascendants.
     * If no heir, returns null.
     *
     * @param Personage $personage
     * @return Personage|null
     */
    public static function findHeir(Personage $personage): ?Personage
    {
        $heir = self::findHeirFromDescendants($personage);
        if (!$heir && $personage->getSpouse() && !$personage->getSpouse()->getDeathYear()) {
            $heir = $personage->getSpouse();
        }
        if (!$heir) {
            $heir = self::findHeirFromSiblings($personage);
        }
        if (!$heir) {
            $heir = self::findHeirFromAscendants($personage, 3);
        }

        return $heir;
    }

    /**
     * Find and return an heir from the descendants of the personage :
     * For each son, by age, return him if alive or his descendant heir if any.
     * If no heir between sons and their descendants, apply the same method for daughters.
     * If still no heir, return null.
     *
     * @param Personage $personage
     * @return Personage|null
     */
    public static function findHeirFromDescendants(Personage $personage): ?Personage
    {
        $sons = new ArrayCollection();
        $daughters = new ArrayCollection();
        foreach ($personage->getChildren() as $child) {
            if ($child->isMale()) {
                $sons->add($child);
            } else {
                $daughters->add($child);
            }
        }

        /** @var Personage $son */
        foreach ($sons as $son) {
            if (!$son->getDeathYear()) {
                return $son;
            }
            $sonHeir = self::findHeirFromDescendants($son);
            if ($sonHeir) {
                return $sonHeir;
            }
        }

        /** @var Personage $daughter */
        foreach ($daughters as $daughter) {
            if (!$daughter->getDeathYear()) {
                return $daughter;
            }
            $daughterHeir = self::findHeirFromDescendants($daughter);
            if ($daughterHeir) {
                return $daughterHeir;
            }
        }

        return null;
    }

    /**
     * Find and return an heir from the siblings of the personage :
     * For each brother, by age, return him if alive or his descendant heir if any.
     * If no heir between brothers and their descendants, apply the same method for sisters.
     * If still no heir, return null.
     *
     * @param Personage $personage
     * @return Personage|null
     */
    public static function findHeirFromSiblings(Personage $personage): ?Personage
    {
        $brothers = new ArrayCollection();
        $sisters = new ArrayCollection();
        foreach ($personage->getSiblings() as $sibling) {
            if ($sibling->isMale()) {
                $brothers->add($sibling);
            } else {
                $sisters->add($sibling);
            }
        }

        /** @var Personage $brother */
        foreach ($brothers as $brother) {
            if (!$brother->getDeathYear()) {
                return $brother;
            }
            $heir = self::findHeirFromDescendants($brother);
            if ($heir) {
                return $heir;
            }
        }

        /** @var Personage $sister */
        foreach ($sisters as $sister) {
            if (!$sister->getDeathYear()) {
                return $sister;
            }
            $heir = self::findHeirFromDescendants($sister);
            if ($heir) {
                return $heir;
            }
        }

        return null;
    }

    /**
     * Find and return an heir from the ascendants of the personage :
     * First, return the father if alive.
     * Second, return the mother if alive.
     * Third, recursively search from the ascendants of the father if the recusivity is above 1.
     * Last, recursively search from the ascendants of the mother if the recusivity is above 1.
     * If still no heir, return null.
     *
     * @param Personage $personage
     * @param int       $level      the recursivity of the search
     * @return Personage|null
     */
    public static function findHeirFromAscendants(Personage $personage, int $level = 1): ?Personage
    {
        $heir = null;

        if ($personage->getFather() && !$personage->getFather()->getDeathYear()) {
            $heir = $personage->getFather();
        }
        if (!$heir && $personage->getMother() && !$personage->getMother()->getDeathYear()) {
            $heir = $personage->getMother();
        }

        if ($level > 1) {
            if (!$heir && $personage->getFather()) {
                $heir = self::findHeirFromAscendants($personage->getFather(), $level - 1);
            }
            if (!$heir && $personage->getMother()) {
                $heir = self::findHeirFromAscendants($personage->getMother(), $level - 1);
            }
        }

        return $heir;
    }
}
