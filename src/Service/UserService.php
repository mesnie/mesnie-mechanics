<?php
/**
 * UserService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usages */
use App\Entity\User;
use App\Exception\UserException;
use App\Factory\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends AbstractEntityManagerAwareService
{

    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    /**
     * UserService constructor.
     * Autowire EntityManager and UserPasswordEncoder.
     *
     * @param EntityManagerInterface        $entityManager
     * @param UserPasswordEncoderInterface  $encoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($entityManager);
        $this->encoder = $encoder;
    }

    /**
     * Encodes the password and set it to the User.
     *
     * @param User      $user           the User to who password will be set
     * @param string    $plainPassword  the password the user has enter, before encoding
     * @return User
     */
    private function setPassword(User $user, string $plainPassword): User
    {
        $encodedPassword = $this->encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        return $user;
    }

    /**
     * Register in database the given User.
     *
     * @param User $user the User to save.
     * @return User
     */
    private function saveUser(User $user): User
    {
        $this->persistAndFlush($user);

        return $user;
    }

    /**
     * Creates an User With the given email and password and saves it.
     *
     * @param string $email     the email of the User to create
     * @param string $password  the password of the user to create
     * @return User
     * @throws UserException
     */
    public function createUser(string $email, string $password): User
    {
        $user = UserFactory::createUser($email);
        $user = $this->setPassword($user, $password);

        return $this->saveUser($user);
    }
}
