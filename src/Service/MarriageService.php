<?php
/**
 * MarriageService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usages */
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Exception\FamilyException;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use App\Factory\PersonageFactory;
use App\Manager\MesnieManager;
use App\Manager\PersonageManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class MariageService
 * @package App\Service
 */
class MarriageService
{
    /** @var PersonageManager */
    private $personageManager;

    /** @var MesnieManager */
    private $mesnieManager;

    /**
     * MarriageService constructor.
     * Autowire EntityManager.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->personageManager = new PersonageManager($entityManager);
        $this->mesnieManager = new MesnieManager($entityManager);
    }

    /**
     * Returns the marriage empeachment :
     * * 0 no impeachment
     * * -1 already married
     * * -2 consanguinity
     * * -3 too young
     * If there is several empeachment, the first one is given
     *
     * @param Personage $first  one of the Personages to be married
     * @param Personage $second one of the Personages to be married
     * @param int|null  $year   the year of the marriage to happen
     * @return int the empeachment code
     * @throws PersonageException
     * @throws MarriageException
     */
    public static function marriageImpeachment(Personage $first, Personage $second, ?int $year = null): int
    {
        if ($first->isMale() === $second->isMale()) {
            throw new MarriageException(
                "The personage ".$first->getId()." is of the same sex as ".$second->getId(),
                MarriageException::ERROR_MARRY_SEX,
                $first,
                $second
            );
        }
        if ($first->getSpouse() || $second->getSpouse()) {
            return -1;
        }
        if (0 !== $first->consanguinity($second)) {
            return -2;
        }
        if ($year && (!$first->isNubile($year) || (!$second->isNubile($year)))) {
            return -3;
        }

        return 0;
    }

    /**
     * Marry two peoples and, if asked, fund a new mesnie for them.
     *
     * @param Personage $male           the male to be married
     * @param Personage $female         the female to be married
     * @param int       $year           the year of the marriage
     * @param bool      $fundNewMesnie  whether or not anew mesnie will be funded.
     * @return Mesnie
     * @throws MarriageException 501, 502
     * @throws PersonageException 161, 171, 173
     * @throws FamilyException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function marry(Personage $male, Personage $female, int $year, bool $fundNewMesnie = false): Mesnie
    {
        $this->personageManager->marriage($male, $female);

        if ($fundNewMesnie) {
            $mesnie = $this->mesnieManager->fundMesnie($male, $year);
            $female->setMesnie($mesnie);
            $this->personageManager->persist($male);
            $this->personageManager->persist($female);
            $this->personageManager->flush();
        }

        return $male->getMesnie();
    }

    /**
     * Creates a Personage of the opposite sex,
     * without known family,
     * and marry the two.
     *
     * @param Personage $perso  the Personage who shall be married to a commoner
     * @param int       $year   the year of the marriage
     * @return Personage the commoner who has been created and married
     * @throws FamilyException
     * @throws MarriageException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws PersonageException
     */
    public function marryCommoner(Personage $perso, int $year): Personage
    {
        if (!$perso->isNubile($year)) {
            throw new MarriageException(
                "The personage ".$perso->getId()." is not nubile",
                MarriageException::ERROR_MARRY_AGE,
                $perso
            );
        }

        if ($perso->isMale()) {
            $commoner = PersonageFactory::createNubileWoman($year);
            $this->marry($perso, $commoner, $year);
        } else {
            $commoner = PersonageFactory::createNubileMan($year);
            $commoner->setMesnie($perso->getMesnie());
            $this->marry($commoner, $perso, $year);
        }

        $this->personageManager->persist($perso);
        $this->personageManager->persist($commoner);
        $this->personageManager->flush();

        return $commoner;
    }
}
