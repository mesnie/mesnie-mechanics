<?php
/**
 * SettingsService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usage */
use App\Entity\City;
use App\Entity\FieldType;
use App\Entity\Material;
use App\Entity\Plant;
use App\Entity\Utils\NamedEntity;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use App\Exception\PlantException;
use App\Factory\CityFactory;
use App\Factory\MaterialFactory;
use App\Factory\FieldTypeFactory;
use App\Factory\PlantFactory;
use App\Factory\WorkTypeFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingsService
 * @package App\Service
 */
class SettingsService extends AbstractEntityManagerAwareService
{
    /** The JSON files where settings data are stored */
    const MATERIAL_FILE     = "/../DataFixtures/material.json";
    const PLANT_FILE        = "/../DataFixtures/plant.json";
    const FIELD_TYPE_FILE   = "/../DataFixtures/fieldType.json";
    const WORK_TYPE_FILE    = "/../DataFixtures/workType.json";
    const CITY_FILE         = "/../DataFixtures/city.json";

    /**
     * SettingsService constructor.
     * Autowire EntityManager.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * Save given entities if their identifier is not already taken or if the "all" options is used.
     *
     * @param ArrayCollection|NamedEntity[] $data       the entities to save
     * @param string                        $className  the doctrine name of the class of the entities
     * @param bool                          $all        if true, will not check if identifiers already exist.
     * @return ArrayCollection
     */
    private function saveCollectionOfEntities(ArrayCollection $data, string $className, bool $all): ArrayCollection
    {
        foreach ($data as $datum) {
            if ($all || !$this->doesItExist($className, $datum->getIdentifier())) {
                $this->persist($datum);
            }
        }
        $this->flush();

        return $data;
    }

    /**
     * Construct the materials from the JSON file and save them.
     *
     * @param ArrayCollection|City[]    $cities
     * @param bool                      $all if true, will not check if identifiers already exist.
     * @return ArrayCollection|Material[]
     * @throws MaterialException
     */
    public function loadMaterial(ArrayCollection $cities, bool $all = false): ArrayCollection
    {
        $materials = MaterialFactory::materialsFromJsonFile(__DIR__.self::MATERIAL_FILE, $cities);
        return $this->saveCollectionOfEntities($materials, Material::class, $all);
    }

    /**
     * Construct the plants from the JSON file and save them.
     *
     * @param bool $all if true, will not check if identifiers already exist.
     * @return ArrayCollection|Plant[]
     * @throws PlantException
     */
    public function loadPlant(bool $all = false): ArrayCollection
    {
        $plants = PlantFactory::plantsFromJsonFile(__DIR__.self::PLANT_FILE);
        return $this->saveCollectionOfEntities($plants, Plant::class, $all);
    }

    /**
     * Construct the fieldtypes from the JSON file and save them.
     *
     * @param bool $all if true, will not check if identifiers already exist.
     * @return ArrayCollection|FieldType[]
     * @throws \Exception
     */
    public function loadFieldTypes(bool $all = false): ArrayCollection
    {
        $fieldTypes = FieldTypeFactory::fieldTypesFromJsonFile(__DIR__.self::FIELD_TYPE_FILE);
        return $this->saveCollectionOfEntities($fieldTypes, FieldType::class, $all);
    }

    /**
     * Construct the cities from the JSON file and save them.
     *
     * @param bool $all if true, will not check if identifiers already exist.
     * @return ArrayCollection|City[]
     * @throws \Exception
     */
    public function loadCities(bool $all = false): ArrayCollection
    {
        $cities = CityFactory::citiesFromJsonFile(__DIR__.self::CITY_FILE);
        return $this->saveCollectionOfEntities($cities, City::class, $all);
    }

    /**
     * Construct the workTypes from the JSON file and save them.
     *
     * @param ArrayCollection|FieldType[] $fieldTypes the FieldTypes that may be linked to WorkTypes
     * @param ArrayCollection|Material[]  $materials  the Materials that may be linked to WorkTypes
     * @param ArrayCollection|Plant[]     $plants     the Plants that may be linked to WorkTypes
     * @param bool                        $all        if true, will not check if identifiers already exist.
     * @return ArrayCollection
     * @throws MaterialException
     */
    public function loadWorkTypes(
        ArrayCollection $fieldTypes,
        ArrayCollection $materials,
        ArrayCollection $plants,
        bool            $all = false
    ): ArrayCollection {
        $workTypes = WorkTypeFactory::workTypesFromJsonFile(
            __DIR__.self::WORK_TYPE_FILE,
            $fieldTypes,
            $materials,
            $plants
        );

        /** @var WorkType $workType */
        foreach ($workTypes as $workType) {
            if ($all || !$this->doesItExist(WorkType::class, $workType->getIdentifier())) {
                $this->persist($workType);
                foreach ($workType->getMaterialNeeds() as $materialNeed) {
                    $this->persist($materialNeed);
                }
                foreach ($workType->getMaterialResults() as $materialResult) {
                    $this->persist($materialResult);
                }
            }
        }
        $this->flush();

        return $workTypes;
    }

    /**
     * Construct the settings entitites from their JSON files and save them :
     * Materials, Plants, FieldTypes, WorkTypes, Cities
     *
     * @param bool $all if true, will not check if identifiers already exist.
     * @return ArrayCollection a Collection of collections of entities, grouped by class.
     * @throws MaterialException
     * @throws PlantException
     * @throws \Exception
     */
    public function loadAll(bool $all = false): ArrayCollection
    {
        $cities = $this->loadCities($all);
        $materials = $this->loadMaterial($cities, $all);
        $plants = $this->loadPlant($all);
        $fieldTypes = $this->loadFieldTypes($all);
        $workTypes = $this->loadWorkTypes($fieldTypes, $materials, $plants, $all);

        return new ArrayCollection([
            "materials"     => $materials,
            "plants"        => $plants,
            "fieldTypes"   => $fieldTypes,
            "workTypes"     => $workTypes,
            "cities"        => $cities
        ]);
    }
}
