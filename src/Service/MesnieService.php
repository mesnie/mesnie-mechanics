<?php
/**
 * MesnieService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usage */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Field;
use App\Entity\FieldType;
use App\Entity\Mesnie;
use App\Entity\User;
use App\Exception\FamilyException;
use App\Exception\MoneyException;
use App\Exception\PersonageException;
use App\Factory\MesnieFactory;
use App\Factory\PersonageFactory;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MesnieService
 * @package App\Service
 */
class MesnieService extends AbstractEntityManagerAwareService
{
    /** @var DateService $dateService */
    private $dateService;

    /**
     * MesnieService constructor.
     * Autowire DateService and EntityManager.
     *
     * @param DateService            $dateService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(DateService $dateService, EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->dateService = $dateService;
    }

    /**
     * Creates a Personage, a Family and a Mesnie.
     *
     * @param User      $user       the User founding its first mesnie
     * @param string    $name       the name of the Family to be founded
     * @param string    $firstname  the firstname of the Personage to be created
     * @param bool      $isMale     whether the Personage is a male or not
     * @param City      $city       the City in which the Mesnie will be founded
     * @return Mesnie
     * @throws FamilyException
     * @throws PersonageException
     * @throws MoneyException
     * @throws \Exception
     */
    public function foundFirstMesnie(
        User    $user,
        string  $name,
        string  $firstname,
        bool    $isMale,
        City    $city
    ): Mesnie {
        $personage = PersonageFactory::createPersonage($firstname, $isMale, $this->dateService->getYear());
        $family = new Family($name);
        $mesnie = MesnieFactory::createMesnie($city, $user, $family, $personage);
        $mesnie->addMoney(240);
        $family->setPatriarch($personage);
        $field = $this->createFriche($mesnie);
        $mesnie->addField($field);

        $this->persist($personage);
        $this->persist($mesnie);
        $this->persist($family);
        $this->persist($field);
        $this->flush();

        return  $mesnie;
    }

    /**
     * Creates a Personage, a Family and a Mesnie in a random city.
     *
     * @param User      $user       the User founding its first mesnie
     * @param string    $name       the name of the Family to be founded
     * @param string    $firstname  the firstname of the Personage to be created
     * @param bool      $isMale     whether the Personage is a male or not
     * @return Mesnie
     * @throws FamilyException
     * @throws PersonageException
     * @throws \Exception
     */
    public function foundFirstMesnieInRandomCity(
        User    $user,
        string  $name,
        string  $firstname,
        bool    $isMale
    ): Mesnie {
        $city = $this->getRandomCity();

        return $this->foundFirstMesnie(
            $user,
            $name,
            $firstname,
            $isMale,
            $city
        );
    }

    /**
     * @return City
     * @throws \Exception
     */
    private function getRandomCity(): City
    {
        $city = $this->getCityRepository()->getRandomCity();
        if (!$city) {
            throw new \Exception("no city !");
        }

        return $city;
    }

    /**
     * @param Mesnie $mesnie
     * @return Field
     */
    private function createFriche(Mesnie $mesnie): Field
    {
        $type = $this->getFieldType("champ_en_friche");

        return new Field($type, $mesnie);
    }

    /**
     * @param string $typeName
     * @return FieldType
     */
    private function getFieldType(string $typeName): FieldType
    {
        return $this->getFieldTypeRepository()->findOneBy([
            "identifier" => $typeName
        ]);
    }
}
