<?php
/**
 * AbstractEntityManagerAwareService.php.
 * @author  mickael
 * @licence GNU GPLv3
 */

/** Namespace **/
namespace App\Service;

/** Usages */
use App\Entity\City;
use App\Entity\Family;
use App\Entity\Field;
use App\Entity\FieldType;
use App\Entity\Material;
use App\Entity\MaterialQuantity;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\Plant;
use App\Entity\Pregnancy;
use App\Entity\ServerDate;
use App\Entity\User;
use App\Entity\WorkType;
use App\Repository\CityRepository;
use App\Repository\FamilyRepository;
use App\Repository\FieldRepository;
use App\Repository\FieldTypeRepository;
use App\Repository\MaterialQuantityRepository;
use App\Repository\MaterialRepository;
use App\Repository\MesnieRepository;
use App\Repository\PersonageRepository;
use App\Repository\PlantRepository;
use App\Repository\PregnancyRepository;
use App\Repository\ServerDateRepository;
use App\Repository\UserRepository;
use App\Repository\WorkTypeRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractEntityManagerAwareService
 * @package App\Manager
 */
abstract class AbstractEntityManagerAwareService
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     * AbstractEntityManagerAwareService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param $entity
     */
    protected function persist($entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     *
     */
    protected function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param $entity
     */
    protected function persistAndFlush($entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * Return the Repository from a given class.
     *
     * @param string $className the doctrine name of the class
     * @return ObjectRepository
     */
    protected function getObjectRepository(string $className): ObjectRepository
    {
        return $this->getEntityManager()->getRepository($className);
    }

    /**
     * Check if an identifier is already taken for a given class.
     *
     * @param string $className
     * @param string $filter
     * @param string $criterium
     * @return bool
     */
    protected function doesItExist(string $className, string $filter, string $criterium = "identifier"): bool
    {
        return (!is_null($this->getObjectRepository($className)->findOneBy([$criterium => $filter])));
    }

    /**
     * @return CityRepository
     */
    protected function getCityRepository(): CityRepository
    {
        return $this->getEntityManager()->getRepository(City::class);
    }

    /**
     * @return FamilyRepository
     */
    protected function getFamilyRepository(): FamilyRepository
    {
        return $this->getEntityManager()->getRepository(Family::class);
    }

    /**
     * @return FieldRepository
     */
    protected function getFieldRepository(): FieldRepository
    {
        return $this->getEntityManager()->getRepository(Field::class);
    }

    /**
     * @return FieldTypeRepository
     */
    protected function getFieldTypeRepository(): FieldTypeRepository
    {
        return $this->getEntityManager()->getRepository(FieldType::class);
    }

    /**
     * @return MaterialQuantityRepository
     */
    protected function getMaterialQuantityRepository(): MaterialQuantityRepository
    {
        return $this->getEntityManager()->getRepository(MaterialQuantity::class);
    }

    /**
     * @return MaterialRepository
     */
    protected function getMaterialRepository(): MaterialRepository
    {
        return $this->getEntityManager()->getRepository(Material::class);
    }

    /**
     * @return MesnieRepository
     */
    protected function getMesnieRepository(): MesnieRepository
    {
        return $this->getEntityManager()->getRepository(Mesnie::class);
    }

    /**
     * @return PersonageRepository
     */
    protected function getPersonageRepository(): PersonageRepository
    {
        return $this->getEntityManager()->getRepository(Personage::class);
    }

    /**
     * @return PlantRepository
     */
    protected function getPlantRepository(): PlantRepository
    {
        return $this->getEntityManager()->getRepository(Plant::class);
    }

    /**
     * @return PregnancyRepository
     */
    protected function getPregnancyRepository(): PregnancyRepository
    {
        return $this->getEntityManager()->getRepository(Pregnancy::class);
    }

    /**
     * @return ServerDateRepository
     */
    protected function getServerDateRepository(): ServerDateRepository
    {
        return $this->getEntityManager()->getRepository(ServerDate::class);
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository(): UserRepository
    {
        return $this->getEntityManager()->getRepository(User::class);
    }

    /**
     * @return WorkTypeRepository
     */
    protected function getWorkTypeRepository(): WorkTypeRepository
    {
        return $this->getEntityManager()->getRepository(WorkType::class);
    }
}
