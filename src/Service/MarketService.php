<?php
/**
 * MarketService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usage */
use App\Entity\City;
use App\Entity\Material;
use App\Entity\Mesnie;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MarketService
 * @package App\Service
 */
class MarketService extends AbstractEntityManagerAwareService
{
    /**
     * MarketService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * Get the price of the material, remove the money from the mesnie and add the material to the mesnie.
     *
     * @param Mesnie $mesnie
     * @param Material $material
     * @param int $quantity
     * @throws MoneyException
     * @throws MaterialException
     */
    public function buyMaterial(Mesnie $mesnie, Material $material, int $quantity)
    {
        $price = self::getPrice($mesnie->getCity(), $material, $quantity);
        $mesnie = $this->removeMoneyFromMesnie($mesnie, $price);
        $mesnie->addToInventory($material, $quantity);

        $this->persistAndFlush($mesnie);
    }

    /**
     * Get the price of the material, remove the material from the mesnie and add the money to the mesnie.
     *
     * @param Mesnie $mesnie
     * @param Material $material
     * @param int $quantity
     * @throws MaterialException
     * @throws MoneyException
     */
    public function sellMaterial(Mesnie $mesnie, Material $material, int $quantity)
    {
        $price = self::getPrice($mesnie->getCity(), $material, $quantity);
        $mesnie->removeFromInventory($material, $quantity);
        $mesnie->addMoney($price);

        $this->persistAndFlush($mesnie);
    }

    /**
     * Calculate the price of a Material in a City, for a default quantity of one.
     *
     * @param City      $city
     * @param Material  $material
     * @param int       $quantity
     * @return float
     * @throws MoneyException
     */
    public static function getPrice(City $city, Material $material, int $quantity = 1): float
    {
        foreach ($material->getMaterialValues() as $materialValue) {
            if ($materialValue->getCity() === $city) {
                return $materialValue->getValue() * $quantity;
            }
        }

        throw new MoneyException(
            "no value for ".$material->getIdentifier()." in ".$city->getIdentifier(),
            MoneyException::NO_VALUE
        );
    }

    /**
     * Try to remove money from a mesnie, throw MoneyException if not enough.
     *
     * @param Mesnie $mesnie
     * @param float $money
     * @return Mesnie
     * @throws MoneyException
     */
    private function removeMoneyFromMesnie(Mesnie $mesnie, float $money): Mesnie
    {
        try {
            $mesnie->removeMoney($money);
        } catch (MoneyException $exception) {
            throw new MoneyException(
                $money."half-pites needed, but only ".$mesnie->getMoney()." in mesnie",
                MoneyException::TOO_POOR
            );
        }

        return $mesnie;
    }
}