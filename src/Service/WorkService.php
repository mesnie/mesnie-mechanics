<?php
/**
 * WorkService.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Service;

/** Usages */
use App\Entity\Field;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\WorkType;
use App\Exception\MaterialException;
use App\Exception\WorkException;
use App\Manager\FieldManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class WorkService
 * @package App\Service
 */
class WorkService
{
    /** @var FieldManager $fieldManager */
    private $fieldManager;

    /**
     * WorkService constructor.
     * Autowires the FieldManager.
     *
     * @param FieldManager $fieldManager
     */
    public function __construct(FieldManager $fieldManager)
    {
        $this->fieldManager = $fieldManager;
    }

    /**
     * Assign a worker on a job.
     * If a WorkType is given, creates a new job and assign the worker to it.
     * If a field is given, the job is on it.
     *
     * @TEMPORARY : for now, all works are on fields.
     *
     * @param Personage     $worker the Personage put to work
     * @param WorkType|null $workType (optional) the new WorkType set. If none, continue the WorkType already set.
     * @param Field|null    $field (optional) the Field on which the work is done.
     * @throws WorkException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MaterialException
     */
    public function setToWork(
        Personage   $worker,
        ?WorkType   $workType,
        ?Field      $field
    ): void {

        if ($field) {
            if ($workType) {
                $this->fieldManager->newWorkOnField($field, $workType, $worker);
            } else {
                $this->fieldManager->setWorkerOnField($field, $worker);
            }
        }
    }

    /**
     * Checks if a Mesnie has enough materials to start a WorkType.
     *
     * @param WorkType  $workType   the WorkType which needs the resources.
     * @param Mesnie    $mesnie     the Mesnie from which to get the resources.
     * @return bool
     */
    public static function hasMesnieEnoughMaterial(WorkType $workType, Mesnie $mesnie): bool
    {
        foreach ($workType->getMaterialNeeds() as $need) {
            $inventoryQuantity = $mesnie->getQuantityFromInventory($need->getMaterial());
            if ($inventoryQuantity < $need->getQuantity()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Removes the materials needed by a WorkType from a mesnie
     *
     * @param WorkType  $workType   the workType which needs the resources.
     * @param Mesnie    $mesnie     the Mesnie from which to get the resources.
     * @return Mesnie
     * @throws MaterialException
     */
    public static function removeMaterialNeededFromMesnie(WorkType $workType, Mesnie $mesnie): Mesnie
    {
        foreach ($workType->getMaterialNeeds() as $need) {
            $mesnie->removeFromInventory($need->getMaterial(), $need->getQuantity());
        }

        return $mesnie;
    }

    /**
     * Add to the mesnie the materials produced by the WorkType.
     *
     * @param WorkType  $workType   the workType producing the resources.
     * @param Mesnie    $mesnie     the Mesnie to add the resources to.
     * @return Mesnie
     * @throws MaterialException
     */
    public static function addMaterialResultToMesnie(WorkType $workType, Mesnie $mesnie): Mesnie
    {
        foreach ($workType->getMaterialResults() as $result) {
            $mesnie->addToInventory($result->getMaterial(), $result->getQuantity());
        }

        return $mesnie;
    }
}
