<?php
/**
 * UserCommand.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Command;

/** Usages */
use App\Entity\Mesnie;
use App\Exception\UserException;
use App\Service\MesnieService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserCommand.
 *
 * @package App\Command
 */
class UserCommand extends AbstractAdminCommand
{
    const ARGUMENT_EMAIL    = "email";
    const ARGUMENT_FEMALE   = "female";

    /** @var UserService $userService */
    private $userService;

    /** @var MesnieService $mesnieService */
    private $mesnieService;

    /**
     * UserCommand constructor.
     *
     * @param UserService            $userService
     * @param MesnieService          $mesnieService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        UserService             $userService,
        MesnieService           $mesnieService,
        EntityManagerInterface  $entityManager
    ) {
        parent::__construct($entityManager);
        $this->userService = $userService;
        $this->mesnieService = $mesnieService;
    }

    /**
     * Set the configuration of the command
     */
    protected function configure()
    {
        $this
            ->setName('mesnie:user:create')
            ->setDescription('create a new user')
            ->setHelp("")
            ->addArgument(
                self::ARGUMENT_EMAIL,
                InputArgument::REQUIRED,
                "The email of the user."
            )
            ->addOption(
                self::ARGUMENT_FEMALE,
                'f',
                InputOption::VALUE_NONE,
                'is the first personage a female'
            );
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws UserException
     * @throws \App\Exception\FamilyException
     * @throws \App\Exception\PersonageException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setInputAndOutput($input, $output);

        $password = $this->askQuestion("Choose a password", true);
        $familyName = $this->askQuestion("Choose a family name");
        $firstName = $this->askQuestion("Choose a firstname");
        $isMale = $this->isMale();

        $user = $this->userService->createUser($this->getMail(), $password);
        $mesnie = $this->mesnieService->foundFirstMesnieInRandomCity($user, $familyName, $firstName, $isMale);
        $this->logResult($mesnie);
    }

    /**
     * @param Mesnie $mesnie
     */
    private function logResult(Mesnie $mesnie)
    {
        $this->log("User created !");
        $this->log("Mesnie of ".$mesnie->getFamily()->getName()." in ".$mesnie->getCity()->getName()." founded !");
        $this->log($mesnie->getHead()->getFirstName()." is the head of the mesnie.");
    }


    /**
     * @return string
     */
    private function getMail(): string
    {
        return $this->getArgument(self::ARGUMENT_EMAIL);
    }

    /**
     * @return bool
     */
    private function isMale(): bool
    {
        return !$this->getOption(self::ARGUMENT_FEMALE);
    }
}
