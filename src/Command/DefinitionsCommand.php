<?php
/**
 * DefinitionsCommand.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Command;

/** Usages */
use App\Service\SettingsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DefinitionsCommand
 * @package App\Command
 */
class DefinitionsCommand extends AbstractAdminCommand
{
    /** @var SettingsService $settingsService */
    private $settingsService;

    /**
     * DefinitionsCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param SettingsService        $settingsService
     */
    public function __construct(EntityManagerInterface $entityManager, SettingsService $settingsService)
    {
        parent::__construct($entityManager);
        $this->settingsService = $settingsService;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('mesnie:definitions:load')
            ->setDescription('Load definitions')
            ->setHelp(
                ''
            )
            ->addOption("all", "a", InputOption::VALUE_NONE)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \App\Exception\MaterialException
     * @throws \App\Exception\PlantException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setInputAndOutput($input, $output);
        $isAllSelected = $this->isAllSelected();

        $this->settingsService->loadAll($isAllSelected);

        $this->logResult($isAllSelected);
    }

    /**
     * @param bool $isAllSelected
     */
    private function logResult(bool $isAllSelected): void
    {
        if ($isAllSelected) {
            $this->log("Every definitions loaded !");
        } else {
            $this->log("Missing definitions loaded");
        }
    }

    /**
     * @return bool
     */
    private function isAllSelected(): bool
    {
        return $this->getOption("all");
    }
}
