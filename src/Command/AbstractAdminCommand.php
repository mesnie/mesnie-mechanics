<?php
/**
 * AbstractAdminCommand.php.
 * @author  mickael
 * @licence GNU GPLv3
 */

/** Namespace **/
namespace App\Command;

/** Usages */
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class AbstractAdminCommand
 * @package App\Command
 */
abstract class AbstractAdminCommand extends Command
{

    /** @var InputInterface */
    private $input;

    /** @var OutputInterface */
    private $output;

    /** @var EntityManagerInterface $entityManager */
    protected $entityManager;

    /**
     * AbstractAdminCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function setInputAndOutput(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @param string $argumentName
     * @return string|null
     */
    protected function getArgument(string $argumentName): ?string
    {
        return $this->input->getArgument($argumentName);
    }

    /**
     * @param string $optionName
     * @return bool|string|string[]|null
     */
    protected function getOption(string $optionName)
    {
        return $this->input->getOption($optionName);
    }

    /**
     * @param string $question
     * @param bool   $hidden
     * @return string
     */
    protected function askQuestion(string $question, bool $hidden = false): string
    {
        $questionEntity = new Question($question.PHP_EOL);
        if ($hidden) {
            $questionEntity->setHidden(true);
            $questionEntity->setHiddenFallback(false);
        }

        return $this->getHelper("question")->ask($this->input, $this->output, $questionEntity);
    }

    /**
     * @param string $log
     */
    protected function log(string $log): void
    {
        $this->output->writeln($log);
    }
}
