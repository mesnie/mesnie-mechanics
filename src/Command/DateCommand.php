<?php
/**
 * DateCommand.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Command;

/** Usages */
use App\Service\DateService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DateCommand.
 * DateCommand is used by CRON to update the server date of one week.
 *
 * @package App\Command
 */
class DateCommand extends AbstractAdminCommand
{
    /** @var DateService $dateService */
    private $dateService;

    /**
     * DateCommand constructor.
     * Autowire the DateService.
     *
     * @param EntityManagerInterface $entityManager
     * @param DateService            $dateService
     */
    public function __construct(EntityManagerInterface $entityManager, DateService $dateService)
    {
        parent::__construct($entityManager);
        $this->dateService = $dateService;
    }

    /**
     * Set the configuration of the command
     */
    protected function configure()
    {
        $this
            ->setName('mesnie:date:upweek')
            ->setDescription('takes server date to next week')
            ->setHelp(
                'Takes server date to next week. "
                ."Updates month and year if appropriate. "
                ."Grows plants if appropriate.'
            )
        ;
    }

    /**
     * Upon execution, the command uses the upweek() method of the DateService
     * and logs the new date.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setInputAndOutput($input, $output);

        try {
            $log = "A new week has started : " . $this->dateService->upWeek()->getEnglish();
        } catch (\Exception $e) {
            $log = $e->getMessage();
        }
        $this->log($log);
    }
}
