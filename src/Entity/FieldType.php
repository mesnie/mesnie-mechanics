<?php
/**
 * FieldType.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\DescribedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FieldType
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\FieldTypeRepository")
 */
class FieldType extends DescribedEntity
{
    use IdTrait;

    /**
     * The worktypes allowed on this fieldtype.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\WorkType", mappedBy="fieldType")
     */
    private $workTypes;

    /**
     * FieldType constructor.
     * Needs name and human name.
     *
     * @param string $name
     * @param string $description
     * @throws \Exception
     */
    public function __construct(string $name, string $description = "")
    {
        parent::__construct($name, $description);
        $this->workTypes = new ArrayCollection();
    }

    /**
     * @return Collection|WorkType[]
     */
    public function getWorkTypes(): Collection
    {
        return $this->workTypes;
    }

    /**
     * @param WorkType $workType
     * @return FieldType
     */
    public function addWorkType(WorkType $workType): self
    {
        if (!$this->workTypes->contains($workType)) {
            $this->workTypes[] = $workType;
        }

        return $this;
    }

    /**
     * @param WorkType $workType
     * @return FieldType
     */
    public function removeWorkType(WorkType $workType): self
    {
        if ($this->workTypes->contains($workType)) {
            $this->workTypes->removeElement($workType);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $fieldType = [
            "id"            => $this->getId(),
            "name"          => $this->getName(),
            "identifier"    => $this->getIdentifier()
        ];

        return $fieldType;
    }
}
