<?php
/**
 * User.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\UserException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    use IdTrait;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var array $roles
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string $password The hashed password
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var Mesnie $mesnie
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Mesnie", mappedBy="user", cascade={"persist", "remove"})
     */
    private $mesnie;

    /**
     * User constructor.
     *
     * @param string $email
     * @throws UserException
     */
    public function __construct(string $email)
    {
        $this->setEmail($email);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     * @throws UserException
     */
    private function setEmail(string $email): self
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new UserException(
                "email not valid",
                UserException::INVALID_EMAIL
            );
        }
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Mesnie|null
     */
    public function getMesnie(): ?Mesnie
    {
        return $this->mesnie;
    }

    /**
     * @param Mesnie|null $mesnie
     * @return User
     * @throws UserException
     */
    public function setMesnie(?Mesnie $mesnie): self
    {
        if ($mesnie && $mesnie->getUser() !== $this) {
            throw new UserException(
                "mesnie not related to user",
                UserException::MESNIE_NOT_RELATED,
                $this
            );
        }
        $this->mesnie = $mesnie;

        return $this;
    }
}
