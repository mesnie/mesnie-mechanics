<?php
/**
 * WorkType.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\DescribedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class WorkType
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\WorkTypeRepository")
 */
class WorkType extends DescribedEntity
{
    use IdTrait;

    /**
     * @var FieldType|null $fieldType the fieldType needed for the workType. Null if none.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FieldType", inversedBy="workTypes")
     */
    private $fieldType;

    /**
     * @var FieldType|null $fieldTypeResult the new type of the field after finishing the work. Empty if none.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FieldType", inversedBy="workTypes")
     */
    private $fieldTypeResult;

    /**
     * @var int the number of weeks of work needed to finish the work.
     *
     * @ORM\Column(type="integer")
     */
    private $weeksNeeded;

    /**
     * @var MaterialQuantity $materialNeeds the materials needed to start the work. Empty if none.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MaterialQuantity", mappedBy="neededByWork")
     */
    private $materialNeeds;

    /**
     * @var MaterialQuantity $materialResults the materials gained after finishing the work. Empty if none.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MaterialQuantity", mappedBy="resultFromWork")
     */
    private $materialResults;

    /**
     * @var Plant|null $plantResult the plant growing on the field after the work is finished.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Plant")
     */
    private $plantResult;

    /**
     * @var Plant|null $plantNeeded the fully grown plant needed to work on field, like to harvest it.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Plant")
     */
    private $plantNeeded;

    /**
     * WorkType constructor.
     *
     * @param string         $name
     * @param int            $weeksNeeded
     * @param string         $description
     * @param FieldType|null $fieldType
     * @param FieldType|null $fieldTypeResult
     * @param Plant|null     $plantResult
     * @param Plant|null     $plantNeeded
     * @throws \Exception
     */
    public function __construct(
        string      $name,
        int         $weeksNeeded = 1,
        string      $description = "",
        ?FieldType  $fieldType = null,
        ?FieldType  $fieldTypeResult = null,
        ?Plant      $plantResult = null,
        ?Plant      $plantNeeded = null
    ) {
        parent::__construct($name, $description);
        $this
            ->setFieldType($fieldType)
            ->setWeeksNeeded($weeksNeeded)
            ->setFieldTypeResult($fieldTypeResult)
            ->setPlantResult($plantResult)
            ->setPlantNeeded($plantNeeded)
        ;
        $this->materialNeeds = new ArrayCollection();
        $this->materialResults = new ArrayCollection();
    }

    /**
     * @return FieldType|null
     */
    public function getFieldType(): ?FieldType
    {
        return $this->fieldType;
    }

    /**
     * @param FieldType|null $fieldType
     * @return WorkType
     */
    private function setFieldType(?FieldType $fieldType): self
    {
        $this->fieldType = $fieldType;
        if ($fieldType) {
            $fieldType->addWorkType($this);
        }

        return $this;
    }

    /**
     * @return FieldType|null
     */
    public function getFieldTypeResult(): ?FieldType
    {
        return $this->fieldTypeResult;
    }

    /**
     * @param FieldType|null $fieldTypeResult
     * @return WorkType
     */
    private function setFieldTypeResult(?FieldType $fieldTypeResult): WorkType
    {
        $this->fieldTypeResult = $fieldTypeResult;

        return $this;
    }

    /**
     * @return int
     */
    public function getWeeksNeeded(): int
    {
        return $this->weeksNeeded;
    }

    /**
     * @param int $weeksNeeded
     * @return WorkType
     */
    private function setWeeksNeeded(int $weeksNeeded): self
    {
        $this->weeksNeeded = $weeksNeeded;

        return $this;
    }

    /**
     * @param int $level
     * @return array
     */
    public function toArray($level = 0): array
    {
        $workType = [
            "id"            => $this->getId(),
            "name"          => $this->getName(),
            "identifier"    => $this->getIdentifier()
        ];
        if (0 === $level) {
            return $workType;
        }

        $workType["description"] = $this->getDescription();
        $workType["weeksNeeded"] = $this->getWeeksNeeded();

        return $workType;
    }

    /**
     * @return Collection|MaterialQuantity[]
     */
    public function getMaterialNeeds(): Collection
    {
        return $this->materialNeeds;
    }

    /**
     * @param MaterialQuantity $materialNeed
     * @return WorkType
     */
    public function addMaterialNeed(MaterialQuantity $materialNeed): self
    {
        if (!$this->materialNeeds->contains($materialNeed)) {
            $this->materialNeeds[] = $materialNeed;
            $materialNeed->setNeededByWork($this);
        }

        return $this;
    }

    /**
     * @return Collection|MaterialQuantity[]
     */
    public function getMaterialResults(): Collection
    {
        return $this->materialResults;
    }

    /**
     * @param MaterialQuantity $materialResult
     * @return WorkType
     */
    public function addMaterialResult(MaterialQuantity $materialResult): self
    {
        if (!$this->materialResults->contains($materialResult)) {
            $this->materialResults[] = $materialResult;
            $materialResult->setResultFromWork($this);
        }

        return $this;
    }

    /**
     * @return Plant|null
     */
    public function getPlantResult(): ?Plant
    {
        return $this->plantResult;
    }

    /**
     * @param Plant|null $plantResult
     * @return WorkType
     */
    private function setPlantResult(?Plant $plantResult): WorkType
    {
        $this->plantResult = $plantResult;
        return $this;
    }

    /**
     * @return Plant|null
     */
    public function getPlantNeeded(): ?Plant
    {
        return $this->plantNeeded;
    }

    /**
     * @param Plant|null $plantNeeded
     * @return WorkType
     */
    private function setPlantNeeded(?Plant $plantNeeded): WorkType
    {
        $this->plantNeeded = $plantNeeded;
        return $this;
    }

    /**
     * @param Field $field
     * @return bool
     */
    public function isFieldOk(Field $field): bool
    {
        $fieldTypeOk = (!$this->getFieldType() || $this->getFieldType() === $field->getType());
        $plantOk = (
            !$this->getPlantNeeded()
            || ($this->getPlantNeeded() === $field->getGrowingPlant() && $field->isGrown())
        );

        return ($fieldTypeOk && $plantOk);
    }

    /**
     * Check the material needs.
     *
     * @param Mesnie $mesnie
     * @return bool
     */
    public function isMesnieOk(Mesnie $mesnie): bool
    {
        foreach ($this->getMaterialNeeds() as $materialNeed) {
            if ($materialNeed->getQuantity() > $mesnie->getQuantityFromInventory($materialNeed->getMaterial())) {
                return false;
            }
        }

        return true;
    }
}
