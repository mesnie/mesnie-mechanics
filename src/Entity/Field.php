<?php
/**
 * Field.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\FieldException;
use App\Exception\WorkException;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Field
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\FieldRepository")
 */
class Field
{
    use IdTrait;

    /**
     * The type of the field.
     *
     * @var FieldType $type
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FieldType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * The Mesnie owning the field.
     *
     * @var Mesnie $mesnie
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Mesnie", inversedBy="fields")
     */
    private $mesnie;

    /**
     * The type of work in progress on the field.
     * Null if no work.
     *
     * @var WorkType $workInProgress
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkType")
     */
    private $workInProgress;

    /**
     * @todo : non nullable
     *
     * The state of progress of the current work on the field.
     * Shall up by 1 on each works week.
     * When equals to work type weeks needed, the work is over.
     *
     * @var int $progress
     *
     * @ORM\Column(type="integer")
     */
    private $progress;

    /**
     * The personage working on the field this week.
     * Null if noone.
     * Reset each week.
     *
     * @var Personage $worker
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Personage", inversedBy="workField", cascade={"persist", "remove"})
     */
    private $worker;

    /**
     * The plant currently growing on the field, if any.
     *
     * @var Plant $growingPlant
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Plant")
     */
    private $growingPlant;

    /**
     * The current growth of the growing plant.
     * Null/zero if no growing plant.
     * Up every month.
     * When equals to the Plant growth time, it is grown.
     *
     * @var int $growth
     *
     * @ORM\Column(type="integer")
     */
    private $growth;

    /**
     * Field constructor.
     * Needs FieldType.
     * Optional Mesnie.
     *
     * @param FieldType $type
     * @param Mesnie|null $mesnie
     */
    public function __construct(FieldType $type, Mesnie $mesnie = null)
    {
        $this
            ->setType($type)
            ->setMesnie($mesnie)
            ->setProgress(0)
            ->setGrowth(0)
        ;
    }

    /**
     * @return FieldType
     */
    public function getType(): FieldType
    {
        return $this->type;
    }

    /**
     * @param FieldType $type
     * @return Field
     */
    public function setType(FieldType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Mesnie|null
     */
    public function getMesnie(): ?Mesnie
    {
        return $this->mesnie;
    }

    /**
     * @param Mesnie|null $mesnie
     * @return Field
     */
    public function setMesnie(?Mesnie $mesnie): self
    {
        $this->mesnie = $mesnie;

        return $this;
    }

    /**
     * @return WorkType|null
     */
    public function getWorkInProgress(): ?WorkType
    {
        return $this->workInProgress;
    }

    /**
     * Reset to 0 any progress already made.
     *
     * @param WorkType|null $workInProgress
     * @return Field
     */
    public function setWorkInProgress(?WorkType $workInProgress): self
    {
        $this->workInProgress = $workInProgress;
        $this->setProgress(0);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getProgress(): ?int
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     * @return Field
     */
    public function setProgress(int $progress): self
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Add one week of progress to the work on the Field.
     * Checks that there is a work in progress on the field.
     *
     * @return int
     * @throws FieldException
     */
    public function upProgress(): int
    {

        if (!$this->getWorkInProgress()) {
            throw new FieldException(
                "field ".$this->getId()." cannot progress without a work in progress",
                FieldException::NO_WORK,
                null,
                $this
            );
        }

        $progress = $this->getProgress();
        if (!$progress) {
            $progress = 0;
        }
        $progress++;

        $this->setProgress($progress);

        return $progress;
    }

    /**
     * Compares the progress of the work on the field
     * to the weeks of work needed by its workType.
     * Also returns false if no work underdone.
     *
     * @return bool
     */
    public function isWorkFinished(): bool
    {
        if ($this->getWorkInProgress() && ($this->getWorkInProgress()->getWeeksNeeded() <= $this->getProgress())) {
            return true;
        }

        return false;
    }

    /**
     * @return Personage|null
     */
    public function getWorker(): ?Personage
    {
        return $this->worker;
    }

    /**
     * Checks that there is a work in progress on the field.
     *
     * @param Personage|null $worker
     * @return Field
     * @throws WorkException
     */
    public function setWorker(?Personage $worker): self
    {
        if ($worker && !$this->getWorkInProgress()) {
            throw new WorkException(
                "cannot set a worker on the field ".$this->getId()." since it has no work in progress",
                WorkException::ERROR_NO_WORK
            );
        }
        $this->worker = $worker;

        return $this;
    }

    /**
     * @return Plant|null
     */
    public function getGrowingPlant(): ?Plant
    {
        return $this->growingPlant;
    }

    /**
     * @param Plant|null $growingPlant
     * @return Field
     */
    public function setGrowingPlant(?Plant $growingPlant): self
    {
        $this->growingPlant = $growingPlant;
        $this->setGrowth(0);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getGrowth(): ?int
    {
        return $this->growth;
    }

    /**
     * Add one month to the growth of the plant.
     * Checks that there is a plant growing on the field.
     *
     * @return int
     * @throws FieldException
     */
    public function upGrowth(): int
    {
        if (!$this->getGrowingPlant()) {
            throw new FieldException(
                "field ".$this->getId()." cannot grow without a growing plant",
                FieldException::NO_GROWING_PLANT,
                null,
                $this
            );
        }

        $growth = $this->getGrowth();
        if (!$growth) {
            $growth = 0;
        }
        $growth++;

        $this->setGrowth($growth);

        return $growth;
    }

    /**
     * @param int|null $growth
     * @return Field
     */
    private function setGrowth(?int $growth): self
    {
        $this->growth = $growth;

        return $this;
    }

    /**
     * Compares the growth of the plant on the field
     * to the number needed for the plant to be fully grown up.
     * Also returns false if no growing plant on the field.
     *
     * @return bool
     */
    public function isGrown(): bool
    {
        if ($this->getGrowingPlant() && $this->getGrowth() >= $this->getGrowingPlant()->getGrowthTime()) {
            return true;
        }

        return false;
    }

    /**
     * Returns an array with all the relevant informations for the field
     *
     * @return array
     */
    public function toArray(): array
    {
        $field = [
            "id"        => $this->getId(),
            "progress"  => $this->getProgress(),
            "type"      => $this->getType()->toArray(),
            "mesnie"    => $this->getMesnie()->toArray(0)
        ];

        return $field;
    }

    /**
     * Returns a JSON built with the toArray() method
     *
     * @return string
     */
    public function toJSON(): string
    {
        return json_encode($this->toArray());
    }
}
