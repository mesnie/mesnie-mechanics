<?php
/**
 * Mesnie.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\CityTrait;
use App\Entity\Traits\IdTrait;
use App\Exception\FamilyException;
use App\Exception\FieldException;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use App\Exception\PersonageException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Mesnie
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MesnieRepository")
 */
class Mesnie
{
    use IdTrait;
    use CityTrait;

    /**
     * The head is the personage, member of the mesnie,
     * who represents and rules it.
     * It will typically be the dominant male figure (father, husband...)
     * On his death, another member must be selected to be the new head.
     *
     * @var Personage $head
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Personage")
     * @ORM\JoinColumn(nullable=false)
     */
    private $head;

    /**
     * The list of Personage members of the mesnie,
     * i.e. living in the house or its dependancies.
     * A Personage cannot be members of several mesnies.
     * Upon death of a member, he is removed from the list.
     *
     * @var ArrayCollection|Personage[] $members
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Personage", mappedBy="mesnie")
     */
    private $members;

    /**
     * Every Mesnie must be linked to a Family,
     * regrouping several mesnies linked by blood.
     *
     * @var Family $family
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Family", inversedBy="mesnies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $family;

    /**
     * The inventory is a list of MaterialQuantities,
     * representing all the materials owned by the Mesnie.
     *
     * @var ArrayCollection|MaterialQuantity[] $inventory
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\MaterialQuantity",
     *     mappedBy="owner",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"})
     */
    private $inventory;

    /**
     * The list of fields owned by the Mesnie.
     * They must be located in the same city than the mesnie.
     *
     * @var ArrayCollection|Field[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Field", mappedBy="mesnie")
     */
    private $fields;

    /**
     * The application user owning this mesnie.
     * He is the only one allowed to take actions for the mesnie.
     * A Mesnie without user must be seen as a NPC.
     *
     * @var User $user
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="mesnie", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * The money owned by the whole mesnie.
     * 1 pound = 20 sols = 240 denars = 480 obols = 960 pites = 1920 half-pites
     * 1 sol = 12 denars = 24 obols = 48 pites = 96 half-pites
     * 1 denar = 2 obols = 4 pites = 8 half-pites
     * 1 obol = 2 pites = 4 half-pites
     * 1 pite = 2 half-pites
     *
     * @var int $money in half-pites
     *
     * @ORM\Column(type="integer")
     */
    private $money;

    /**
     * Mesnie constructor.
     * May receive optional family and user.
     *
     * @param City          $city
     * @param Family|null   $family
     * @param User|null     $user
     * @throws MoneyException
     */
    public function __construct(City $city, ?Family $family = null, ?User $user = null)
    {
        $this
            ->setCity($city)
            ->setFamily($family)
            ->setUser($user)
            ->setMoney(0)
        ;
        $this->members = new ArrayCollection();
        $this->inventory = new ArrayCollection();
        $this->fields = new ArrayCollection();
    }

    /**
     * @return Personage|null
     */
    public function getHead(): ?Personage
    {
        return $this->head;
    }

    /**
     * Checks the head is a member of the Mesnie.
     *
     * @param Personage $head
     * @return Mesnie
     * @throws PersonageException 161
     */
    public function setHead(Personage $head): self
    {
        if (!$this->isMember($head)) {
            throw new PersonageException(
                "The personage ".$head->getId(). "cannot be head of the mesnie ".$this->getId()." without being member",
                PersonageException::ERROR_MESNIE_HEAD,
                $head
            );
        }
        $this->head = $head;

        return $this;
    }

    /**
     * @return Collection|Personage[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    /**
     * @param Personage $member
     * @return Mesnie
     * @throws FamilyException
     */
    public function addMember(Personage $member): self
    {
        if ($member->getMesnie() !== $this) {
            throw new FamilyException(
                "the personage ".$member->getId()." is not affiliated to the mesnie ".$this->getId(),
                FamilyException::WRONG_MESNIE
            );
        }
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setMesnie($this);
        }

        return $this;
    }

    /**
     * Also remove the member affiliation to the mesnie.
     *
     * @param Personage $member
     * @return Mesnie
     */
    public function removeMember(Personage $member): self
    {
        if ($this->isMember($member)) {
            $this->members->removeElement($member);
            if ($member->getMesnie() === $this) {
                $member->setMesnie(null);
            }
        }

        return $this;
    }

    /**
     * @return Family|null
     */
    public function getFamily(): ?Family
    {
        return $this->family;
    }

    /**
     * @param Family|null $family
     * @return Mesnie
     */
    public function setFamily(?Family $family): self
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Checks if the given personage is in the list of members.
     *
     * @param Personage $personage
     * @return bool
     */
    public function isMember(Personage $personage): bool
    {
        return $this->members->contains($personage);
    }

    /**
     * Tries to find a heir for the mesnie.
     * First, checks if the head has an heir, who is over 15 and member of the mesnie.
     * Second, get the oldest relative male of the members, head excepted.
     * Third, get the oldest relative of the members (female), head excepted.
     * Fourth, get the oldest member, head excepted.
     * If nothing found, return null.
     *
     * @param int $year
     * @return Personage|null
     * @throws PersonageException 151
     */
    public function getMemberHeir(int $year): ?Personage
    {
        if ($this->getHead() && $this->getHead()->getHeir()
            && 15 <= $this->getHead()->getHeir()->getAge($year)
            && $this->isMember($this->getHead()->getHeir())
        ) {
            return $this->getHead()->getHeir();
        }
        $potentialHeirs = [
            "relative"  => [
                "male"      => null,
                "female"      => null
            ],
            "other"  => [
                "male"      => null,
                "female"      => null
            ]
        ];
        foreach ($this->members as $member) {
            if ($this->getHead() !== $member) {
                $relativity = "other";
                if ($this->getHead() && $this->getHead()->consanguinity($member) !== 0) {
                    $relativity = "relative";
                }
                $sex = $member->isMale() ? "male" : "female";

                /** @var Personage $potentialHeir */
                $potentialHeir = $potentialHeirs[$relativity][$sex];
                if (!$potentialHeir || $potentialHeir->getBirthYear() > $member->getBirthYear()) {
                    $potentialHeirs[$relativity][$sex] = $member;
                }
            }
        }
        $heir = $potentialHeirs["relative"]["male"];
        if (!$heir) {
            $heir = $potentialHeirs["relative"]["female"];
        }
        if (!$heir) {
            $heir = $potentialHeirs["other"]["male"];
        }
        if (!$heir) {
            $heir = $potentialHeirs["other"]["female"];
        }

        return $heir;
    }

    /**
     * Returns an ArrayCollection containing,
     * as "men", an ArrayCollection of nubile men,
     * as "women", an ArrayCollection of nubile women,
     *
     * @param int $year
     * @return ArrayCollection
     * @throws PersonageException 151
     */
    public function getNubileMembers(int $year): ArrayCollection
    {
        $nubileMen = new ArrayCollection();
        $nubileWomen = new ArrayCollection();
        foreach ($this->getMembers() as $member) {
            if (!$member->getSpouse()
                && $member->isNubile($year)
            ) {
                if ($member->isMale()) {
                    $nubileMen->add($member);
                } else {
                    $nubileWomen->add($member);
                }
            }
        }

        return new ArrayCollection([
            "men"   => $nubileMen,
            "women" => $nubileWomen
        ]);
    }

    /**
     * Returns an ArrayCollection of the members over 14 without any work assigned.
     *
     * @param int $year
     * @return ArrayCollection
     * @throws PersonageException 151
     */
    public function getInactiveMembers(int $year): ArrayCollection
    {
        $inactiveMembers = new ArrayCollection();
        foreach ($this->getMembers() as $member) {
            if (!$member->getWorkField() && $member->getAge($year) > 14) {
                $inactiveMembers->add($member);
            }
        }

        return $inactiveMembers;
    }

    /**
     * @return Collection|MaterialQuantity[]
     */
    public function getInventory(): Collection
    {
        return $this->inventory;
    }

    /**
     * Add a quantity of material to the inventory.
     * If the material is already present, add the new quantity,
     * else create a new MaterialQuantity with the given Material and quantity
     * and add it to the inventory.
     *
     * @param Material $material
     * @param int $add
     * @return int
     * @throws MaterialException 301
     */
    public function addToInventory(Material $material, int $add): int
    {
        foreach ($this->getInventory() as $materialQuantity) {
            if ($materialQuantity->getMaterial() === $material) {
                return $materialQuantity->add($add);
            }
        }

        $newMaterialQuantity = new MaterialQuantity($material, $add);
        $newMaterialQuantity->setOwner($this);
        $this->getInventory()->add($newMaterialQuantity);

        return $add;
    }

    /**
     * Removes a quantity of material from the inventory.
     * Finds the material in the inventory and substract the quantity from it.
     * If the quantity reach 0, remove the MaterialQuantity from the inventory.
     * Throws an exception if the material is not found in the inventory.
     *
     * @param Material $material
     * @param int $remove
     * @return int
     * @throws MaterialException 301, 303
     */
    public function removeFromInventory(Material $material, int $remove): int
    {
        foreach ($this->getInventory() as $materialQuantity) {
            if ($materialQuantity->getMaterial() === $material) {
                if ($materialQuantity->getQuantity() === $remove) {
                    $this->getInventory()->removeElement($materialQuantity);
                    return 0;
                }
                return $materialQuantity->remove($remove);//301
            }
        }
        throw new MaterialException(
            "The material ".$material->getId()." is not present in the mesnie ".$this->getId()." inventory",
            MaterialException::ERROR_INVENTORY_ENOUGH,
            null
        );
    }

    /**
     * Returns the quantity of the given Material in the inventory.
     * Parse the inventory to find a MaterialQuantity with the given Material
     * Then returns the quantity associated.
     * If no MaterialQuantity found, returns 0.
     *
     * @param Material $material
     * @return int
     */
    public function getQuantityFromInventory(Material $material): int
    {
        foreach ($this->getInventory() as $materialQuantity) {
            if ($materialQuantity->getMaterial() === $material) {
                return $materialQuantity->getQuantity();
            }
        }

        return 0;
    }

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    /**
     * @param Field $field
     * @return Mesnie
     * @throws FieldException
     */
    public function addField(Field $field): self
    {
        if ($field->getMesnie() !== $this) {
            throw new FieldException(
                "the field ".$field->getId()." is not affiliated to the mesnie ".$this->getId(),
                FieldException::WRONG_OWNER,
                null,
                $field
            );
        }

        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setMesnie($this);
        }

        return $this;
    }

    /**
     * Also remove the Field affiliation to this mesnie.
     *
     * @param Field $field
     * @return Mesnie
     */
    public function removeField(Field $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            // set the owning side to null (unless already changed)
            if ($field->getMesnie() === $this) {
                $field->setMesnie(null);
            }
        }

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Mesnie
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return Mesnie
     * @throws MoneyException
     */
    private function setMoney(int $money): Mesnie
    {
        if (0 > $money) {
            throw new MoneyException($money . " under 0", MoneyException::VALUE_NEGATIVE);
        }
        $this->money = $money;

        return $this;
    }

    /**
     * @param int $add
     * @return int
     * @throws MoneyException
     */
    public function addMoney(int $add): int
    {
        $this->setMoney($this->getMoney() + $add);

        return $this->getMoney();
    }

    /**
     * @param int $remove
     * @return int
     * @throws MoneyException
     */
    public function removeMoney(int $remove): int
    {
        return $this->addMoney(-$remove);
    }

    /**
     * @return array
     */
    private function toArrayMembers(): array
    {
        $members = [];
        foreach ($this->getMembers() as $member) {
            $memberArray = [
                "id"    => $member->getId(),
                "firstName" => $member->getFirstName(),
                "birthYear" => $member->getBirthYear()
            ];
            if ($member->getFather()) {
                $memberArray["fatherId"] = $member->getFather()->getId();
            }
            if ($member->getMother()) {
                $memberArray["motherId"] = $member->getMother()->getId();
            }
            $members[] = $memberArray;
        }

        return $members;
    }

    /**
     * @return array
     */
    private function toArrayFields(): array
    {
        $fields = [];
        foreach ($this->getFields() as $field) {
            $fields[] = [
                "id"        => $field->getId(),
                "type"      => $field->getType(),
                "evolution" => $field->getProgress()
            ];
        }

        return $fields;
    }

    /**
     * @param int $level
     * @return array
     */
    public function toArray($level = 0): array
    {
        $mesnie = [
            "id"    => $this->getId()
        ];

        $mesnie["head"]     = $this->getHead()->toArray(0);
        $mesnie["family"]   = $this->getFamily()->toArray();
        if (0 == $level) {
            return $mesnie;
        }

        $members = $this->toArrayMembers();
        $fields = $this->toArrayFields();
        $mesnie["members"]  = $members;
        $mesnie["fields"]   = $fields;

        return $mesnie;
    }
}
