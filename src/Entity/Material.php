<?php
/**
 * Material.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\DescribedEntity;
use App\Entity\Utils\MaterialTypeInterface;
use App\Exception\MaterialException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Material
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\MaterialRepository")
 */
class Material extends DescribedEntity implements MaterialTypeInterface
{
    use IdTrait;

    /**
     * The type of the material, e.g. "cereal".
     *
     * @var string $type
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * The value of the Material in cities.
     *
     * @var ArrayCollection|MaterialValue[] $materialValues
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\MaterialValue",
     *     mappedBy="material",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"}
     * )
     */
    private $materialValues;

    /**
     * Material constructor.
     * Needs name and valid type.
     *
     * @param string $name
     * @param string $type
     * @param string $description
     * @throws MaterialException 302
     * @throws \Exception
     */
    public function __construct(string $name, string $type, string $description = "")
    {
        parent::__construct($name, $description);
        $this->setType($type);
        $this->materialValues = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Checks that the given type is in the defined type list.
     *
     * @param string $type
     * @return Material
     * @throws MaterialException 302
     */
    private function setType(string $type): self
    {
        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new MaterialException(
                "The type $type is not allowed",
                MaterialException::ERROR_TYPE_NOT_ALLOWED
            );
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $material = [
            "id"            => $this->getId(),
            "name"          => $this->getName(),
            "identifier"    => $this->getIdentifier(),
            "type"          => $this->getType()
        ];

        return $material;
    }

    /**
     * @return Collection|MaterialValue[]
     */
    public function getMaterialValues(): Collection
    {
        return $this->materialValues;
    }

    /**
     * @param MaterialValue $materialValue
     * @return Material
     */
    public function addMaterialValue(MaterialValue $materialValue): Material
    {
        if (!$this->materialValues->contains($materialValue)) {
            $this->materialValues[] = $materialValue;
        }

        return $this;
    }

    /**
     * @param MaterialValue $materialValue
     * @return Material
     */
    public function removeMaterialValue(MaterialValue $materialValue): Material
    {
        if ($this->materialValues->contains($materialValue)) {
            $this->materialValues->removeElement($materialValue);
        }

        return $this;
    }
}
