<?php
/**
 * DescriptionTrait.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Traits;

/**
 * Trait DescriptionTrait
 * Contains an orm string of length 1024 named description, its getter and its private setter.
 * The setter is private as it is intended to be only used by the constructor.
 *
 * @package App\Entity\Traits
 */
trait DescriptionTrait
{
    /**
     * The historical and hint description of the entity.
     *
     * @var string $description
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return self
     */
    private function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
