<?php
/**
 * CityTrait.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Traits;

/** Usages */
use App\Entity\City;

/**
 * Trait CityTrait
 * @package App\Entity\Traits
 */
trait CityTrait
{
    /**
     * @var City $city
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @param City $city
     * @return self
     */
    protected function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }
}
