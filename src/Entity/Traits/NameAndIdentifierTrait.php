<?php
/**
 * NameAndIdentifierTrait.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Traits;

/**
 * Trait NameAndIdentifierTrait
 * @package App\Entity\Traits
 */
trait NameAndIdentifierTrait
{

    /**
     * The human readable name of the entity
     *
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * The slugified system name of the entity
     *
     * @var string $identifier
     *
     * @ORM\Column(type="string", length=255)
     */
    private $identifier;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $name
     * @return NameAndIdentifierTrait
     * @throws \Exception
     */
    private function setNameAndIdentifier(string $name): self
    {
        if (4 > strlen($name)) {
            throw new \Exception(
                "The name $name is too short"
            );
        }

        $this->name = $name;

        $identifier = $name;
        $replaceCharacters = [
            'e' => ['è', 'é', 'ê', 'ë'],
            'a' => ['à', 'â'],
            'c' => ['ç'],
            'u' => ['ù', 'û', 'ü'],
            'o' => ['ô', 'ö'],
            'i' => ['î', 'ï'],
            'oe' => ['œ'],
        ];
        foreach ($replaceCharacters as $valid => $invalid) {
            $identifier = str_replace($invalid, $valid, $identifier);
        }

        $delimiter = '_';
        $identifier = preg_replace('~[^\pL\d]+~u', $delimiter, $identifier);
        $identifier = preg_replace('~_+~', $delimiter, $identifier);
        $identifier = trim($identifier, $delimiter);
        $identifier = strtolower($identifier);
        $this->identifier = $identifier;

        return $this;
    }
}
