<?php
/**
 * Pregnancy.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\ChildException;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Pregnancy
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\PregnancyRepository")
 */
class Pregnancy
{
    const PREGNANCY_PROBABILITY = 10;

    use IdTrait;

    /**
     * @ORM\Column(type="integer")
     */
    private $month;

    /**
     * @ORM\Column(type="integer")
     */
    private $childrenNumber;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Personage", inversedBy="pregnancy")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mother;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Personage")
     * @ORM\JoinColumn(nullable=false)
     */
    private $father;

    /**
     * Pregnancy constructor.
     * @param Personage $mother
     * @param Personage $father
     * @param int       $month
     * @param int       $childrenNumber
     * @throws ChildException
     */
    public function __construct(Personage $mother, Personage $father, int $month = 0, int $childrenNumber = 1)
    {
        $this
            ->setMother($mother)
            ->setFather($father)
            ->setMonth($month)
            ->setChildrenNumber($childrenNumber);
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @param int $month
     * @return Pregnancy
     * @throws ChildException
     */
    private function setMonth(int $month): self
    {
        if (0 > $month || $month > 9) {
            throw new ChildException(
                "a pregnancy cannot be long of ".$month." months",
                ChildException::WRONG_PREGNANCY_MONTH
            );
        }

        $this->month = $month;

        return $this;
    }

    /**
     * @return Pregnancy
     * @throws ChildException
     */
    public function upMonth(): self
    {
        $this->setMonth($this->getMonth()+1);

        return $this;
    }

    /**
     * @return int
     */
    public function getChildrenNumber(): int
    {
        return $this->childrenNumber;
    }

    /**
     * @param int $childrenNumber
     * @return Pregnancy
     * @throws ChildException
     */
    private function setChildrenNumber(int $childrenNumber): self
    {
        if (0 >= $childrenNumber) {
            throw new ChildException(
                "$childrenNumber is not a valid number of children in a pregnancy",
                ChildException::WRONG_PREGNANCY_NUMBER
            );
        }
        $this->childrenNumber = $childrenNumber;

        return $this;
    }

    /**
     * @return Personage
     */
    public function getMother(): Personage
    {
        return $this->mother;
    }

    /**
     * @param Personage $mother
     * @return Pregnancy
     * @throws ChildException
     */
    private function setMother(Personage $mother): self
    {
        if ($mother->isMale()) {
            throw new ChildException(
                "personage ".$mother->getId()." is not a woman and so cannot be mother",
                ChildException::ERROR_PARENT_SEX,
                null,
                null,
                $mother
            );
        }

        $this->mother = $mother;

        return $this;
    }

    /**
     * @return Personage
     */
    public function getFather(): Personage
    {
        return $this->father;
    }

    /**
     * @param Personage $father
     * @return Pregnancy
     * @throws ChildException
     */
    private function setFather(Personage $father): self
    {
        if (!$father->isMale()) {
            throw new ChildException(
                "personage ".$father->getId()." is not a man and so cannot be father",
                ChildException::ERROR_PARENT_SEX,
                null,
                $father
            );
        }

        $this->father = $father;

        return $this;
    }

    /**
     * @param int $level
     * @return array
     */
    public function toArray(int $level = 0): array
    {
        $array = [
            "id"                => $this->getId(),
            "month"             => $this->getMonth(),
            "childrenNumber"    => $this->getChildrenNumber()
        ];
        if ($level === 0) {
            return $array;
        }

        $array["father"] = $this->getFather() ?? $this->getFather()->toArray(0);
        $array["mother"] = $this->getMother() ?? $this->getMother()->toArray(0);
        return $array;
    }
}
