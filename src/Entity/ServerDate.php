<?php
/**
 * ServerDate.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\EnglishDateInterface;
use App\Exception\DateException;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ServerDate
 * Represents game date.
 * There should only be one in database.
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ServerDateRepository")
 */
class ServerDate implements EnglishDateInterface
{

    use IdTrait;

    /**
     * The week of the month, from 1 to 4.
     *
     * @ORM\Column(type="integer")
     *
     * @var integer $week 1-4
     */
    private $week;

    /**
     * The month as an int from 1 to 12.
     * @ORM\Column(type="integer")
     *
     * @var integer $month 1-12
     */
    private $month;

    /**
     * The year.
     *
     * @ORM\Column(type="integer")
     *
     * @var integer $year
     */
    private $year;

    /**
     * ServerDate constructor.
     *
     * @param int $year
     * @param int $month
     * @param int $week
     * @throws DateException
     */
    public function __construct(int $year, int $month, int $week)
    {
        $this
            ->setYear($year)
            ->setMonth($month)
            ->setWeek($week)
            ;
    }

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * Returns week like day of the month :
     * 1, 8, 15, 22.
     *
     * @return int
     */
    public function getWeekDayLike(): int
    {
        return ($this->getWeek()-1)*7+1;
    }

    /**
     * @param int $week
     * @return ServerDate
     * @throws DateException
     */
    private function setWeek(int $week): self
    {
        if (1 > $week || 4 < $week) {
            throw new DateException("time week must be between 1 and 4", DateException::INVALID_VALUE);
        }
        $this->week = $week;

        return $this;
    }

    /**
     * Also up the month if upping from 4th week to 1st.
     *
     * @return ServerDate
     * @throws \Exception
     */
    public function upWeek(): self
    {
        $this->week++;
        if (4 < $this->getWeek()) {
            $this->setWeek(1);
            $this->upMonth();
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isNewMonth(): bool
    {
        return (1 === $this->week);
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * Returns the month in its english name.
     *
     * @return string
     */
    public function getMonthEnglish(): string
    {
        return self::ENGLISH_MONTH[$this->month];
    }

    /**
     * @param int $month
     *
     * @return ServerDate
     * @throws DateException
     */
    private function setMonth(int $month): self
    {
        if (1 > $month || 12 < $month) {
            throw new DateException("time month must be between 1 and 12", DateException::INVALID_VALUE);
        }
        $this->month = $month;

        return $this;
    }

    /**
     * Also up year if upping from 12th month to 1st.
     *
     * @return ServerDate
     * @throws \Exception
     */
    private function upMonth(): self
    {
        $this->month++;
        if (12 < $this->getMonth()) {
            $this->setMonth(1);
            $this->upYear();
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isNewYear(): bool
    {
        return ($this->isNewMonth() && 1 === $this->month);
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     *
     * @return ServerDate
     */
    private function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return ServerDate
     */
    private function upYear(): self
    {
        $this->year++;

        return $this;
    }

    /**
     * Returns a readable string.
     * @return string
     */
    public function getEnglish(): string
    {
        return $this->getYear().", ".$this->getMonthEnglish().", week ".$this->getWeek();
    }

    /**
     * @param null|string $langage
     * @return array
     */
    public function toArray(?string $langage = null): array
    {
        $month = $this->getMonth();
        if ($langage === "en") {
            $month = $this->getMonthEnglish();
        }
        return [
            "year"  => $this->getYear(),
            "month" => $month,
            "week"  => $this->getWeek()
        ];
    }
}
