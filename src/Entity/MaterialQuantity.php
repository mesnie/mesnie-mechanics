<?php
/**
 * MaterialQuantity.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\MaterialException;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MaterialQuantity represents a quantity of a material.
 * It can be a part of the inventory of a Mesnie
 * or of the need or result of a WorkType
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MaterialQuantityRepository")
 */
class MaterialQuantity
{
    use IdTrait;

    /**
     * The type of material.
     *
     * @var Material $material
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Material")
     * @ORM\JoinColumn(nullable=false)
     */
    private $material;

    /**
     * The quantity of material.
     * Unit for object, litron for grain
     * 1 muid = 12 setiers = 24 mines = 48 minots = 144 boissels = 2304 litrons ~= 1560L
     * 1 setier = 2 mines = 4 minots = 12 boissels = 192 litrons ~= 130L
     * 1 mine = 2 minots  = 6 boissels = 96 litrons ~= 65L
     * 1 minot = 3 boissels = 48 litrons ~= 33L
     * 1 boissel = 16 litrons ~= 11L
     * 1 litron ~= 0.67L
     *
     * @var int $quantity
     *
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * The optional Mesnie who owns this quantity of material
     * if this MaterialQuantity is part of an inventory.
     *
     * @var Mesnie $owner
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Mesnie", inversedBy="inventory")
     */
    private $owner;

    /**
     * The optional WorkType who refers this quantity of material
     * if this MaterialQuantity is part of the materialNeeds of the WorkType.
     *
     * @var WorkType $neededByWork
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkType", inversedBy="materialNeeds")
     */
    private $neededByWork;

    /**
     * The optional WorkType who refers this quantity of material
     * if this MaterialQuantity is part of the materialResults of the WorkType.
     *
     * @var WorkType $resultFromWork
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\WorkType", inversedBy="materialResults")
     */
    private $resultFromWork;

    /**
     * MaterialQuantity constructor.
     * Needs the Material referenced.
     * Optional quantity, 1 by default.
     *
     * @param Material  $material
     * @param int       $quantity
     * @throws MaterialException 301
     */
    public function __construct(Material $material, int $quantity = 1)
    {
        $this->setMaterial($material);
        $this->setQuantity($quantity);
    }

    /**
     * @return Material
     */
    public function getMaterial(): Material
    {
        return $this->material;
    }

    /**
     * @param Material $material
     * @return MaterialQuantity
     */
    private function setMaterial(Material $material): self
    {
        $this->material = $material;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Checks that the given quantity is at least 1.
     *
     * @param int $quantity
     * @return MaterialQuantity
     * @throws MaterialException 301
     */
    private function setQuantity(int $quantity): self
    {
        if (1 > $quantity) {
            throw new MaterialException(
                "The quantity $quantity is negative or null",
                MaterialException::ERROR_QUANTITY_NEGATIVE,
                $this
            );
        }

        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Add a quantity to the MaterialQuantity.
     *
     * @param int $add
     * @return int
     * @throws MaterialException 301
     */
    public function add(int $add): int
    {
        if (1 > $add) {
            throw new MaterialException(
                "The quantity $add is negative or null",
                MaterialException::ERROR_QUANTITY_NEGATIVE,
                $this
            );
        }

        $this->setQuantity($this->getQuantity() + $add);

        return $this->getQuantity();
    }

    /**
     * Removes some quantity from the MaterialQuantity.
     *
     * @param int $remove
     * @return int
     * @throws MaterialException 301
     */
    public function remove(int $remove): int
    {
        if (1 > $remove) {
            throw new MaterialException(
                "The quantity $remove is negative or null",
                MaterialException::ERROR_QUANTITY_NEGATIVE,
                $this
            );
        }

        $this->setQuantity($this->getQuantity() - $remove);

        return $this->getQuantity();
    }

    /**
     * @return Mesnie|null
     */
    public function getOwner(): ?Mesnie
    {
        return $this->owner;
    }

    /**
     * @param Mesnie $owner
     * @return MaterialQuantity
     */
    public function setOwner(Mesnie $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return WorkType|null
     */
    public function getNeededByWork(): ?WorkType
    {
        return $this->neededByWork;
    }

    /**
     * @param WorkType $neededByWork
     * @return MaterialQuantity
     */
    public function setNeededByWork(WorkType $neededByWork): self
    {
        $this->neededByWork = $neededByWork;

        return $this;
    }

    /**
     * @return WorkType|null
     */
    public function getResultFromWork(): ?WorkType
    {
        return $this->resultFromWork;
    }

    /**
     * @param WorkType $resultFromWork
     * @return MaterialQuantity
     */
    public function setResultFromWork(WorkType $resultFromWork): self
    {
        $this->resultFromWork = $resultFromWork;

        return $this;
    }

    /**
     * Returns an array with all informations relative to the materialQuantity
     *
     * @param int $level
     * @return array
     */
    public function toArray($level = 1) : array
    {
        $data = [
            "quantity"  => $this->getQuantity(),
            "material"  => $this->getMaterial()->toArray()
        ];
        if ($level === 0) {
            return $data;
        }

        if ($this->getOwner()) {
            $data["mesnie"] = $this->getOwner()->toArray(0);
        }
        if ($this->getNeededByWork()) {
            $data["neededByWork"] = $this->getNeededByWork()->toArray(0);
        }
        if ($this->getResultFromWork()) {
            $data["resultFromWork"] = $this->getResultFromWork()->toArray(0);
        }
        return $data;
    }
}
