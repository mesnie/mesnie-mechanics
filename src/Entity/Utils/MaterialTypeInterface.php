<?php
/**
 * MaterialTypeInterface.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Utils;

/**
 * Interface MaterialType
 * @package App\Entity\Utils
 */
interface MaterialTypeInterface
{
    /*
     * The full list of the types of materials.
     */
    const TYPE_BREAD    = "bread";
    const TYPE_FLOUR    = "flour";
    const TYPE_GRAIN    = "grain";
    
    /*
     * an array containing the list of types.
     */
    const ALLOWED_TYPES = [
        self::TYPE_BREAD    => self::TYPE_BREAD,
        self::TYPE_FLOUR    => self::TYPE_FLOUR,
        self::TYPE_GRAIN    => self::TYPE_GRAIN,
    ];
}
