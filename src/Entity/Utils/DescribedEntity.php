<?php
/**
 * DescribedEntity.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Utils;

/** Usages */
use App\Entity\Traits\DescriptionTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DescribedEntity
 *
 * @package App\Entity\Utils
 * @ORM\MappedSuperclass()
 */
class DescribedEntity extends NamedEntity
{
    use DescriptionTrait;

    /**
     * DescribedEntity constructor.
     * @param string $name
     * @param string $description
     * @throws \Exception
     */
    public function __construct(string $name, string $description = "")
    {
        parent::__construct($name);
        $this->setDescription($description);
    }
}
