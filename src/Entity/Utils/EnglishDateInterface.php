<?php
/**
 * EnglishDateInterface.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Utils;

/**
 * Interface EnglishMonth
 * Used by Serve Date to provide the naming of its months, e.g. :
 * self::english_month[5] = 'may'
 *
 * @package App\Entity\Utils
 */
interface EnglishDateInterface
{
    /*
     * The english names of the months.
     * The array starts at 1 to suit the common indexing of months.
     */
    const ENGLISH_MONTH = [
        1 => "january",
        2 => "february",
        3 => "march",
        4 => "april",
        5 => "may",
        6 => "june",
        7 => "july",
        8 => "august",
        9 => "september",
        10=> "october",
        11=> "november",
        12=> "december"
    ];
}
