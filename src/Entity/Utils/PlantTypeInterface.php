<?php
/**
 * PlantTypeInterface.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Utils;

/**
 * Interface PlantTypeInterface
 * @package App\Entity\Utils
 */
interface PlantTypeInterface
{
    /*
     * The list of the types of plants.
     */
    const TYPE_CEREAL = 'cereal';
    
    /*
     * The same list in an array.
     */
    const ALLOWED_TYPES = [
        self::TYPE_CEREAL => self::TYPE_CEREAL
    ];
}
