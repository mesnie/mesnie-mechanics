<?php
/**
 * NamedEntity.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity\Utils;

/** Usages */
use App\Entity\Traits\NameAndIdentifierTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class NamedEntity
 *
 * @package App\Entity\Utils
 * @ORM\MappedSuperclass()
 */
class NamedEntity
{
    use NameAndIdentifierTrait;

    /**
     * @param string $name
     * @throws \Exception
     */
    public function __construct(string $name)
    {
        $this->setNameAndIdentifier($name);
    }
}
