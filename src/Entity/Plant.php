<?php
/**
 * Plant.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\DescribedEntity;
use App\Entity\Utils\PlantTypeInterface;
use App\Exception\PlantException;
use Doctrine\ORM\Mapping as ORM;

/**
 * A type of Plant that can be linked on a Field.
 *
 * @ORM\Entity(repositoryClass="App\Repository\PlantRepository")
 */
class Plant extends DescribedEntity implements PlantTypeInterface
{
    use IdTrait;

    /**
     * The type of the Plant, e.g. cereal or tree.
     *
     * @var string $type
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * The number of months needed by this plant to grow up.
     *
     * @var int $growthtime
     *
     * @ORM\Column(type="integer")
     */
    private $growthTime;

    /**
     * Plant constructor.
     * Needs everything ($name, $humanname, $type, $growthtime) except $description, optional.
     *
     * @param string $name
     * @param string $type
     * @param int $growthTime
     * @param string $description
     * @throws PlantException
     * @throws \Exception
     */
    public function __construct(
        string  $name,
        string  $type,
        int     $growthTime,
        string  $description = ""
    ) {
        parent::__construct($name, $description);
        $this
            ->setType($type)
            ->setGrowthTime($growthTime);
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Plant
     * @throws PlantException
     */
    private function setType(string $type): self
    {
        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new PlantException(
                "The type $type is not allowed for a Plant",
                PlantException::TYPE_NOT_ALLOWED
            );
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getGrowthTime(): ?int
    {
        return $this->growthTime;
    }

    /**
     * @param int $growthTime
     * @return Plant
     */
    private function setGrowthTime(int $growthTime): self
    {
        $this->growthTime = $growthTime;

        return $this;
    }
}
