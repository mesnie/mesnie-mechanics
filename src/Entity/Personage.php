<?php
/**
 * Personage.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\ChildException;
use App\Exception\MarriageException;
use App\Exception\PersonageException;
use App\Service\PersonageService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Mesnie personage, with its family relations.
 *
 * @ORM\Entity(repositoryClass="App\Repository\PersonageRepository")
 */
class Personage
{
    /** defines the ages of fertility and nubility for each sex */
    const FERTILITY_FEMALE  = 12;
    const FERTILITY_MALE    = 14;
    const NUBILITY_FEMALE   = 14;
    const NUBILITY_MALE     = 16;
    const MENOPAUSE         = 45;

    use IdTrait;

    /**
     * The baptism name of the personage.
     * Null if the baby has not yet be baptised.
     *
     * @var string $firstname
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * True if the personage is male, false if female.
     *
     * @var bool $isMale
     *
     * @ORM\Column(type="boolean")
     */
    private $isMale;

    /**
     * The birth year of the personage.
     *
     * @var int $birthYear
     *
     * @ORM\Column(type="integer")
     */
    private $birthYear;

    /**
     * The death year of the personage. Null if alive.
     *
     * @var int $deathYear
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deathYear;

    /**
     * The legal father of the personage.
     * Null if bastard or lost in memory.
     *
     * @var Personage $father
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Personage")
     */
    private $father;

    /**
     * The mother of the personage.
     * Null if lost in memory.
     *
     * @var Personage $mother
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Personage")
     */
    private $mother;

    /**
     * The spouse of the personage.
     * Null if celibate or list in memory.
     *
     * @var Personage $spouse
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Personage", inversedBy="spouse")
     */
    private $spouse;

    /**
     * The born legal children of the personage.
     *
     * @var ArrayCollection|Personage[] $children
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Personage")
     * @ORM\JoinTable(name="children",
     *      joinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")}
     *      )
     *
     * @todo remove the database entry, as it is already linked with father and mother ?
     */
    private $children;

    /**
     * If the personage is a female, its pregnancy.
     * Null if not pregnant.
     *
     * @var Pregnancy $pregnancy
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Pregnancy", mappedBy="mother", cascade={"persist", "remove"})
     */
    private $pregnancy;

    /**
     * The Mesnie where the Personage lives.
     * Null if dead or without Mesnie.
     *
     * @var Mesnie $mesnie
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Mesnie", inversedBy="members")
     */
    private $mesnie;

    /**
     * The Field where the personage is working.
     * Null if not working or working on something which is not a Field
     *
     * @var Field $workField
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Field", mappedBy="worker")
     */
    private $workField;

    /**
     * Personage constructor.
     * Needs the sex and the birthYear
     *
     * @param bool  $isMale
     * @param int   $birthYear
     */
    public function __construct(bool $isMale, int $birthYear)
    {
        $this->setIsMale($isMale);
        $this->setBirthYear($birthYear);
        $this->children = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Checks that the Personage doesn't already have a name,
     * and that the name to set contains at least three letters.
     *
     * @param string|null $firstName
     * @return Personage
     * @throws PersonageException 101, 102
     */
    public function setFirstName(?string $firstName): self
    {
        if ($this->firstName) {
            throw new PersonageException(
                "The personage is already named ".$this->firstName.", can't be renamed $firstName",
                PersonageException::ERROR_NAME_ALREADY,
                $this
            );
        }
        if (3 > strlen($firstName)) {
            throw new PersonageException(
                "The firstName $firstName is too short",
                PersonageException::ERROR_NAME_SHORT,
                $this
            );
        }

        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMale(): bool
    {
        return $this->isMale;
    }

    /**
     * @param bool $isMale
     * @return Personage
     */
    private function setIsMale(bool $isMale): self
    {
        $this->isMale = $isMale;

        return $this;
    }

    /**
     * @return int
     */
    public function getBirthYear(): int
    {
        return $this->birthYear;
    }

    /**
     * @param int $birthYear
     * @return Personage
     */
    private function setBirthYear(int $birthYear): self
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeathYear(): ?int
    {
        return $this->deathYear;
    }

    /**
     * Checks that the deathYear is not already set
     * and that the given one is not lower than birthYear
     *
     * @param int|null $deathYear
     * @return Personage
     * @throws PersonageException 111, 112
     */
    public function setDeathYear(?int $deathYear): self
    {
        if ($this->getDeathYear()) {
            throw new PersonageException(
                "The personage has already died in ".$this->getDeathYear(),
                PersonageException::ERROR_DEATH_ALREADY,
                $this
            );
        }
        if ($deathYear && ($this->birthYear > $deathYear)) {
            throw new PersonageException(
                "The personae is born in ".$this->birthYear." and so cannot die in $deathYear",
                PersonageException::ERROR_DEATH_BIRTH,
                $this
            );
        }
        $this->deathYear = $deathYear;

        return $this;
    }

    /**
     * @return Personage|null
     */
    public function getFather(): ?Personage
    {
        return $this->father;
    }

    /**
     * Checks that the father is not already set
     * and that the given father is a male
     * and is at least 10 years older than the Personage.
     *
     * @param Personage|null $father
     * @return Personage
     * @throws ChildException
     */
    public function setFather(?Personage $father): self
    {
        if ($this->getFather()) {
            throw new ChildException(
                "The personage ".$this->getId()." already has a father : ".$father->getId(),
                ChildException::ERROR_CHILD_ALREADY,
                $this,
                $father,
                null,
                null
            );
        }
        if ($father && !$father->isMale()) {
            throw new ChildException(
                "The personage ".$this->getId()." is not a male and so can't be a father",
                ChildException::ERROR_PARENT_SEX,
                $this,
                $father,
                null,
                null
            );
        }
        if ($father && ($father->getBirthYear() + 10 > $this->getBirthYear())) {
            throw new ChildException(
                "The personage ".$this->getId()."is born in ".$this->getBirthYear()
                ." and so cannot have a father ".$this->getId()." born in ".$father->getBirthYear(),
                ChildException::ERROR_CHILD_AGE,
                $this,
                $father,
                null,
                null
            );
        }
        $this->father = $father;

        return $this;
    }

    /**
     * @return Personage|null
     */
    public function getMother(): ?Personage
    {
        return $this->mother;
    }

    /**
     * Checks that the mother is not already set
     * and that the given mother is a female
     * and is at least 10 years older than the Personage.
     *
     * @param Personage|null $mother
     * @return Personage
     * @throws ChildException
     */
    public function setMother(?Personage $mother): self
    {
        if ($this->getMother()) {
            throw new ChildException(
                "The personage ".$this->getId()." already has a mother : ".$mother->getId(),
                ChildException::ERROR_CHILD_ALREADY,
                $this,
                null,
                $mother,
                null
            );
        }
        if ($mother && $mother->isMale()) {
            throw new ChildException(
                "The personage ".$this->getId()." is not a female and so can't be a mother",
                ChildException::ERROR_PARENT_SEX,
                $this,
                null,
                $mother,
                null
            );
        }
        if ($mother && ($mother->getBirthYear() + 10 > $this->getBirthYear())) {
            throw new ChildException(
                "The personage ".$this->getId()."is born in ".$this->getBirthYear()
                ." and so cannot have a mother ".$this->getId()." born in ".$mother->getBirthYear(),
                ChildException::ERROR_CHILD_AGE,
                $this,
                null,
                $mother,
                null
            );
        }
        $this->mother = $mother;

        return $this;
    }

    /**
     * @return Personage|null
     */
    public function getSpouse(): ?Personage
    {
        return $this->spouse;
    }

    /**
     * Checks that the spouse is not already set
     * and is of the other sex.
     *
     * @param Personage $spouse
     * @return Personage
     * @throws MarriageException 501, 502
     */
    public function setSpouse(Personage $spouse): self
    {
        if ($this->getSpouse()) {
            $male = $spouse;
            $female = $this;
            if ($this->isMale()) {
                $male = $this;
                $female = $spouse;
            }
            throw new MarriageException(
                "The personage ".$this->getId()." is already married to ".$this->getSpouse()->getId(),
                MarriageException::ERROR_MARRY_ALREADY,
                $male,
                $female
            );
        }
        if (($spouse->isMale() && $this->isMale()) || (!$spouse->isMale()) && !$this->isMale()) {
            throw new MarriageException(
                "The personage ".$this->getId()." is of the same sex as ".$spouse->getId(),
                MarriageException::ERROR_MARRY_SEX,
                $this,
                $spouse
            );
        }

        $this->spouse = $spouse;

        return $this;
    }

    /**
     * Checks that the Personage has a spouse to remove.
     *
     * @return Personage
     * @throws MarriageException
     */
    public function removeSpouse(): self
    {
        if (!$this->getSpouse()) {
            throw new MarriageException(
                "The personage ".$this->getId()." has no spouse to remove",
                MarriageException::ERROR_REMOVE_NOONE,
                $this
            );
        }
        $this->spouse = null;

        return $this;
    }

    /**
     * Returns an ArrayCollection of children,
     * sorted by birthYear.
     *
     * @return Collection|Personage[]
     */
    public function getChildren(): Collection
    {
        return PersonageService::sortByBirthYear($this->children);
    }

    /**
     * @param Personage $child
     * @return Personage
     * @throws ChildException
     */
    public function addChild(Personage $child): self
    {
        if ($this->getBirthYear() + 10 > $child->getBirthYear()) {
            throw new ChildException(
                "The personage is born in ".$this->getBirthYear()
                ." and so cannot have a child born in ".$child->getBirthYear(),
                ChildException::ERROR_CHILD_AGE,
                $this
            );
        }
        $this->children->add($child);

        return $this;
    }

    /**
     * @return Pregnancy|null
     */
    public function getPregnancy(): ?Pregnancy
    {
        return $this->pregnancy;
    }

    /**
     * Checks that the Personage is not already pregnant
     * that the Personage is a female
     * And that the Personage is the mother of the Pregnancy.
     *
     * @param Pregnancy $pregnancy
     * @return Personage
     * @throws ChildException
     */
    public function setPregnancy(Pregnancy $pregnancy): self
    {
        if ($this->getPregnancy()) {
            throw new ChildException(
                "The personage ".$this->getId()." is already pregnant (pregnancyId:".$this->getPregnancy()->getId().")",
                ChildException::ERROR_PREGNANCY_ALREADY,
                null,
                null,
                $this,
                $pregnancy
            );
        }
        if ($this->isMale()) {
            throw new ChildException(
                "The personage ".$this->getId()." is a male and so cannot be pregnant",
                ChildException::ERROR_PREGNANCY_MALE,
                null,
                $this,
                null,
                $pregnancy
            );
        }
        if ($this !== $pregnancy->getMother()) {
            throw new ChildException(
                "The pregnancy ".$pregnancy->getId()." has another mother : ".$pregnancy->getMother()->getId(),
                ChildException::ERROR_PREGNANCY_OTHER,
                null,
                null,
                $this,
                $pregnancy
            );
        }

        $this->pregnancy = $pregnancy;

        return $this;
    }

    /**
     * @return Mesnie|null
     */
    public function getMesnie(): ?Mesnie
    {
        return $this->mesnie;
    }

    /**
     * @param Mesnie|null $mesnie
     * @return Personage
     */
    public function setMesnie(?Mesnie $mesnie): self
    {
        $this->mesnie = $mesnie;

        return $this;
    }

    /**
     * Checks if the personage has a mesnie and is the head of it.
     *
     * @return bool
     */
    public function isMesnieHead(): bool
    {
        return ($this->getMesnie() && $this->getMesnie()->getHead() === $this);
    }

    /**
     * @return Field|null
     */
    public function getWorkField(): ?Field
    {
        return $this->workField;
    }

    /**
     * @param Field|null $workField
     * @return Personage
     */
    public function setWorkField(?Field $workField = null): self
    {
        $this->workField = $workField;

        return $this;
    }

    /**
     * Returns the age of the personage.
     * Death age if dead.
     * Error if the Personage is not born.
     *
     * @param $currentYear
     * @return int
     * @throws PersonageException 151
     */
    public function getAge($currentYear): int
    {
        $compareYear = $this->getDeathYear() ?? $currentYear;
        $currentAge = $compareYear - $this->getBirthYear();
        if (0 > $currentAge) {
            throw new PersonageException(
                "The personage is born in ".$this->getBirthYear()." but we are in ".$currentYear,
                PersonageException::ERROR_BIRTH_DATE,
                $this
            );
        }

        return $currentAge;
    }

    /**
     * Returns a degree of consanguinity :
     * * 0 if no consanguinity
     * * -1 if child or parent
     * * -2 if great-child or great-parent
     * * 1 if sibling
     * * 2 if cousin, uncle, aunt, nephew, niece
     *
     * @param Personage $other
     * @return int
     */
    public function consanguinity(Personage $other): int
    {

        //parent
        if ($this->getFather() === $other ||
            $this->getMother() === $other ||
            $other->getFather() === $this ||
            $other->getMother() === $this) {
            return -1;
        }

        //sibling
        if (($this->getFather() && $this->getFather() === $other->getFather()) ||
            ($this->getMother() && $this->getMother() === $other->getMother())) {
            return 1;
        }

        //greatparent
        if (($this->getFather() && -1 === $this->getFather()->consanguinity($other)) ||
            ($this->getMother() && -1 === $this->getMother()->consanguinity($other)) ||
            ($other->getFather() && -1 === $other->getFather()->consanguinity($this)) ||
            ($other->getMother() && -1 === $other->getMother()->consanguinity($this))) {
            return -2;
        }


        //uncle
        if (($this->getFather() && 1 === $this->getFather()->consanguinity($other)) ||
            ($this->getMother() && 1 === $this->getMother()->consanguinity($other)) ||
            ($other->getFather() && 1 === $other->getFather()->consanguinity($this)) ||
            ($other->getMother() && 1 === $other->getMother()->consanguinity($this))) {
            return 2;
        }

        //cousin
        if (($this->getFather() && $other->getFather() && 1
                === $this->getFather()->consanguinity($other->getFather())) ||
            ($this->getFather() && $other->getMother() && 1
                === $this->getFather()->consanguinity($other->getMother())) ||
            ($this->getMother() && $other->getFather() && 1
                === $this->getMother()->consanguinity($other->getFather())) ||
            ($this->getMother() && $other->getMother() && 1
                === $this->getMother()->consanguinity($other->getMother()))) {
            return 2;
        }

        return 0;
    }

    /**
     * @return array
     */
    public function upperTree(): array
    {
        $fatherTree = $this->getFather() ? $this->getFather()->upperTree() : null;
        $motherTree = $this->getMother() ? $this->getMother()->upperTree() : null;

        return [
            "self"      => $this->toArray(0),
            "father"    => $fatherTree,
            "mother"    => $motherTree
        ];
    }

    /**
     * @return array
     */
    public function lowerTree(): array
    {
        $childrenTree = [];
        /** @var Personage $child */
        foreach ($this->getChildren() as $child) {
            $childrenTree[] = $child->lowerTree();
        }

        return [
            "self"      => $this->toArray(0),
            "children"  => $childrenTree
        ];
    }

    /**
     * @return array
     */
    public function getFamilyTree(): array
    {
        $upperTree = $this->upperTree();
        $lowerTree = $this->lowerTree();

        return array_merge($upperTree, $lowerTree);
    }

    /**
     * Returns an ArrayCollection of siblings,
     * i.e. Personages who are children of the father OR the mother,
     * sorted by birthYear.
     *
     * @return ArrayCollection|Personage[]
     */
    public function getSiblings(): ArrayCollection
    {
        $siblings = new ArrayCollection();
        if ($this->getFather()) {
            foreach ($this->getFather()->getChildren() as $child) {
                if (!$siblings->contains($child)) {
                    $siblings->add($child);
                }
            }
        }
        if ($this->getMother()) {
            foreach ($this->getMother()->getChildren() as $child) {
                if (!$siblings->contains($child)) {
                    $siblings->add($child);
                }
            }
        }
        $siblings->removeElement($this);

        return PersonageService::sortByBirthYear($siblings);
    }

    /**
     * @return Personage|null
     */
    public function getHeir(): ?Personage
    {
        return PersonageService::findHeir($this);
    }

    /**
     * @return Family
     */
    public function getFamily(): ?Family
    {
        if ($this->getMesnie()) {
            return $this->getMesnie()->getFamily();
        }

        return null;
    }

    /**
     * Compares the Personage age
     * with the fertility constants relative to its sex.
     *
     * @param int $currentYear
     * @return bool
     * @throws PersonageException 151
     */
    public function isFertile(int $currentYear): bool
    {
        $age = $this->getAge($currentYear);
        $ageFertility = $this->isMale() ? self::FERTILITY_MALE : self::FERTILITY_FEMALE;
        $menopause = !$this->isMale() && $age > self::MENOPAUSE;

        return ($ageFertility <= $age && !$menopause);
    }

    /**
     * Compares the Personage age
     * with the nubility constant relative to its sex.
     *
     * @param int $currentYear
     * @return bool
     * @throws PersonageException 151
     */
    public function isNubile(int $currentYear): bool
    {
        $age = $this->getAge($currentYear);
        $ageNubility = $this->isMale() ? self::NUBILITY_MALE : self::NUBILITY_FEMALE;

        return $ageNubility <= $age;
    }

    /**
     * Returns an array with every information relevant to the personage
     *
     * @param int $level
     * @return array
     */
    public function toArray(int $level = 0): array
    {
        $perso = [
            "id"        => $this->getId(),
            "firstName" => $this->getFirstName(),
            "birthYear" => $this->getBirthYear(),
            "sex"       => $this->isMale() ? "male" : "female"
        ];
        if (0 === $level) {
            return $perso;
        }

        $children = [];
        foreach ($this->getChildren() as $child) {
            $childArray = [
                "id"        => $child->getId(),
                "firstName" => $child->getFirstName(),
                "birthYear" => $child->getBirthYear(),
                "sex"       => $child->isMale() ? "male" : "female"
            ];
            if ($child->getDeathYear()) {
                $childArray["deathYear"] = $child->getDeathYear();
            }
            $children[] = $childArray;
        }

        $perso["children"]  = $children;

        if ($this->getFather()) {
            $perso["father"] = [
                "id"        => $this->getFather()->getId(),
                "firstName" => $this->getFather()->getFirstName(),
            ];
            if ($this->getFather()->getDeathYear()) {
                $perso["father"]["deathYear"] = $this->getFather()->getDeathYear();
            }
        }
        if ($this->getMother()) {
            $perso["mother"] = [
                "id"        => $this->getMother()->getId(),
                "firstName" => $this->getMother()->getFirstName(),
            ];
            if ($this->getMother()->getDeathYear()) {
                $perso["mother"]["deathYear"] = $this->getMother()->getDeathYear();
            }
        }

        if ($this->getSpouse()) {
            $perso["spouse"] = [
                "id"        => $this->getSpouse()->getId(),
                "firstName" => $this->getSpouse()->getFirstName(),
                "birthYear" => $this->getSpouse()->getBirthYear()
            ];
        }
        if ($this->getPregnancy()) {
            $perso["pregnancy"] = [
                "id"    => $this->getPregnancy()->getId(),
                "month" => $this->getPregnancy()->getMonth()
            ];
        }

        if ($this->getHeir()) {
            $perso["heir"] = [
                "id"        => $this->getHeir()->getId(),
                "firstName" => $this->getHeir()->getFirstName()
            ];
        }

        if ($this->getDeathYear()) {
            $perso["deathYear"] = $this->getDeathYear();
        }

        if ($this->getMesnie()) {
            $perso["mesnie"] = $this->getMesnie()->getId();
            $perso["family"] = $this->getMesnie()->getFamily();
        }

        return $perso;
    }
}
