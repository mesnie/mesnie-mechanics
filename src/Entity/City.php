<?php
/**
 * City.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\DescribedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class City
 *
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City extends DescribedEntity
{
    use IdTrait;

    /**
     * @var ArrayCollection|MaterialValue[] $materialValues
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MaterialValue", mappedBy="City", orphanRemoval=true)
     */
    private $materialValues;

    /**
     * City constructor.
     *
     * @param string $name
     * @throws \Exception
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->materialValues = new ArrayCollection();
    }

    /**
     * @return Collection|MaterialValue[]
     */
    public function getMaterialValues(): Collection
    {
        return $this->materialValues;
    }

    /**
     * @param MaterialValue $materialValue
     * @return City
     */
    public function addMaterialValue(MaterialValue $materialValue): self
    {
        if (!$this->materialValues->contains($materialValue)) {
            $this->materialValues[] = $materialValue;
        }

        return $this;
    }

    /**
     * @param MaterialValue $materialValue
     * @return City
     */
    public function removeMaterialValue(MaterialValue $materialValue): self
    {
        if ($this->materialValues->contains($materialValue)) {
            $this->materialValues->removeElement($materialValue);
        }

        return $this;
    }
}
