<?php
/**
 * MaterialValue.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Exception\MoneyException;
use Doctrine\ORM\Mapping as ORM;

/**
 * MaterialValue represents the value of a Material in a City.
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MaterialValueRepository")
 */
class MaterialValue
{
    use IdTrait;

    /**
     * The City in which the Material has this value.
     *
     * @var City $city
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="materialValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * The Material which has the value.
     *
     * @var Material $material
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Material", inversedBy="materialValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $material;

    /**
     * The value in half-pites.
     * 1 pound = 20 sols = 240 denars = 480 obols = 960 pites = 1920 half-pites
     * 1 sol = 12 denars = 24 obols = 48 pites = 96 half-pites
     * 1 denar = 2 obols = 4 pites = 8 half-pites
     * 1 obol = 2 pites = 4 half-pites
     * 1 pite = 2 half-pites
     *
     * @var float $value in half-pites
     *
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * MaterialValue constructor.
     *
     * @param City      $city
     * @param Material  $material
     * @param float     $value
     * @throws MoneyException
     */
    public function __construct(City $city, Material $material, float $value)
    {
        $this
            ->setCity($city)
            ->setMaterial($material)
            ->setValue($value)
        ;
    }

    /**
     * @return City|null
     */
    public function getCity(): ?City
    {
        return $this->city;
    }

    /**
     * @param City $City
     * @return MaterialValue
     */
    private function setCity(City $City): MaterialValue
    {
        $this->city = $City;

        return $this;
    }

    /**
     * @return Material|null
     */
    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    /**
     * @param Material $material
     * @return MaterialValue
     */
    private function setMaterial(Material $material): MaterialValue
    {
        $this->material = $material;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return MaterialValue
     * @throws MoneyException
     */
    private function setValue(float $value): MaterialValue
    {
        if (0 > $value) {
            throw new MoneyException($value . " under 0", MoneyException::VALUE_NEGATIVE);
        }
        $this->value = $value;

        return $this;
    }
}
