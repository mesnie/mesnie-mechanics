<?php
/**
 * Family.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Entity;

/** Usages */
use App\Entity\Traits\IdTrait;
use App\Entity\Utils\NamedEntity;
use App\Exception\FamilyException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Family is mainly a name, to which mesnies are linked.
 * Each Family has a patriarch, member of one the mesnies.
 *
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\FamilyRepository")
 */
class Family extends NamedEntity
{
    use IdTrait;

    /**
     * The mesnies linked to the family. They are supposed to all have blood links.
     *
     * @var ArrayCollection|Mesnie[] $mesnies
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Mesnie", mappedBy="family")
     */
    private $mesnies;

    /**
     * The patriarch of the family.
     * He is the heir of the predecessor,
     * or, if the predecessor has no heir, the oldest man of the family.
     *
     * @var Personage $patriarch
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Personage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $patriarch;

    /**
     * Family constructor.
     * Sets the name of the Family.
     *
     * @param string $name
     * @throws \Exception
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->mesnies = new ArrayCollection();
    }

    /**
     * @return Collection|Mesnie[]
     */
    public function getMesnies(): Collection
    {
        return $this->mesnies;
    }

    /**
     * Checks the mesnie refers to this family and isn't already a member.
     *
     * @param Mesnie $mesnie
     * @return Family
     * @throws FamilyException
     */
    public function addMesnie(Mesnie $mesnie): self
    {
        if ($mesnie->getFamily() !== $this) {
            throw new FamilyException(
                "The mesnie ".$mesnie->getId()." refers to another or no family",
                FamilyException::OTHER_FAMILY,
                $this
            );
        }
        if (!$this->mesnies->contains($mesnie)) {
            $this->mesnies[] = $mesnie;
            $mesnie->setFamily($this);
        }

        return $this;
    }

    /**
     * Removes the mesnie if it is a member.
     * Also set the mesnie family to null if necessary.
     *
     * @param Mesnie $mesnie
     * @return Family
     */
    public function removeMesnie(Mesnie $mesnie): self
    {
        if ($this->mesnies->contains($mesnie)) {
            $this->mesnies->removeElement($mesnie);
            // set the owning side to null (unless already changed)
            if ($mesnie->getFamily() === $this) {
                $mesnie->setFamily(null);
            }
        }

        return $this;
    }

    /**
     * @return Personage|null
     */
    public function getPatriarch(): ?Personage
    {
        return $this->patriarch;
    }

    /**
     * Checks that the patriarch is member of the family through its mesnie.
     *
     * @param Personage $patriarch
     * @return Family
     * @throws FamilyException
     */
    public function setPatriarch(Personage $patriarch): self
    {
        if ($patriarch->getFamily() !== $this) {
            throw new FamilyException(
                "The personage ".$patriarch->getId()." refers to another or no family",
                FamilyException::OTHER_FAMILY,
                $this
            );
        }
        $this->patriarch = $patriarch;

        return $this;
    }

    /**
     * Search a heir for the patriarch :
     * first, look for the patriarch own heir.
     * if no heir found, then return the oldest man between all mesnies.
     * if still no heir return null.
     *
     * @return Personage|null
     */
    public function getHeir(): ?Personage
    {
        $heir = null;
        if ($this->getPatriarch()) {
            $heir = $this->getPatriarch()->getHeir();
        }
        if (!$heir) {
            foreach ($this->getMesnies() as $mesnie) {
                foreach ($mesnie->getMembers() as $member) {
                    if ($member->isMale()
                        && $this->getPatriarch() !== $member
                        && (!$heir || $heir->getBirthYear() > $member->getBirthYear())) {
                        $heir = $member;
                    }
                }
            }
        }

        return $heir;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $family = [
            "id"    => $this->getId(),
            "name"  => $this->getName()
        ];

        return $family;
    }
}
