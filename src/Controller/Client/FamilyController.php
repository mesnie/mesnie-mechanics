<?php

/** Namespace */
namespace App\Controller\Client;

/** Usages */
use App\Entity\Personage;
use App\Exception\PersonageException;
use App\Service\DateService;
use App\Service\MarriageService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FamilyController
 * @package App\Controller\Client
 */
class FamilyController extends AbstractController
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var DateService $dateService */
    private $dateService;

    /** @var MarriageService $marriageService */
    private $marriageService;

    /**
     * FamilyController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface  $entityManager,
        LoggerInterface         $logger
    ) {
        $this->entityManager = $entityManager;
        $this->dateService = new DateService($entityManager, $logger);
        $this->marriageService = new MarriageService($entityManager);
    }

    /**
     * @Route("/client/family/marry/{id}", name="client_family_marry_commoner")
     * @param string $id
     * @return Response
     */
    public function marryCommonerAction(string $id): Response
    {
        /** @var Personage|null $perso */
        $perso = $this->entityManager->getRepository(Personage::class)->find((int) $id);
        if (!$perso) {
            return new Response("personage does'nt exist", 404);
        }

        try {
            $date = $this->dateService->currentDate();
            $this->marriageService->marryCommoner($perso, $date->getYear());
        } catch (PersonageException $e) {
            return new Response("Personage error. Please send us this error code : 684", 500);
        } catch (Exception $e) {
            return new Response("Date error. Please send us this error code : 1868416", 500);
        }

        return $this->redirectToRoute("client_default");
    }
}
