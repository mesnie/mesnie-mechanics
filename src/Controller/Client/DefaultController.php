<?php

/** Namespace */
namespace App\Controller\Client;

/** Usages */
use App\Entity\Field;
use App\Entity\Material;
use App\Entity\MaterialValue;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\WorkType;
use App\Exception\DateException;
use App\Exception\MaterialException;
use App\Exception\MoneyException;
use App\Repository\FieldRepository;
use App\Repository\MaterialRepository;
use App\Repository\MaterialValueRepository;
use App\Repository\MesnieRepository;
use App\Repository\PersonageRepository;
use App\Repository\WorkTypeRepository;
use App\Service\DateService;
use App\Service\MarketService;
use App\Service\WorkService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package App\Controller\Client
 */
class DefaultController extends AbstractController
{
    /** @var DateService $dateService */
    private $dateService;

    /** @var WorkService $workService */
    private $workService;

    /** @var MarketService $marketService */
    private $marketService;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     * DefaultController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param DateService            $dateService
     * @param WorkService            $workService
     * @param MarketService          $marketService
     */
    public function __construct(
        EntityManagerInterface  $entityManager,
        DateService             $dateService,
        WorkService             $workService,
        MarketService           $marketService
    ) {
        $this->entityManager = $entityManager;
        $this->dateService = $dateService;
        $this->workService = $workService;
        $this->marketService = $marketService;
    }

    /**
     * @Route("/client/default", name="client_default")
     * @IsGranted("ROLE_USER")
     */
    public function index()
    {
        $currentMesnie = $this->getUserMesnie();
        if (!$currentMesnie) {
            return new Response("you have no mesnie");
        }

        try {
            $currentDate = $this->dateService->currentDate();
        } catch (DateException $exception) {
            return new Response("date exception ".$exception->getCode(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $materialValues = $this->getMaterialValueRepository()->findAll();

        return $this->render('client/default/index.html.twig', [
            'date'              => $currentDate,
            'mesnie'            => $currentMesnie,
            'materialValues'   => $materialValues
        ]);
    }

    /**
     * @Route("/client/work/set", name="client_work_set")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function setToWork(Request $request)
    {
        $personageId = $request->get("personage");
        $workTypeId = $request->get("workType");
        $fieldId = $request->get("field");

        if (!$personageId || !is_numeric($personageId)) {
            return new Response("missing personageId", Response::HTTP_BAD_REQUEST);
        }

        $personage = $this->getPersonageRepository()->find($personageId);
        if (!$personage) {
            return new Response("personage not found", Response::HTTP_NOT_FOUND);
        }

        /** @var WorkType $workType */
        $workType = null;
        if ($workTypeId) {
            $workType = $this->getWorkTypeRepository()->find($workTypeId);
            if (!$workType) {
                return new Response("workType not found", Response::HTTP_NOT_FOUND);
            }
        }

        /** @var Field $field */
        $field = null;
        if ($fieldId) {
            $field = $this->getFieldRepository()->find($fieldId);
            if (!$field) {
                return new Response("Field not found", Response::HTTP_NOT_FOUND);
            }
        }

        try {
            $this->workService->setToWork($personage, $workType, $field);
        } catch (\Exception $exception) {
            return new Response("internal error code ".$exception->getCode());
        }

        return $this->redirectToRoute("client_default");
    }

    /**
     * @Route("/client/market/buy", name="client_market_buy")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws MaterialException
     */
    public function buyMaterial(Request $request)
    {
        $materialId = $request->get("material");
        $quantity = $request->get("quantity");

        if (!$quantity || !is_numeric($quantity)) {
            return new Response("missing quantity", Response::HTTP_BAD_REQUEST);
        }

        if (!$materialId || !is_numeric($materialId)) {
            return new Response("missing materialID", Response::HTTP_BAD_REQUEST);
        }
        $material = $this->getMaterialRepository()->find($materialId);
        if (!$material) {
            return new Response("material not found", Response::HTTP_NOT_FOUND);
        }

        try {
            $this->marketService->buyMaterial($this->getUserMesnie(), $material, $quantity);
        } catch (MoneyException $exception) {
            return new Response($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return $this->redirectToRoute("client_default");
    }

    /**
     * @Route("/client/market/sell", name="client_market_sell")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws MoneyException
     */
    public function sellMaterial(Request $request)
    {
        $materialId = $request->get("material");
        $quantity = $request->get("quantity");

        if (!$quantity || !is_numeric($quantity)) {
            return new Response("missing quantity", Response::HTTP_BAD_REQUEST);
        }

        if (!$materialId || !is_numeric($materialId)) {
            return new Response("missing materialID", Response::HTTP_BAD_REQUEST);
        }
        $material = $this->getMaterialRepository()->find($materialId);
        if (!$material) {
            return new Response("material not found", Response::HTTP_NOT_FOUND);
        }

        try {
            $this->marketService->sellMaterial($this->getUserMesnie(), $material, $quantity);
        } catch (MaterialException $exception) {
            return new Response($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return $this->redirectToRoute("client_default");
    }

    /**
     * @return Mesnie|null
     */
    private function getUserMesnie(): ?Mesnie
    {
        return $this->getMesnieRepository()->getFromUser($this->getUser());
    }

    /**
     * @return MesnieRepository
     */
    private function getMesnieRepository(): MesnieRepository
    {
        return $this->entityManager->getRepository(Mesnie::class);
    }

    /**
     * @return PersonageRepository
     */
    private function getPersonageRepository(): PersonageRepository
    {
        return $this->entityManager->getRepository(Personage::class);
    }

    /**
     * @return WorkTypeRepository
     */
    private function getWorkTypeRepository(): WorkTypeRepository
    {
        return $this->entityManager->getRepository(WorkType::class);
    }

    /**
     * @return FieldRepository
     */
    private function getFieldRepository(): FieldRepository
    {
        return $this->entityManager->getRepository(Field::class);
    }

    /**
     * @return MaterialRepository
     */
    private function getMaterialRepository(): MaterialRepository
    {
        return $this->entityManager->getRepository(Material::class);
    }

    /**
     * @return MaterialValueRepository
     */
    private function getMaterialValueRepository(): MaterialValueRepository
    {
        return $this->entityManager->getRepository(MaterialValue::class);
    }
}
