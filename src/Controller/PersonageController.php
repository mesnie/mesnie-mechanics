<?php
/**
 * PersonageController.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Controller;

/** Usages */
use App\Entity\Personage;
use App\Exception\PersonageException;
use App\Repository\PersonageRepository;
use App\Service\DateService;
use App\Service\MarriageService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PersonageController
 * @package App\Controller
 */
class PersonageController extends AbstractController
{
    /** @var DateService $dateService */
    private $dateService;

    /** @var MarriageService */
    private $marriageService;

    /**
     * PersonageController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->dateService = new DateService($entityManager, $logger);
        $this->marriageService = new MarriageService($entityManager);
    }

    /**
     * @return PersonageRepository
     */
    private function getPersonageRepository(): PersonageRepository
    {
        /** @var PersonageRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Personage::class);

        return $repo;
    }

    /**
     * Index of all personages, dead or alive.
     *
     * @Route("/personage/", name="personage_index")
     */
    public function index(): Response
    {
        $allPersonages = $this->getPersonageRepository()->findAll();
        $allPersonagesArray = [];
        /** @var Personage $personage */
        foreach ($allPersonages as $personage) {
            $allPersonagesArray[] = $personage->toArray(1);
        }

        return new Response(json_encode($allPersonagesArray));
    }

    /**
     * Informations on a personage and its direct family
     *
     * @Route("/personage/{id}/", name="personage_one")
     * @param string $id
     * @return Response
     */
    public function getOne(string $id): Response
    {
        $id = intval($id);
        /** @var Personage $personage */
        $personage = $this->getPersonageRepository()->find($id);
        if ($personage) {
            return new Response(json_encode($personage->toArray(1)));
        }

        return new Response("personage not found", Response::HTTP_NOT_FOUND);
    }

    /**
     * Tree of ascendants of a personage.
     *
     * @Route("/personage/{id}/uppertree", name="personage_one_uppertree")
     * @param string $id
     * @return Response
     */
    public function getOneUpperTree(string $id): Response
    {
        $id = intval($id);
        /** @var Personage $personage */
        $personage = $this->getPersonageRepository()->find($id);
        if ($personage) {
            return new Response(json_encode($personage->upperTree()));
        }

        return new Response("personage not found", Response::HTTP_NOT_FOUND);
    }

    /**
     * Tree of descendants of a personage.
     *
     * @Route("/personage/{id}/lowertree", name="personage_one_lowertree")
     * @param string $id
     * @return Response
     */
    public function getOneLowerTree(string $id): Response
    {
        $id = intval($id);
        /** @var Personage $personage */
        $personage = $this->getPersonageRepository()->find($id);
        if ($personage) {
            return new Response(json_encode($personage->lowerTree()));
        }

        return new Response("personage not found", Response::HTTP_NOT_FOUND);
    }

    /**
     * Family tree of a personage, ascendants and descendants
     *
     * @Route("/personage/{id}/tree", name="personage_one_tree")
     * @param string $id
     * @return Response
     */
    public function getOneTree(string $id): Response
    {
        $id = intval($id);
        /** @var Personage $personage */
        $personage = $this->getPersonageRepository()->find($id);
        if ($personage) {
            return new Response(json_encode($personage->getFamilyTree()));
        }

        return new Response("personage not found", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/marriage/", name="personage_marriage")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function marriageAction(Request $request): Response
    {
        $maleId = $request->query->get("male");
        $femaleId = $request->query->get("female");
        $fundNewMesnie = ($request->query->get("newMesnie") !== null);
        if (!$maleId) {
            return new Response("missing male parameter", Response::HTTP_BAD_REQUEST);
        }
        if (!$femaleId) {
            return new Response("missing female parameter", Response::HTTP_BAD_REQUEST);
        }

        $male = $this->getPersonageRepository()->find(intval($maleId));
        $female = $this->getPersonageRepository()->find(intval($femaleId));
        if (!$male || !$female) {
            return new Response("personage not found", Response::HTTP_NOT_FOUND);
        }

        try {
            $date = $this->dateService->currentDate();
            $this->marriageService->marry($male, $female, $date->getYear(), $fundNewMesnie);
        } catch (PersonageException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response("OK");
    }

    /**
     * @Route("/marriage/commoner", name="personage_marriage_commoner")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function marryCommonerAction(Request $request): Response
    {
        $persoId = $request->query->get("perso");
        if (!$persoId) {
            return new Response("missing perso parameter", Response::HTTP_BAD_REQUEST);
        }

        $perso = $this->getPersonageRepository()->find(intval($persoId));
        if (!$perso) {
            return new Response("personage doesn't exist", 404);
        }

        try {
            $date = $this->dateService->currentDate();
            $this->marriageService->marryCommoner($perso, $date->getYear());
        } catch (PersonageException $e) {
            return new Response("Personage error. Please send us this error code : 684", 500);
        } catch (\Exception $e) {
            return new Response("Date error. Please send us this error code : 1868416", 500);
        }

        return new Response("OK");
    }
}
