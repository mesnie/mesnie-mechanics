<?php
/**
 * DateController.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Controller;

/** Usages */
use App\Service\DateService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The DateController provides a route on which is returned the current date
 *
 * Class DateController
 * @package App\Controller
 */
class DateController extends AbstractController
{
    /** @var DateService $dateService */
    private $dateService;

    /**
     * DateController constructor.
     * Autowires the DateService
     *
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->dateService = new DateService($entityManager, $logger);
    }

    /**
     * Returns a JSON of the current date.
     * If a valid lang parameter is provided, the month value will be in the seletec langage
     * Valid lang parameters : en
     *
     * @Route("/date/", name="date_get")
     *
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $langage = $request->get("lang");
        try {
            $date = $this->dateService->currentDate();
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
        return new Response(json_encode($date->toArray($langage)));
    }
}
