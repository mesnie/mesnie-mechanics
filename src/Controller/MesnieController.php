<?php
/**
 * MesnieController.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Controller;

/** Usages */
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Exception\PersonageException;
use App\Manager\MesnieManager;
use App\Repository\MesnieRepository;
use App\Repository\PersonageRepository;
use App\Service\DateService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MesnieController
 * @package App\Controller
 */
class MesnieController extends AbstractController
{
    /** @var MesnieManager $mesnieManager */
    private $mesnieManager;

    /** @var DateService $dateService */
    private $dateService;

    /**
     * MesnieController constructor.
     * Autowires FamilyService and DateService used by the createMesnieAction() method
     *
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->dateService = new DateService($entityManager, $logger);
        $this->mesnieManager = new MesnieManager($entityManager);
    }

    /**
     * The MesnieRepository is used by all methods to get Mesnie(s) from database
     *
     * @return MesnieRepository
     */
    private function getMesnieRepo(): MesnieRepository
    {
        /** @var MesnieRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Mesnie::class);

        return $repo;
    }

    /**
     * The PersonageRepository is used by the createMesnieAction()
     * to find the personage who will fund the new Mesnie
     *
     * @return PersonageRepository
     */
    private function getPersonageRepository(): PersonageRepository
    {
        /** @var PersonageRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Personage::class);

        return $repo;
    }

    /**
     * Returns all Mesnies
     *
     * @Route("/mesnie/", name="mesnie_list")
     *
     * @return Response
     */
    public function listAction(): Response
    {
        $mesnies = $this->getMesnieRepo()->findAll();
        $mesniesArray = [];
        foreach ($mesnies as $mesnie) {
            $mesniesArray[] = $mesnie->toArray(1);
        }

        return new Response(json_encode($mesniesArray));
    }

    /**
     * Returns the JSON of the selected mesnie, or 404 if it doesn't exist
     *
     * @Route("/mesnie/{id}/", name="mesnie_one")
     *
     * @param string $id
     * @return Response
     */
    public function getAction(string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($id));
        if ($mesnie) {
            return new Response(json_encode($mesnie->toArray(1)));
        }

        return new Response("mesnie not found", 404);
    }

    /**
     * Returns the JSON of all members, with all their informations
     *
     * @Route("/mesnie/{id}/members", name="mesnie_members")
     *
     * @param string $id
     * @return Response
     */
    public function getMembersAction(string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($id));
        if ($mesnie) {
            $members = [];
            foreach ($mesnie->getMembers() as $member) {
                $members[] = $member->toArray(1);
            }
            return new Response(json_encode($members));
        }

        return new Response("mesnie not found", 404);
    }

    /**
     * Returns the JSON of all fields, with all their informations
     *
     * @Route("/mesnie/{id}/fields", name="mesnie_fields")
     *
     * @param string $id
     * @return Response
     */
    public function getFieldsAction(string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($id));
        if ($mesnie) {
            $fields = [];
            foreach ($mesnie->getFields() as $field) {
                $fields[] = $field->toArray();
            }
            return new Response(json_encode($fields));
        }

        return new Response("mesnie not found", 404);
    }

    /**
     * Returns the JSON of all items in the inventory
     *
     * @Route("/mesnie/{id}/inventory", name="mesnie_inventory")
     *
     * @param string $id
     * @return Response
     */
    public function getInventoryAction(string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($id));
        if ($mesnie) {
            $items = [];
            foreach ($mesnie->getInventory() as $item) {
                $items[] = $item->toArray(0);
            }
            return new Response(json_encode($items));
        }

        return new Response("mesnie not found", 404);
    }

    /**
     * Creates a new Mesnie with the given Personage as head, and a new Family as an option.
     * The perso POST parameter is required to get the selected Personage.
     * The name POST parameter is optional. If given, a new Family will be created.
     * Returns the id of the newly created Mesnie.
     *
     * @Route("/mesnie/", name="mesnie_create")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function createMesnieAction(Request $request): Response
    {
        $persoId = $request->query->get("perso");
        $newFamilyName = $request->query->get("name");
        if (!$persoId) {
            return new Response("missing perso parameter", Response::HTTP_BAD_REQUEST);
        }

        $perso = $this->getPersonageRepository()->find(intval($persoId));
        if (!$perso) {
            return new Response("personage doesn't exist", 404);
        }

        try {
            $date = $this->dateService->currentDate();
            $mesnie = $this->mesnieManager->fundMesnie($perso, $date->getYear(), $newFamilyName);
        } catch (PersonageException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response($mesnie->getId());
    }
}
