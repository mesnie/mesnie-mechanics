<?php
/**
 * WorkController.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Controller;

/** Usages */
use App\Entity\Field;
use App\Entity\Mesnie;
use App\Entity\Personage;
use App\Entity\WorkType;
use App\Exception\WorkException;
use App\Repository\FieldRepository;
use App\Repository\MesnieRepository;
use App\Repository\PersonageRepository;
use App\Repository\WorkTypeRepository;
use App\Service\WorkService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WorkController
 * @package App\Controller
 */
class WorkController extends AbstractController
{
    /** @var WorkService $workService */
    private $workService;

    /**
     * WorkController constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->workService = new WorkService($entityManager);
    }

    /**
     * @return MesnieRepository
     */
    private function getMesnieRepo(): MesnieRepository
    {
        /** @var MesnieRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Mesnie::class);

        return $repo;
    }

    /**
     * @return PersonageRepository
     */
    private function getPersoRepo(): PersonageRepository
    {
        /** @var PersonageRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Personage::class);

        return $repo;
    }

    /**
     * @return WorkTypeRepository
     */
    private function getWorkRepo(): WorkTypeRepository
    {
        /** @var WorkTypeRepository $repo */
        $repo = $this->getDoctrine()->getRepository(WorkType::class);

        return $repo;
    }

    /**
     * @return FieldRepository
     */
    private function getFieldRepo(): FieldRepository
    {
        /** @var FieldRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Field::class);

        return $repo;
    }


    /**
     * @return WorkTypeRepository
     */
    private function getWorkTypeRepo(): WorkTypeRepository
    {
        /** @var WorkTypeRepository $repo */
        $repo = $this->getDoctrine()->getRepository(WorkType::class);

        return $repo;
    }

    /**
     * @Route("/mesnie/{id}/works", name="mesnie_works")
     * @param string $id
     * @return Response
     */
    public function getWorksAction(string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($id));
        if ($mesnie) {
            $works = [];
            /** @var WorkType $work */
            foreach ($this->getWorkRepo()->findByMesnie($mesnie) as $work) {
                $works[] = $work->toArray();
            }

            return new Response(json_encode($works));
        }

        return new Response("mesnie not found", 404);
    }

    /**
     * @Route("/mesnie/{mesnie_id}/fields/{id}/works", name="mesnie_fields_works")
     * @param string $mesnieId
     * @param string $id
     * @return Response
     */
    public function getWorksOnField(string $mesnieId, string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($mesnieId));
        $field = $this->getFieldRepo()->find(intval($id));
        if ($field && $mesnie && ($field->getMesnie() === $mesnie)) {
            $workList = $this->getWorkRepo()->findBy([
                "field" => $field
            ]);
            $works = [];
            foreach ($workList as $work) {
                $works[] = $work->toArray();
            }

            return new Response(json_encode($works));
        }

        return new Response("field not found", 404);
    }

    /**
     * @Route("/mesnie/{mesnie_id}/fields/{id}/worktypes", name="mesnie_fields_worktypes")
     * @param string $mesnieId
     * @param string $id
     * @return Response
     */
    public function getWorkTypesOnField(string $mesnieId, string $id): Response
    {
        $mesnie = $this->getMesnieRepo()->find(intval($mesnieId));
        $field = $this->getFieldRepo()->find(intval($id));
        if ($field && $mesnie && ($field->getMesnie() === $mesnie)) {
            $workTypeList = $this->getWorkTypeRepo()->findBy([
                "fieldType" => $field->getType()
            ]);
            $workTypes = [];
            foreach ($workTypeList as $workType) {
                $workTypes[] = $workType->toArray(1);
            }

            return new Response(json_encode($workTypes));
        }

        return new Response("field not found", 404);
    }

    /**
     * @Route("/personage/work/", name="personage_work_create")
     * @Method("POST")
     * @param Request $request
     * @return Response
     * @throws \App\Exception\MaterialException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setWorkAction(Request $request): Response
    {
        $persoId = $request->query->get("perso");
        $workTypeId = $request->query->get("workType");
        $fieldId = $request->query->get("field");
        if (!$persoId) {
            return new Response("missing personage parameter", Response::HTTP_BAD_REQUEST);
        }
        if (!$workTypeId) {
            return new Response("missing workType parameter", Response::HTTP_BAD_REQUEST);
        }

        $perso = $this->getPersoRepo()->find($persoId);
        $workType = $this->getWorkTypeRepo()->find($workTypeId);
        $field = $fieldId ? $this->getFieldRepo()->find($fieldId) : null;
        if (!$perso) {
            return new Response("personage not found", Response::HTTP_NOT_FOUND);
        }
        if (!$workType) {
            return new Response("workType not found", Response::HTTP_NOT_FOUND);
        }
        if ($fieldId && !$field) {
            return new Response("field not found", Response::HTTP_NOT_FOUND);
        }

        try {
            $this->workService->setToWork($perso, $workType, $field);
        } catch (WorkException $e) {
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response();
    }
}
