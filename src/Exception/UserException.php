<?php
/**
 * UserException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\User;

/**
 * Class UserException
 * @package App\Exception
 */
class UserException extends \Exception
{
    const INVALID_EMAIL         = 80;
    const MESNIE_NOT_RELATED    = 81;

    /** @var User $user */
    private $user;

    /**
     * UserException constructor.
     *
     * @param string $message
     * @param int $code
     * @param User|null $user
     */
    public function __construct(string $message = "", int $code = 0, ?User $user = null)
    {
        parent::__construct($message, $code);
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getUserJSON(): string
    {
        $json = "{}";
        if ($this->user) {
            $json = json_encode($this->user->getUsername());
        }

        return $json;
    }
}
