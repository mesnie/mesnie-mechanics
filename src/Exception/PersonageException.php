<?php
/**
 * PersonageException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\Personage;

/**
 * Class PersonageException
 * @package App\Exception
 */
class PersonageException extends \Exception
{
    const ERROR_NAME_ALREADY        = 10;
    const ERROR_NAME_SHORT          = 11;
    const ERROR_DEATH_ALREADY       = 12;
    const ERROR_DEATH_BIRTH         = 13;
    const ERROR_BIRTH_DATE          = 14;
    const ERROR_MESNIE_HEAD         = 15;


    /** @var Personage|null */
    private $personage;

    /**
     * PersonageException constructor.
     *
     * @param string            $message
     * @param int               $code
     * @param Personage|null    $personage
     */
    public function __construct(
        string      $message,
        int         $code,
        ?Personage  $personage = null
    ) {
        parent::__construct($message, $code, null);
        $this->personage = $personage;
    }

    /**
     * @return string
     */
    public function getPersonageJSON(): string
    {
        $json = "{}";
        if ($this->personage) {
            $json = json_encode($this->personage->toArray(1));
        }

        return $json;
    }
}
