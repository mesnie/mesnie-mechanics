<?php
/**
 * MaterialException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\MaterialQuantity;

/**
 * Class MaterialException
 * @package App\Exception
 */
class MaterialException extends \Exception
{
    const ERROR_QUANTITY_NEGATIVE   = 30;
    const ERROR_TYPE_NOT_ALLOWED    = 31;
    const ERROR_INVENTORY_ENOUGH    = 32;

    /** @var MaterialQuantity $materialQuantity */
    private $materialQuantity;

    /**
     * MaterialException constructor.
     *
     * @param string                $message
     * @param int                   $code
     * @param MaterialQuantity|null $materialQuantity
     */
    public function __construct(
        string              $message,
        int                 $code,
        ?MaterialQuantity   $materialQuantity = null
    ) {
        parent::__construct($message, $code, null);
        $this->materialQuantity = $materialQuantity;
    }

    /**
     * @return string
     */
    public function getMaterialQuantityJSON(): string
    {
        $json = "{}";
        if ($this->materialQuantity) {
            $json = json_encode($this->materialQuantity->toArray());
        }

        return $json;
    }
}
