<?php
/**
 * ChildException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\Personage;
use App\Entity\Pregnancy;

/**
 * Class ChildException
 * @package App\Exception
 */
class ChildException extends \Exception
{
    const ERROR_CHILD_AGE           = 60;
    const ERROR_CHILD_ALREADY       = 61;
    const ERROR_PREGNANCY_ALREADY   = 62;
    const ERROR_PREGNANCY_MALE      = 63;
    const ERROR_PREGNANCY_OTHER     = 64;
    const ERROR_PARENT_SEX          = 65;
    const WRONG_PREGNANCY_MONTH     = 66;
    const WRONG_PREGNANCY_NUMBER    = 67;

    /** @var Personage|null */
    private $father;

    /** @var Personage|null */
    private $mother;

    /** @var Personage|null */
    private $child;

    /** @var Pregnancy|null */
    private $pregnancy;

    /**
     * MariageException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Personage|null $child
     * @param Personage|null $father
     * @param Personage|null $mother
     * @param Pregnancy|null $pregnancy
     */
    public function __construct(
        string      $message,
        int         $code,
        ?Personage  $child = null,
        ?Personage  $father = null,
        ?Personage  $mother = null,
        ?Pregnancy  $pregnancy = null
    ) {
        parent::__construct($message, $code, null);
        $this->father = $father;
        $this->mother = $mother;
        $this->child = $child;
        $this->pregnancy = $pregnancy;
    }

    /**
     * @return string
     */
    public function getPersonagesJSON(): string
    {
        $json = json_encode([
            "father"    => $this->father ?? $this->father->toArray(0),
            "mother"    => $this->mother ?? $this->mother->toArray(0),
            "child"     => $this->child ?? $this->child->toArray(0),
            "pregnancy" => $this->pregnancy ?? $this->pregnancy->toArray(0)
        ]);

        return $json;
    }
}
