<?php
/**
 * MoneyException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use Throwable;

/**
 * Class MoneyException
 * @package App\Exception
 */
class MoneyException extends \Exception
{
    const VALUE_NEGATIVE    = 90;
    const NO_VALUE          = 91;
    const TOO_POOR          = 92;

    /**
     * @var float $value
     */
    private $value;

    /**
     * MoneyException constructor.
     *
     * @param string            $message
     * @param int               $code
     * @param Throwable|null    $previous
     * @param float|null        $value
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, ?float $value = null)
    {
        parent::__construct($message, $code, $previous);
        $this->value = $value;
    }

}