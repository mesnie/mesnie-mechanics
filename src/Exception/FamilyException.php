<?php
/**
 * PersonageException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\Family;

/**
 * Class FamilyException
 * @package App\Exception
 */
class FamilyException extends \Exception
{
    const ERROR_NAME_SHORT  = 20;
    const OTHER_FAMILY      = 21;
    const WRONG_MESNIE      = 22;

    /** @var Family|null */
    private $family;

    /**
     * FamilyException constructor.
     *
     * @param string        $message
     * @param int           $code
     * @param Family|null   $family
     */
    public function __construct(
        string  $message,
        int     $code,
        ?Family $family = null
    ) {
        parent::__construct($message, $code, null);
        $this->family = $family;
    }

    /**
     * @return string
     */
    public function getFamilyJSON(): string
    {
        $json = "{}";
        if ($this->family) {
            $json = json_encode($this->family->toArray());
        }

        return $json;
    }
}
