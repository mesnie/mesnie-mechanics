<?php
/**
 * DateException.php.
 * @author  mickael
 * @licence GNU GPLv3
 */

/** Namespace **/
namespace App\Exception;

/**
 * Class DateException
 * @package App\Exception
 */
class DateException extends \Exception
{
    const NON_UNIQUE = 90;
    const CANNOT_CREATE = 91;
    const INVALID_VALUE = 92;
}
