<?php
/**
 * WorkException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\WorkType;

/**
 * Class WorkException
 * @package App\Exception
 */
class WorkException extends \Exception
{
    const ERROR_WORKER_IDLE     = 40;
    const ERROR_NOTHING_CANCEL  = 41;
    const ERROR_NEED_ENOUGH     = 42;
    const ERROR_FIELD_ALREADY   = 43;
    const ERROR_FIELD_TYPE      = 44;
    const ERROR_NO_WORK         = 45;
    const ERROR_MISSING_PLANT   = 46;

    /** @var WorkType */
    private $workType;

    /**
     * WorkException constructor.
     *
     * @param string                $message
     * @param int                   $code
         * @param WorkType|null     $workType
     */
    public function __construct(
        string              $message,
        int                 $code,
        ?WorkType           $workType = null
    ) {
        parent::__construct($message, $code, null);
        $this->workType = $workType;
    }

    /**
     * @return string
     */
    public function getWorkTypeJSON(): string
    {
        $json = "{}";
        if ($this->workType) {
            $json = json_encode($this->workType->toArray());
        }

        return $json;
    }
}
