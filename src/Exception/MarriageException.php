<?php
/**
 * MarriageException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\Personage;

/**
 * Class MarriageException
 * @package App\Exception
 */
class MarriageException extends \Exception
{
    const ERROR_MARRY_ALREADY   = 50;
    const ERROR_MARRY_SEX       = 51;
    const ERROR_MARRY_AGE       = 52;
    const ERROR_MARRY_EMPEACHMT = 53;
    const ERROR_REMOVE_NOONE    = 54;

    /** @var Personage|null */
    private $male;

    /** @var Personage|null */
    private $female;

    /**
     * MariageException constructor.
     *
     * @param string            $message
     * @param int               $code
     * @param Personage|null    $male
     * @param Personage|null    $female
     */
    public function __construct(
        string      $message,
        int         $code,
        ?Personage  $male = null,
        ?Personage  $female = null
    ) {
        parent::__construct($message, $code, null);
        $this->male = $male;
        $this->female = $female;
    }

    /**
     * @return string
     */
    public function getPersonagesJSON(): string
    {
        $json = json_encode([
            "male"      => $this->male ?? $this->male->toArray(0),
            "female"    => $this->female ?? $this->female->toArray(0)
        ]);

        return $json;
    }
}
