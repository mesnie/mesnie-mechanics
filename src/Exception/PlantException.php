<?php
/**
 * FieldException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

/** Usages */
use App\Entity\Plant;

class PlantException extends \Exception
{
    const TYPE_NOT_ALLOWED  = 80;

    /** @var Plant $plant */
    private $plant;

    /**
     * PlantException constructor.
     *
     * @param string        $message
     * @param int           $code
     * @param Plant|null    $plant
     */
    public function __construct(
        string  $message,
        int     $code,
        ?Plant  $plant = null
    ) {
        parent::__construct($message, $code, null);
        $this->plant = $plant;
    }

    /**
     * @return string
     */
    public function getPlantJSON(): string
    {
        $json = "{}";
        if ($this->plant) {
            $json = json_encode($this->plant->toArray());
        }

        return $json;
    }
}
