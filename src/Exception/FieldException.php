<?php
/**
 * FieldException.php
 * @author Mickaël Buliard
 * @licence: GNU GPLv3
 */

/** Namespace */
namespace App\Exception;

use App\Entity\Field;
use App\Entity\FieldType;

/**
 * Class FieldException
 * @package App\Exception
 */
class FieldException extends \Exception
{
    const NO_GROWING_PLANT  = 71;
    const NO_WORK           = 72;
    const WRONG_OWNER       = 73;

    /** @var Field $field */
    private $field;

    /** @var FieldType $fieldType */
    private $fieldType;

    /**
     * FieldException constructor.
     *
     * @param string            $message
     * @param int               $code
     * @param FieldType|null    $fieldType
     * @param Field|null        $field
     */
    public function __construct(
        string      $message,
        int         $code,
        ?FieldType  $fieldType = null,
        ?Field      $field = null
    ) {
        parent::__construct($message, $code, null);
        $this->fieldType = $fieldType;
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getFieldsJson(): string
    {
        $json = json_encode([
            "field"     => $this->field ?? $this->field->toArray(),
            "fieldType" => $this->fieldType ?? $this->fieldType->toArray()
        ]);

        return $json;
    }
}
